#include "srsreco/SRSAlignment.h"
#include "srsreco/Utils.h"
#include "srsutils/Logging.h"

ClassImp(SRSAlignment);

SRSAlignment::~SRSAlignment() { ClearTrackCoordinates(); }

std::map<float, float> SRSAlignment::GetTrackFitData(const std::map<float, float>& /*rawData*/) {
  /*auto trackFit = std::make_unique<SRSTrackFit>(rawData);
  const auto fitData = trackFit->getFitData(rawData);
  fTrackOffset = trackFit->getTrackOffset();
  fTrackDirection = trackFit->getTrackDirection();
  return fitData;*/
  return std::map<float, float>{};
}

float SRSAlignment::GetAngleFromTracks(std::vector<float> inX,
                                       std::vector<float> inY,
                                       std::vector<float> inZ,
                                       std::vector<float> outX,
                                       std::vector<float> outY,
                                       std::vector<float> outZ) {
  const auto inXsize = inX.size(), inYsize = inY.size(), inZsize = inZ.size(), outXsize = outX.size(),
             outYsize = outY.size(), outZsize = outZ.size();
  if (inYsize != inXsize || inZsize != inXsize || outYsize != outXsize || outZsize != outXsize)
    SRS_WARNING("SRSAlignment:GetAngleFromTracks") << "Input vectors are not all the same dimension.";
  const auto u = std::vector<float>{inX[inYsize - 1] - inX[0], inY[inYsize - 1] - inY[0], inZ[inYsize - 1] - inZ[0]},
             v = std::vector<float>{
                 outX[outYsize - 1] - outX[0], outY[outYsize - 1] - outY[0], outZ[outYsize - 1] - outZ[0]};
  return srsreco::utils::getAngleTo(u, v);  // calculate the scattering angle
}

void SRSAlignment::ClearTrackCoordinates() {
  fTrackX.clear();
  fTrackY.clear();
  fTrackZ.clear();
}

void SRSAlignment::TrackCoordinates() {
  ClearTrackCoordinates();
  for (const auto& detector : fSRSTrack->GetTrackSpacePoints()) {
    const auto& detectorName = detector.first;
    if (fSRSTrack->GetDetectorType(detectorName) == "tracker") {
      const auto& detectorHit = detector.second;
      fTrackX.push_back(detectorHit.at(0));
      fTrackY.push_back(detectorHit.at(1));
      fTrackZ.push_back(detectorHit.at(2));
    }
  }
}
