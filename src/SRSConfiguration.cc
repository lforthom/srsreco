#include <TSystem.h>

#include <fstream>

#include "srsreco/SRSConfiguration.h"
#include "srsutils/Logging.h"

ClassImp(SRSConfiguration);

SRSConfiguration::SRSConfiguration() { Init(); }

SRSConfiguration::SRSConfiguration(const char* file) { Init(file); }

bool SRSConfiguration::FileExists(const char* name) const {
  if (std::ifstream f(gSystem->ExpandPathName(name)); f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }
}

//============================================================================================
SRSConfiguration& SRSConfiguration::operator=(const SRSConfiguration& rhs) {
  fCycleWait = rhs.GetCycleWait();
  fRunType = rhs.GetRunType();
  fRunName = rhs.GetRunName();
  fZeroSupCut = rhs.GetZeroSupCut();
  fROOTDataType = rhs.GetROOTDataType();
  fRunNbFile = rhs.GetRunNbFile();
  fMappingFile = rhs.GetMappingFile();
  fPadMappingFile = rhs.GetPadMappingFile();
  fSavedMappingFile = rhs.GetSavedMappingFile();
  fHistosFile = rhs.GetHistoCfgName();
  fDisplayFile = rhs.GetDisplayCfgName();
  fPedestalFile = rhs.GetPedestalFile();
  fTrackingOffsetDir = rhs.GetTrackingOffsetDir();
  fPositionCorrectionFile = rhs.GetClusterPositionCorrectionFile();
  fPositionCorrectionFlag = rhs.GetClusterPositionCorrectionFlag();
  fMaskedChannelCut = rhs.GetMaskedChannelCut();
  fStartEventNumber = rhs.GetStartEventNumber();
  fEventFrequencyNumber = rhs.GetEventFrequencyNumber();
  fRawPedestalFile = rhs.GetRawPedestalFile();
  fMinClusterSize = rhs.GetMinClusterSize();
  fMaxClusterSize = rhs.GetMaxClusterSize();
  fMaxClusterMultiplicity = rhs.GetMaxClusterMultiplicity();
  fIsHitMaxOrTotalADCs = rhs.GetHitMaxOrTotalADCs();
  fIsClusterMaxOrTotalADCs = rhs.GetClusterMaxOrTotalADCs();
  fAPVGainCalibrationFile = rhs.GetAPVGainCalibrationFile();
  return *this;
}

//============================================================================================
void SRSConfiguration::Init(const char* file) {
  if (!file) {
    SRS_WARNING("SRSConfiguratio:Init") << "conf file not specified. Setting defaults.";
    SetDefaults();
  } else {
    if (!Load(file)) {
      SRS_WARNING("SRSConfiguration:Init") << "Cannot open conf file. Setting defaults.";
      SetDefaults();
    }
  }
}

//============================================================================================
void SRSConfiguration::SetDefaults() {
  fRunType = "RAWDATA";
  fRunName = "data";
  fCycleWait = "1";
  fZeroSupCut = "3";
  fMaskedChannelCut = "20";
  fMaxClusterSize = "20";
  fMinClusterSize = "0";
  fMappingFile = "../../configFileDir/mapping.cfg";
  fPadMappingFile = "../../configFileDir/padMapping.cfg";
  fSavedMappingFile = "../../results/savedMapping.cfg";
  fRunNbFile = "../../configFileDir/runNb.cfg";
  fHistosFile = "../../configFileDir/histogram.cfg";
  fDisplayFile = "../../configFileDir/display.cfg";
  fPedestalFile = "../../pedestalDir/pedestal.root";
  fRawPedestalFile = "../../pedestalDir/rawpedestal.root";
  fPositionCorrectionFile = "../../pedestalDir/clusterPositionCorrection.root";
  fPositionCorrectionFlag = "NO";
  fTrackingOffsetDir = "../../offsetDir/";
  fROOTDataType = "HITS_ONLY";
  fAPVGainCalibrationFile = "";
  fMaxClusterMultiplicity = "1";
  fIsHitMaxOrTotalADCs = "signalPeak";
  fIsClusterMaxOrTotalADCs = "TotalCharges";
  fStartEventNumber = "0";
  fEventFrequencyNumber = "1";
}

//============================================================================================
void SRSConfiguration::Save(const char* filename) const {
  SRS_DEBUG("SRSConfiguration:Save") << "saving conf in '" << gSystem->ExpandPathName(filename) << "'.";
  std::ofstream file(gSystem->ExpandPathName(filename));
  file << "RUNNAME " << fRunName << std::endl;
  file << "RUNTYPE " << fRunType << std::endl;
  file << "CYCLEWAIT " << fCycleWait << std::endl;
  file << "ZEROSUPCUT " << fZeroSupCut << std::endl;
  file << "CHMASKCUT " << fMaskedChannelCut << std::endl;
  file << "MINCLUSTSIZE " << fMinClusterSize << std::endl;
  file << "MAXCLUSTSIZE " << fMaxClusterSize << std::endl;
  file << "MAXCLUSTMULT " << fMaxClusterMultiplicity << std::endl;
  file << "MAPFILE " << fMappingFile << std::endl;
  file << "PADMAPFILE " << fPadMappingFile << std::endl;
  file << "SAVEDMAPFILE " << fSavedMappingFile << std::endl;
  file << "RUNNBFILE " << fRunNbFile << std::endl;
  file << "HISTCFG " << fHistosFile << std::endl;
  file << "DISPCFG " << fDisplayFile << std::endl;
  file << "OFFSETDIR " << fTrackingOffsetDir << std::endl;
  file << "ROOTDATATYPE " << fROOTDataType << std::endl;
  file << "PEDFILE " << fPedestalFile << std::endl;
  file << "RAWPEDFILE " << fRawPedestalFile << std::endl;
  file << "CLUSTERPOSCORRFILE " << fPositionCorrectionFile << std::endl;
  file << "CLUSTERPOSCORRFLAG " << fPositionCorrectionFlag << std::endl;
  file << "STARTEVENTNUMBER " << fStartEventNumber << std::endl;
  file << "EVENTFREQUENCYNUMBER " << fEventFrequencyNumber << std::endl;
  file << "APVGAINCALIB " << fAPVGainCalibrationFile << std::endl;
  file << "HIT_ADCS " << fIsHitMaxOrTotalADCs << std::endl;
  file << "CLUSTER_ADCS " << fIsClusterMaxOrTotalADCs << std::endl;
  file.close();
}

//============================================================================================
bool SRSConfiguration::Load(const char* filename) {
  SRS_INFO("SRSConfiguration:Load") << "Loading cfg from '" << gSystem->ExpandPathName(filename) << "'.";
  /*if (std::ifstream file(gSystem->ExpandPathName(filename), std::ifstream::in); file.is_open())
    return false;*/

  std::ifstream file;
  if (FileExists(gSystem->ExpandPathName(filename))) {
    try {
      file.open(gSystem->ExpandPathName(filename), std::ifstream::in);
    } catch (std::ifstream::failure&) {
      SRS_ERROR("SRSConfiguration:Load") << "File '" << gSystem->ExpandPathName(filename)
                                         << "' does not exist or cannot be opened!";
    } catch (...) {
      SRS_ERROR("SRSConfiguration:Load") << "Non-processed exception!";
    }
    if (!file.is_open())
      return false;
  } else
    return false;

  TString line;
  while (line.ReadLine(file)) {
    // strip leading spaces and skip comments
    line.Remove(TString::kLeading, ' ');
    if (line.BeginsWith("#"))
      continue;

    if (line.BeginsWith("RUNTYPE")) {
      char hfile[1000];
      sscanf(line.Data(), "RUNTYPE %s", hfile);
      fRunType = hfile;
    }
    if (line.BeginsWith("RUNNAME")) {
      char hfile[1000];
      sscanf(line.Data(), "RUNNAME %s", hfile);
      fRunName = hfile;
    }
    if (line.BeginsWith("ZEROSUPCUT")) {
      char hfile[10];
      sscanf(line.Data(), "ZEROSUPCUT %s", hfile);
      fZeroSupCut = hfile;
    }
    if (line.BeginsWith("MAXCLUSTSIZE")) {
      char hfile[10];
      sscanf(line.Data(), "MAXCLUSTSIZE %s", hfile);
      fMaxClusterSize = hfile;
    }
    if (line.BeginsWith("MINCLUSTSIZE")) {
      char hfile[10];
      sscanf(line.Data(), "MINCLUSTSIZE %s", hfile);
      fMinClusterSize = hfile;
    }
    if (line.BeginsWith("MAXCLUSTMULT")) {
      char hfile[100];
      sscanf(line.Data(), "MAXCLUSTMULT %s", hfile);
      fMaxClusterMultiplicity = hfile;
    }
    if (line.BeginsWith("CYCLEWAIT")) {
      char hfile[100];
      sscanf(line.Data(), "CYCLEWAIT %s", hfile);
      fCycleWait = hfile;
    }
    if (line.BeginsWith("STARTEVENTNUMBER")) {
      char hfile[100];
      sscanf(line.Data(), "STARTEVENTNUMBER %s", hfile);
      fStartEventNumber = hfile;
    }
    if (line.BeginsWith("EVENTFREQUENCYNUMBER")) {
      char hfile[1000];
      sscanf(line.Data(), "EVENTFREQUENCYNUMBER %s", hfile);
      fEventFrequencyNumber = hfile;
    }
    if (line.BeginsWith("HISTCFG")) {
      char hfile[1000];
      sscanf(line.Data(), "HISTCFG %s", hfile);
      fHistosFile = hfile;
    }
    if (line.BeginsWith("PEDFILE")) {
      char pedfile[1000];
      sscanf(line.Data(), "PEDFILE %s", pedfile);
      fPedestalFile = pedfile;
    }
    if (line.BeginsWith("RAWPEDFILE")) {
      char rawpedfile[1000];
      sscanf(line.Data(), "RAWPEDFILE %s", rawpedfile);
      fRawPedestalFile = rawpedfile;
    }
    if (line.BeginsWith("OFFSETDIR")) {
      char offsetfile[1000];
      sscanf(line.Data(), "OFFSETDIR %s", offsetfile);
      fTrackingOffsetDir = offsetfile;
    }
    if (line.BeginsWith("CLUSTERPOSCORRFILE")) {
      char posCorrectfile[1000];
      sscanf(line.Data(), "CLUSTERPOSCORRFILE %s", posCorrectfile);
      fPositionCorrectionFile = posCorrectfile;
    }
    if (line.BeginsWith("CLUSTERPOSCORRFLAG")) {
      char posCorrectflag[10];
      sscanf(line.Data(), "CLUSTERPOSCORRFLAG %s", posCorrectflag);
      fPositionCorrectionFlag = posCorrectflag;
    }
    if (line.BeginsWith("APVGAINCALIB")) {
      char apvGainCalibrationFile[1000];
      sscanf(line.Data(), "APVGAINCALIB %s", apvGainCalibrationFile);
      fAPVGainCalibrationFile = apvGainCalibrationFile;
    }
    if (line.BeginsWith("MAPFILE")) {
      char mapfile[1000];
      sscanf(line.Data(), "MAPFILE %s", mapfile);
      fMappingFile = mapfile;
    }
    if (line.BeginsWith("PADMAPFILE")) {
      char padmapfile[1000];
      sscanf(line.Data(), "PADMAPFILE %s", padmapfile);
      fPadMappingFile = padmapfile;
    }
    if (line.BeginsWith("SAVEDMAPFILE")) {
      char savedmapfile[1000];
      sscanf(line.Data(), "SAVEDMAPFILE %s", savedmapfile);
      fSavedMappingFile = savedmapfile;
    }
    if (line.BeginsWith("RUNNBFILE")) {
      char runNbfile[1000];
      sscanf(line.Data(), "RUNNBFILE %s", runNbfile);
      fRunNbFile = runNbfile;
    }
    if (line.BeginsWith("DISPCFG")) {
      char displayfile[1000];
      sscanf(line.Data(), "DISPCFG %s", displayfile);
      fDisplayFile = displayfile;
    }
    if (line.BeginsWith("CHMASKCUT")) {
      char maskchannels[1000];
      sscanf(line.Data(), "CHMASKCUT %s", maskchannels);
      fMaskedChannelCut = maskchannels;
    }
    if (line.BeginsWith("ROOTDATATYPE")) {
      char rootdatatype[1000];
      sscanf(line.Data(), "ROOTDATATYPE %s", rootdatatype);
      fROOTDataType = rootdatatype;
    }
    if (line.BeginsWith("CLUSTER_ADCS")) {
      char isClusterMaxOrTotalADCs[1000];
      sscanf(line.Data(), " CLUSTER_ADCS%s", isClusterMaxOrTotalADCs);
      fIsClusterMaxOrTotalADCs = isClusterMaxOrTotalADCs;
    }
    if (line.BeginsWith("HIT_ADCS")) {
      char isHitMaxOrTotalADCs[1000];
      sscanf(line.Data(), " HIT_ADCS%s", isHitMaxOrTotalADCs);
      fIsHitMaxOrTotalADCs = isHitMaxOrTotalADCs;
    }
  }
  Dump();
  return true;
}

void SRSConfiguration::Dump() const {
  SRS_INFO("SRSConfiguration:Load") << "RUNTYPE               " << fRunType.Data() << "\n"
                                    << "RUNNAME               " << fRunName.Data() << "\n"
                                    << "CYCLEWAIT             " << fCycleWait.Data() << "\n"
                                    << "ZEROSUPCUT            " << fZeroSupCut.Data() << "\n"
                                    << "MINCLUSTSIZE          " << fMinClusterSize.Data() << "\n"
                                    << "MAXCLUSTSIZE          " << fMaxClusterSize.Data() << "\n"
                                    << "MAXCLUSTMULT          " << fMaxClusterMultiplicity.Data() << "\n"
                                    << "HISTCFG               " << fHistosFile.Data() << "\n"
                                    << "DISPCFG               " << fDisplayFile.Data() << "\n"
                                    << "MAPFILE               " << fMappingFile.Data() << "\n"
                                    << "PADMAPFILE            " << fPadMappingFile.Data() << "\n"
                                    << "SAVEDMAPFILE          " << fSavedMappingFile.Data() << "\n"
                                    << "PEDFILE               " << fPedestalFile.Data() << "\n"
                                    << "RAWPEDFILE            " << fRawPedestalFile.Data() << "\n"
                                    << "CLUSTERPOSCORRFILE    " << fPositionCorrectionFile.Data() << "\n"
                                    << "CLUSTERPOSCORRFLAG    " << fPositionCorrectionFlag.Data() << "\n"
                                    << "HIT_ADCS              " << fIsHitMaxOrTotalADCs.Data() << "\n"
                                    << "CLUSTER_ADCS          " << fIsClusterMaxOrTotalADCs.Data() << "\n"
                                    << "STARTEVENTNUMBER      " << fStartEventNumber.Data() << "\n"
                                    << "EVENTFREQUENCYNUMBER  " << fEventFrequencyNumber.Data() << "\n"
                                    << "APVGAINCALIB          " << fAPVGainCalibrationFile.Data() << "\n"
                                    << "ROOTDATATYPE          " << fROOTDataType.Data() << "\n"
                                    << "OFFSETDIR             " << fTrackingOffsetDir.Data() << ".";
}

const char* SRSConfiguration::GetCycleWait() const { return gSystem->ExpandPathName(fCycleWait.Data()); }

const char* SRSConfiguration::GetRunType() const { return gSystem->ExpandPathName(fRunType.Data()); }

const char* SRSConfiguration::GetRunName() const { return gSystem->ExpandPathName(fRunName.Data()); }

const char* SRSConfiguration::GetZeroSupCut() const { return gSystem->ExpandPathName(fZeroSupCut.Data()); }

const char* SRSConfiguration::GetMaskedChannelCut() const { return gSystem->ExpandPathName(fMaskedChannelCut.Data()); }

const char* SRSConfiguration::GetStartEventNumber() const { return gSystem->ExpandPathName(fStartEventNumber.Data()); }

const char* SRSConfiguration::GetEventFrequencyNumber() const {
  return gSystem->ExpandPathName(fEventFrequencyNumber.Data());
}

const char* SRSConfiguration::GetMinClusterSize() const { return gSystem->ExpandPathName(fMinClusterSize.Data()); }

const char* SRSConfiguration::GetMaxClusterSize() const { return gSystem->ExpandPathName(fMaxClusterSize.Data()); }

const char* SRSConfiguration::GetMaxClusterMultiplicity() const {
  return gSystem->ExpandPathName(fMaxClusterMultiplicity.Data());
}

const char* SRSConfiguration::GetMappingFile() const { return gSystem->ExpandPathName(fMappingFile.Data()); }

const char* SRSConfiguration::GetPadMappingFile() const { return gSystem->ExpandPathName(fPadMappingFile.Data()); }

const char* SRSConfiguration::GetSavedMappingFile() const { return gSystem->ExpandPathName(fSavedMappingFile.Data()); }

const char* SRSConfiguration::GetRunNbFile() const { return gSystem->ExpandPathName(fRunNbFile.Data()); }

const char* SRSConfiguration::GetHistoCfgName() const { return gSystem->ExpandPathName(fHistosFile.Data()); }

const char* SRSConfiguration::GetDisplayCfgName() const { return gSystem->ExpandPathName(fDisplayFile.Data()); }

const char* SRSConfiguration::GetROOTDataType() const { return gSystem->ExpandPathName(fROOTDataType.Data()); }

const char* SRSConfiguration::GetPedestalFile() const { return gSystem->ExpandPathName(fPedestalFile.Data()); }

const char* SRSConfiguration::GetTrackingOffsetDir() const {
  return gSystem->ExpandPathName(fTrackingOffsetDir.Data());
}

const char* SRSConfiguration::GetRawPedestalFile() const { return gSystem->ExpandPathName(fRawPedestalFile.Data()); }

const char* SRSConfiguration::GetClusterPositionCorrectionFile() const {
  return gSystem->ExpandPathName(fPositionCorrectionFile.Data());
}

const char* SRSConfiguration::GetClusterPositionCorrectionFlag() const {
  return gSystem->ExpandPathName(fPositionCorrectionFlag.Data());
}

const char* SRSConfiguration::GetAPVGainCalibrationFile() const {
  return gSystem->ExpandPathName(fAPVGainCalibrationFile.Data());
}

const char* SRSConfiguration::GetClusterMaxOrTotalADCs() const {
  return gSystem->ExpandPathName(fIsClusterMaxOrTotalADCs.Data());
};

const char* SRSConfiguration::GetHitMaxOrTotalADCs() const {
  return gSystem->ExpandPathName(fIsHitMaxOrTotalADCs.Data());
};
