#include <TMath.h>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSMapping.h"
#include "srsutils/Logging.h"

ClassImp(SRSAPVEvent);

SRSAPVEvent::SRSAPVEvent(int fec_no, int fec_channel, int apv_id, int zeroSupCut, int EventNb, int packetSize)
    : fEventNb(EventNb),
      fFECNo(fec_no),
      fADCChannel(fec_channel),
      fAPVID(apv_id),
      fZeroSupCut(zeroSupCut),
      fPacketSize(packetSize) {
  Clear();

  auto mapping = SRSMapping::GetInstance();
  fAPVKey = mapping->GetAPVNoFromID(apv_id);
  fAPV = mapping->GetAPVFromID(apv_id);
  fAPVHeaderLevel = mapping->GetAPVHeaderLevelFromID(apv_id);
  fAPVIndexOnPlane = mapping->GetAPVIndexOnPlane(apv_id);
  fAPVOrientation = mapping->GetAPVOrientation(apv_id);
  fPlane = mapping->GetDetectorPlaneFromAPVID(apv_id);
  fDetector = mapping->GetDetectorFromPlane(fPlane);
  fReadoutBoard = mapping->GetReadoutBoardFromDetector(fDetector);
  fDetectorType = mapping->GetDetectorTypeFromDetector(fDetector);

  if (fReadoutBoard == "CARTESIAN") {
    fPlaneSize = mapping->GetCartesianReadoutMap(fPlane).at(1);
    fNbOfAPVsFromPlane = (int)((mapping->GetCartesianReadoutMap(fPlane))[2]);
    fPadDetectorMap.resize(5);
    SRS_DEBUG("SRSAPVEvent") << "detType='" << fDetectorType.Data() << "', plane='" << fPlane.Data()
                             << "', planeSize=" << fPlaneSize << ", fNbOfAPVsFromPlane=" << fNbOfAPVsFromPlane << ".";
  } else if (fReadoutBoard == "UV_ANGLE") {
    fTrapezoidDetLength = mapping->GetUVangleReadoutMap(fDetector).at(0);
    fTrapezoidDetInnerRadius = (mapping->GetUVangleReadoutMap(fDetector))[1];
    fTrapezoidDetOuterRadius = (mapping->GetUVangleReadoutMap(fDetector))[2];
    fNbOfAPVsFromPlane = (int)((mapping->GetUVangleReadoutMap(fPlane))[1]);
    fPadDetectorMap.resize(5);
    SRS_DEBUG("SRSAPVEvent") << "fDetector = '" << fDetector.Data() << "',  fPlane='" << fPlane.Data()
                             << "', detLength=" << fTrapezoidDetLength << ", innerRadius=" << fTrapezoidDetInnerRadius
                             << ", outerRadius=" << fTrapezoidDetOuterRadius
                             << ", fNbOfAPVsFromPlane=" << fNbOfAPVsFromPlane << ".";
  } else if (fReadoutBoard == "1DSTRIPS") {
    fPlaneSize = (mapping->Get1DStripsReadoutMap(fPlane))[1];
    fNbOfAPVsFromPlane = (int)((mapping->Get1DStripsReadoutMap(fPlane))[2]);
    fPadDetectorMap.resize(5);
    SRS_DEBUG("SRSAPVEvent") << "fPlane='" << fPlane.Data() << "', planeSize=" << fPlaneSize
                             << ", fNbOfAPVsFromPlane=" << fNbOfAPVsFromPlane << ".";
  } else if (fReadoutBoard == "CMSGEM") {
    fEtaSectorPos = (mapping->GetCMSGEMReadoutMap(fPlane))[0];
    fPlaneSize = (mapping->GetCMSGEMReadoutMap(fPlane))[1];
    fNbOfAPVsFromPlane = (int)((mapping->GetCMSGEMReadoutMap(fPlane))[2]);
    fPadDetectorMap.resize(5);
    SRS_DEBUG("SRSAPVEvent") << "fPlane='" << fPlane.Data() << "', etaSectorPos=" << fEtaSectorPos
                             << ", planeSize=" << fPlaneSize << ", fNbOfAPVsFromPlane=" << fNbOfAPVsFromPlane << ".";
  } else if (fReadoutBoard == "PADPLANE") {
    for (const auto& apvChPadCh : mapping->GetPadChannelsMapping(apv_id)) {
      const auto padCh = (apvChPadCh >> 8) & 0xffff, apvCh = apvChPadCh & 0xff;
      fapvChToPadChMap[apvCh] = padCh;
      fPadDetectorMap = mapping->GetPadDetectorMap(fDetector);
      fPadDetectorMap.resize(5);
    }
  }
  SRS_DEBUG("SRSAPVEvent") << "fPlane='" << fPlane.Data() << "', etaSectorPos=" << fEtaSectorPos
                           << ", planeSize=" << fPlaneSize << ", fNbOfAPVsFromPlane=" << fNbOfAPVsFromPlane << ".";
}

SRSAPVEvent::~SRSAPVEvent() { Clear(); }

void SRSAPVEvent::Clear(Option_t*) {
  fRawData32bits.clear();
  fRawData16bits.clear();
  fapvTimeBinDataMap.clear();
  fPedestalData.clear();
  fRawPedestalData.clear();
  fMaskedChannels.clear();
  fPedestalNoises.clear();
  fRawPedestalNoises.clear();

  fCommonModeOffsets.clear();
  fCommonModeOffsets_odd.clear();
  fCommonModeOffsets_even.clear();
  fPadDetectorMap.clear();
  SRS_DEBUG("SRSAPVEvent:Clear");
}

void SRSAPVEvent::Add32BitsRawData(unsigned int rawData32bits) {
  SRS_DEBUG("SRSAPVEvent:Add32BitsRawData");
  fRawData32bits.push_back(rawData32bits);
}

void SRSAPVEvent::Set32BitsRawData(std::vector<unsigned int> rawData32bits) {
  SRS_DEBUG("SRSAPVEvent:Set32BitsRawData");
  fRawData32bits.clear();
  fRawData32bits = rawData32bits;
}

int SRSAPVEvent::APVchannelCorrection(int chNo) {
  chNo = (32 * (chNo % 4)) + (8 * (int)(chNo / 4)) - (31 * (int)(chNo / 16));
  return chNo;
}

int SRSAPVEvent::NS2StripMapping(int chNo) {
  if ((chNo % 2) == 1)
    return (chNo - 1) / 2 + 64;
  return chNo / 2;
}

int SRSAPVEvent::CMSStripMapping(int chNo) {
  if ((chNo % 2) == 1)
    return 127 - (chNo - 1) / 2;
  return chNo / 2;
}

int SRSAPVEvent::MMStripMappingAPV1(int chNo) {
  if ((chNo % 2) == 1)
    return (chNo - 1) / 2 + 32;
  chNo = (chNo / 2);
  if (chNo < 32)
    return 31 - chNo;
  if (chNo > 37)
    return 159 - chNo;
  return chNo + 90;
}

int SRSAPVEvent::MMStripMappingAPV2(int chNo) {
  if ((chNo % 2) == 1)
    return (chNo - 1) / 2 + 27;
  chNo = (chNo / 2);
  if (chNo < 27)
    return 26 - chNo;
  if (chNo > 38)
    return 154 - chNo;
  return chNo + 89;
}

int SRSAPVEvent::MMStripMappingAPV3(int chNo) {
  if ((chNo % 2) == 1)
    return (chNo - 1) / 2 + 26;
  chNo = (chNo / 2);
  if (chNo < 26)
    return 25 - chNo;
  if (chNo > 31)
    return 153 - chNo;
  return chNo + 96;
}

int SRSAPVEvent::PRadStripsMapping(int chNo) {
  int chno = chNo / 2;
  //  chNo = chNo /2 ;
  //  if (chNo % 2 == 0) chNo = 31 + (chNo / 2) ;
  if (chNo % 2 == 0)
    chNo = 31 + (chNo / 2);
  else {
    if (chNo < 64)
      chNo = 31 - ((chNo + 1) / 2);
    else
      chNo = 127 - ((chNo - 65) / 2);
  }
  SRS_DEBUG("SRSAPVEvent:PRadStripsMapping") << "APVID=" << fAPVID << ", chNo=" << chno << ", stripNo=" << chNo << ".";

  return chNo;
}

int SRSAPVEvent::StandardMapping(int chNo) { return chNo; }

int SRSAPVEvent::EICStripMapping(int chNo) {
  if (chNo % 2 == 0)
    return chNo / 2;
  return 64 + (chNo - 1) / 2;
}

int SRSAPVEvent::HMSStripMapping(int chNo) {
  if (chNo % 4 == 0)
    return chNo + 2;
  if (chNo % 4 == 1)
    return chNo - 1;
  if (chNo % 4 == 2)
    return chNo + 1;
  if (chNo % 4 == 3)
    return chNo - 2;
  return chNo;
}

int SRSAPVEvent::StripMapping(int chNo) {
  chNo = APVchannelCorrection(chNo);
  if (fDetectorType == "CMSGEM")
    return CMSStripMapping(chNo);
  if (fDetectorType == "ZIGZAG")
    return ZigZagStripMapping(chNo);
  if (fDetectorType == "NS2")
    return NS2StripMapping(chNo);
  if (fDetectorType == "EICPROTO1")
    return EICStripMapping(chNo);
  if (fDetectorType == "HMSGEM")
    return HMSStripMapping(chNo);
  if (fDetectorType == "PRADGEM")
    return PRadStripsMapping(chNo);
  return StandardMapping(chNo);
}

void SRSAPVEvent::Print(Option_t*) const {
  SRS_DEBUG("SRSAPVEvent:Print") << "Printing APV Data: -1 == No Data, -2 == Underflow, -3 == Overflow.";
  /*int Size = fRawData32bits.size();
  int Capacity = fRawData32bits.capacity();
  int MaxSize = fRawData32bits.max_size();*/
  for (const auto& rawdata : fRawData32bits)
    SRS_INFO("SRSAPVEvent:Print") << "32 bits raw data [0x" << std::hex << rawdata << "].";
}

void SRSAPVEvent::ComputeRawData16bits() {
  SRS_DEBUG("SRSAPVEvent:ComputeRawData") << "fRawData32bits.size() = " << fRawData32bits.size() << ".";
  fRawData16bits.clear();
  for (const auto& word32bit : fRawData32bits) {
    if (((word32bit >> 8) & 0xffffff) != 0x414443) {
      unsigned int data1 = (word32bit >> 24) & 0xff, data2 = (word32bit >> 16) & 0xff, data3 = (word32bit >> 8) & 0xff,
                   data4 = word32bit & 0xff;
      fRawData16bits.push_back(((data2 << 8) | data1));
      fRawData16bits.push_back(((data4 << 8) | data3));
    }
  }
}

void SRSAPVEvent::ComputeTimeBinCommonMode() {
  SRS_DEBUG("SRSAPVEvent:ComputeTimeBinCommonMode") << "enter";

  int idata = 0;
  Bool_t startDataFlag = false;

  fapvTimeBinDataMap.clear();
  auto apvheaderlevel = (unsigned int)fAPVHeaderLevel;
  int size = fRawData16bits.size();
  /*if (size != fPacketSize)
    SRS_ERROR("SRSAPVEvent:ComputeTimeBinCommonMode") << "Packet size " << size << " different from expected "
                                                      << fPacketSize << ", header=" << fAPVHeaderLevel << ".";*/
  std::list<float> commonModeOffset, commonModeOffset_odd, commonModeOffset_even;
  float apvBaseline = 4096 - TMath::Mean(fPedestalOffsets.begin(), fPedestalOffsets.end());

  while (idata < size) {
    SRS_DEBUG("SRSAPVEvent:ComputeTimeBinCommonMode") << "idata=" << idata << ", apvBaseline=" << apvBaseline << ".";
    //===============================================================//
    // If 3 consecutive words satisfy this condition below => it is  //
    // an APV header so we could take meaninfull data                //
    //===============================================================//
    if (fRawData16bits[idata] < apvheaderlevel) {
      idata++;
      if (fRawData16bits[idata] < apvheaderlevel) {
        idata++;
        if (fRawData16bits[idata] < apvheaderlevel) {
          idata += 10;
          startDataFlag = true;
          continue;
        }
      }
    }

    //===============================================================//
    // That's where the meaninfull data are taken                    //
    // 128 analog word for each apv strip and each timebin           //
    //===============================================================//
    if (startDataFlag == true) {
      commonModeOffset.clear(), commonModeOffset_odd.clear(), commonModeOffset_even.clear();

      float commMode = 0;
      float commMode_odd = 0;
      float commMode_even = 0;

      for (size_t chNo = 0; chNo < kNumChannels; ++chNo) {
        int stripNo = StripMapping(chNo);

        float rawdata = ((float)fRawData16bits[idata]);
        float comMode = rawdata;

        float thresohld = 500;
        if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1"))
          thresohld = 500;

        if (fabs(comMode - apvBaseline) > thresohld) {
          comMode = apvBaseline;
        }

        rawdata = 4096 - rawdata;
        comMode = 4096 - comMode;

        if (fIsCosmicRunFlag) {
          if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
            if (stripNo < 64)
              commonModeOffset_even.push_back(comMode - fPedestalOffsets[stripNo]);
            else
              commonModeOffset_odd.push_back(comMode - fPedestalOffsets[stripNo]);
          } else {
            commonModeOffset.push_back(comMode - fPedestalOffsets[stripNo]);
          }
        }

        if (fIsPedestalRunFlag) {
          if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
            if (stripNo < 64)
              commonModeOffset_even.push_back(comMode - fRawPedestalOffsets[stripNo]);
            else
              commonModeOffset_odd.push_back(comMode - fRawPedestalOffsets[stripNo]);
          } else
            commonModeOffset.push_back(comMode - fRawPedestalOffsets[stripNo]);
        }
        fapvTimeBinDataMap.insert(std::pair<int, float>(stripNo, rawdata));
        idata++;
      }

      if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
        commonModeOffset_odd.sort();
        commonModeOffset_even.sort();

        commMode_odd = TMath::Mean(commonModeOffset_odd.begin(), commonModeOffset_odd.end());
        commMode_even = TMath::Mean(commonModeOffset_even.begin(), commonModeOffset_even.end());
        commonModeOffset_odd.clear();
        commonModeOffset_even.clear();
      } else {
        commonModeOffset.sort();
        commMode = TMath::Mean(commonModeOffset.begin(), commonModeOffset.end());
        commonModeOffset.clear();
      }

      if (fIsRawPedestalRunFlag) {
        if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
          fCommonModeOffsets_odd.push_back(0);
          fCommonModeOffsets_even.push_back(0);
        } else
          fCommonModeOffsets.push_back(0);
      } else {
        if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
          fCommonModeOffsets_odd.push_back(commMode_odd);
          fCommonModeOffsets_even.push_back(commMode_even);
        } else
          fCommonModeOffsets.push_back(commMode);
      }
      startDataFlag = false;
      continue;
    }
    idata++;
  }
  SRS_DEBUG("SRSAPVEvent:ComputeTimeBinCommonMode") << "exit";
}

std::list<SRSHit*> SRSAPVEvent::ComputeListOfAPVHits() {
  SRS_DEBUG("SRSAPVEvent:ComputeListOfAPVHits") << "enter";

  fIsCosmicRunFlag = true;
  fIsPedestalRunFlag = false;
  fIsRawPedestalRunFlag = false;

  ComputeRawData16bits();
  ComputeTimeBinCommonMode();
  std::list<SRSHit*> listOfHits;

  int apvTimeBinDataMapSize = fapvTimeBinDataMap.size();
  int padNo = 0;
  TString plane = fPlane;

  if (apvTimeBinDataMapSize != 0) {
    std::vector<float> stripPedestalNoise;
    for (size_t stripNo = 0; stripNo < kNumChannels; stripNo++) {
      SRS_DEBUG("SRSAPVEvent:ComputeListOfAPVHits") << "stripNo=" << stripNo << ".";
      std::vector<float> timeBinADCs;
      size_t timebin = 0;
      const auto stripSetOfTimeBinRawData = fapvTimeBinDataMap.equal_range(stripNo);
      for (auto timebin_it = stripSetOfTimeBinRawData.first; timebin_it != stripSetOfTimeBinRawData.second;
           ++timebin_it) {
        float rawdata = timebin_it->second;
        SRS_DEBUG("SRSAPVEvent:ComputeListOfAPVHits") << "stripNo=" << stripNo << ", rawdata=" << rawdata << ".";

        // BASELINE CORRECTION
        if (fCommonModeFlag) {
          if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
            if (stripNo < 64)
              rawdata -= fCommonModeOffsets_even[timebin];
            else
              rawdata -= fCommonModeOffsets_odd[timebin];
          } else
            rawdata -= fCommonModeOffsets[timebin];
        }

        // Pedestal Offset Suppression
        if (fPedSubFlag)
          rawdata -= fPedestalOffsets[stripNo];

        // Masked Channels
        rawdata = (1 - fMaskedChannels[stripNo]) * rawdata;

        // APV GAIN CORRECTION DEFAULT VALUE GAIN = 1
        rawdata = rawdata / fAPVGain;
        timeBinADCs.push_back(rawdata);
        timebin++;
      }

      if (fReadoutBoard == "PADPLANE") {
        padNo = fapvChToPadChMap[stripNo];
        if (padNo == 65535)
          continue;
      }

      int stripNb = stripNo;
      if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
        if (stripNo > 63) {
          stripNb = stripNo - 64;
          plane = fDetector + "BOT";
        } else
          plane = fDetector + "TOP";
      }

      // ZERO SUPPRESSION
      if (fZeroSupCut > 0) {
        if (TMath::Mean(timeBinADCs.begin(), timeBinADCs.end()) < fZeroSupCut * fPedestalNoises[stripNo]) {
          stripPedestalNoise.push_back(TMath::Mean(timeBinADCs.begin(), timeBinADCs.end()));
          timeBinADCs.clear();
          continue;
        }

        if ((int)(timeBinADCs.size()) != 0) {
          float adcs = *(TMath::LocMax(timeBinADCs.begin(), timeBinADCs.end()));
          if (adcs < 0)
            adcs = 0;
          SRSHit* apvHit = new SRSHit();
          apvHit->SetAPVID(fAPVID);
          apvHit->IsHitFlag(true);
          apvHit->SetDetector(fDetector);
          apvHit->SetDetectorType(fDetectorType);
          apvHit->SetReadoutBoard(fReadoutBoard);
          apvHit->SetPadDetectorMap(fPadDetectorMap);
          apvHit->SetPlane(plane);
          apvHit->SetPlaneSize(fPlaneSize);
          apvHit->SetTrapezoidDetRadius(fTrapezoidDetInnerRadius, fTrapezoidDetOuterRadius);
          apvHit->SetAPVOrientation(fAPVOrientation);
          apvHit->SetAPVIndexOnPlane(fAPVIndexOnPlane);
          apvHit->SetNbAPVsFromPlane(fNbOfAPVsFromPlane);
          apvHit->SetTimeBinADCs(timeBinADCs);
          apvHit->SetHitADCs(fZeroSupCut, adcs, fIsHitMaxOrTotalADCs);
          apvHit->SetPadNo(padNo);
          apvHit->SetStripNo(stripNb);
          listOfHits.push_back(apvHit);
        }
      } else {
        SRSHit* apvHit = new SRSHit();
        apvHit->SetAPVID(fAPVID);
        float adcs = *(TMath::LocMax(timeBinADCs.begin(), timeBinADCs.end()));
        if (TMath::Mean(timeBinADCs.begin(), timeBinADCs.end()) < 5 * fPedestalNoises[stripNo]) {
          apvHit->IsHitFlag(false);
          adcs = TMath::Mean(timeBinADCs.begin(), timeBinADCs.end());
        }
        apvHit->SetDetector(fDetector);
        apvHit->SetDetectorType(fDetectorType);
        apvHit->SetReadoutBoard(fReadoutBoard);
        apvHit->SetPadDetectorMap(fPadDetectorMap);
        apvHit->SetPlane(plane);
        apvHit->SetPlaneSize(fPlaneSize);
        apvHit->SetTrapezoidDetRadius(fTrapezoidDetInnerRadius, fTrapezoidDetOuterRadius);
        apvHit->SetAPVOrientation(fAPVOrientation);
        apvHit->SetAPVIndexOnPlane(fAPVIndexOnPlane);
        apvHit->SetNbAPVsFromPlane(fNbOfAPVsFromPlane);
        apvHit->SetTimeBinADCs(timeBinADCs);
        apvHit->SetHitADCs(fZeroSupCut, adcs, fIsHitMaxOrTotalADCs);
        apvHit->SetPadNo(padNo);
        apvHit->SetStripNo(stripNb);
        listOfHits.push_back(apvHit);
      }
      timeBinADCs.clear();
    }
    fMeanAPVnoise = TMath::RMS(stripPedestalNoise.begin(), stripPedestalNoise.end());
    stripPedestalNoise.clear();
  }
  SRS_DEBUG("SRSAPVEvent:ComputeListOfAPVHits") << "exit";
  return listOfHits;
}

void SRSAPVEvent::ComputeMeanTimeBinRawPedestalData() {
  SRS_DEBUG("SRSAPVEvent:ComputeMeanTimeBinRawPedestalData");

  fPedSubFlag = false;
  fIsCosmicRunFlag = false;

  fIsPedestalRunFlag = false;
  fIsRawPedestalRunFlag = true;

  ComputeRawData16bits();
  ComputeTimeBinCommonMode();
  fRawPedestalData.clear();

  std::vector<float> meanTimeBinRawPedestalDataVect;
  for (size_t stripNo = 0; stripNo < kNumChannels; stripNo++) {
    const auto stripSetOfTimeBinRawData = fapvTimeBinDataMap.equal_range(stripNo);
    size_t timebin = 0;
    for (auto timebin_it = stripSetOfTimeBinRawData.first; timebin_it != stripSetOfTimeBinRawData.second;
         ++timebin_it) {
      float rawdata = timebin_it->second;

      if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
        if (stripNo < 64)
          meanTimeBinRawPedestalDataVect.push_back(rawdata - fCommonModeOffsets_even[timebin]);
        else
          meanTimeBinRawPedestalDataVect.push_back(rawdata - fCommonModeOffsets_odd[timebin]);
      } else
        meanTimeBinRawPedestalDataVect.push_back(rawdata - fCommonModeOffsets[timebin]);
      timebin++;
    }
    fRawPedestalData.push_back(
        TMath::Mean(meanTimeBinRawPedestalDataVect.begin(), meanTimeBinRawPedestalDataVect.end()));
    meanTimeBinRawPedestalDataVect.clear();
  }
}

void SRSAPVEvent::ComputeMeanTimeBinPedestalData() {
  SRS_DEBUG("SRSAPVEvent:ComputeMeanTimeBinPedestalData");

  fPedSubFlag = false;
  fIsCosmicRunFlag = false;

  fIsPedestalRunFlag = true;
  fIsRawPedestalRunFlag = false;

  ComputeRawData16bits();
  ComputeTimeBinCommonMode();
  fPedestalData.clear();

  std::vector<float> meanTimeBinPedestalDataVect;
  for (size_t stripNo = 0; stripNo < kNumChannels; stripNo++) {
    const auto stripSetOfTimeBinRawData = fapvTimeBinDataMap.equal_range(stripNo);
    size_t timebin = 0;
    for (auto timebin_it = stripSetOfTimeBinRawData.first; timebin_it != stripSetOfTimeBinRawData.second;
         ++timebin_it) {
      const auto& rawdata = timebin_it->second;
      if (fReadoutBoard == "UV_ANGLE" && fDetectorType == "EICPROTO1") {
        if (stripNo < 64)
          meanTimeBinPedestalDataVect.push_back(rawdata - fCommonModeOffsets_even[timebin]);
        else
          meanTimeBinPedestalDataVect.push_back(rawdata - fCommonModeOffsets_odd[timebin]);
      } else
        meanTimeBinPedestalDataVect.push_back(rawdata - fCommonModeOffsets[timebin]);
      timebin++;
    }
    fPedestalData.push_back(TMath::Mean(meanTimeBinPedestalDataVect.begin(), meanTimeBinPedestalDataVect.end()));
    meanTimeBinPedestalDataVect.clear();
  }
}
