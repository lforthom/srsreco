#include "srsreco/SRSHit.h"
#include "srsreco/SRSMapping.h"
#include "srsreco/SRSOutputROOT.h"
#include "srsutils/Logging.h"

ClassImp(SRSOutputROOT);

SRSOutputROOT::SRSOutputROOT(TString zeroSupCutStr, TString rootDataType) {
  fZeroSupCut = zeroSupCutStr.Atoi();
  fROOTDataType = rootDataType;
}

SRSOutputROOT::~SRSOutputROOT() {
  fFile->Close();
  if (fZeroSupCut == 0)
    DeleteHitsTree();

  else {
    if (fROOTDataType == "HITS_ONLY")
      DeleteHitsTree();
    else if (fROOTDataType == "CLUSTERS_ONLY")
      DeleteClustersTree();
    else if (fROOTDataType == "TRACKING_ONLY")
      DeleteTrackingTree();
    else {
      DeleteHitsTree();
      DeleteClustersTree();
    }
  }
}

void SRSOutputROOT::DeleteHitsTree() {
  if (m_adc0)
    delete[] m_adc0;
  if (m_adc1)
    delete[] m_adc1;
  if (m_adc2)
    delete[] m_adc2;
  if (m_adc3)
    delete[] m_adc3;
  if (m_adc4)
    delete[] m_adc4;
  if (m_adc5)
    delete[] m_adc5;
  if (m_adc6)
    delete[] m_adc6;
  if (m_adc7)
    delete[] m_adc7;
  if (m_adc8)
    delete[] m_adc8;
  if (m_adc9)
    delete[] m_adc9;
  if (m_adc10)
    delete[] m_adc10;
  if (m_adc11)
    delete[] m_adc11;
  if (m_adc12)
    delete[] m_adc12;
  if (m_adc13)
    delete[] m_adc13;
  if (m_adc14)
    delete[] m_adc14;
  if (m_adc15)
    delete[] m_adc15;
  if (m_adc16)
    delete[] m_adc16;
  if (m_adc17)
    delete[] m_adc17;
  if (m_adc18)
    delete[] m_adc18;
  if (m_adc19)
    delete[] m_adc19;
  if (m_adc20)
    delete[] m_adc20;
  if (m_adc21)
    delete[] m_adc21;
  if (m_adc22)
    delete[] m_adc22;
  if (m_adc23)
    delete[] m_adc23;
  if (m_adc24)
    delete[] m_adc24;
  if (m_adc25)
    delete[] m_adc25;
  if (m_adc26)
    delete[] m_adc26;
  if (m_adc27)
    delete[] m_adc27;
  if (m_adc28)
    delete[] m_adc28;
  if (m_adc29)
    delete[] m_adc29;

  if (m_hit_timeBin)
    delete[] m_hit_timeBin;
  if (m_strip)
    delete[] m_strip;
  if (m_hit_planeID)
    delete[] m_hit_planeID;
  if (m_hit_detID)
    delete[] m_hit_detID;
  if (m_hit_etaSector)
    delete[] m_hit_etaSector;
  if (fHitTree)
    delete fHitTree;
}

void SRSOutputROOT::DeleteClustersTree() {
  if (m_clustTimeBin)
    delete[] m_clustTimeBin;
  if (m_clustPos)
    delete[] m_clustPos;
  if (m_clustSize)
    delete[] m_clustSize;
  if (m_clustADCs)
    delete[] m_clustADCs;
  if (m_planeID)
    delete[] m_planeID;
  if (m_detID)
    delete[] m_detID;
  if (m_etaSector)
    delete[] m_etaSector;
  if (fClusterTree)
    delete fClusterTree;
}

void SRSOutputROOT::DeleteTrackingTree() {
  if (m_REF1)
    delete[] m_REF1;
  if (m_REF2)
    delete[] m_REF2;
  if (m_REF3)
    delete[] m_REF3;
  if (m_REF4)
    delete[] m_REF4;
  if (m_SBS1)
    delete[] m_SBS1;
  if (m_UVAEIC)
    delete[] m_UVAEIC;
  if (fTrackingTree)
    delete fTrackingTree;
}

void SRSOutputROOT::FillRootFile(SRSEventBuilder *eventbuilder, SRSTrack *track) {
  m_evtID++;

  if (eventbuilder->IsAGoodEvent()) {
    if (fZeroSupCut == 0) {
      FillHitsTree(eventbuilder);
    } else {
      if (fROOTDataType == "HITS_ONLY") {
        FillHitsTree(eventbuilder);
      } else if (fROOTDataType == "CLUSTERS_ONLY") {
        FillClustersTree(eventbuilder);
      } else if (fROOTDataType == "TRACKING_ONLY") {
        FillTrackingTree(eventbuilder, track);
      } else {
        FillClustersTree(eventbuilder);
        FillHitsTree(eventbuilder);
      }
    }
  }
}

static Bool_t CompareStripNo(TObject *obj1, TObject *obj2) {
  Bool_t compare;
  if (((SRSHit *)obj1)->GetStripNo() < ((SRSHit *)obj2)->GetStripNo())
    compare = kTRUE;
  else
    compare = kFALSE;
  return compare;
}

void SRSOutputROOT::FillHitsTree(SRSEventBuilder *eventbuilder) {
  SRS_DEBUG("SRSOutputROOT:FillHitsTree") << "Enter";
  m_chID = 0;

  auto mapping = SRSMapping::GetInstance();

  int NbADCTimeFrames = 40;
  std::vector<float> timebinCharges;
  timebinCharges.resize(NbADCTimeFrames, 0);

  std::map<TString, std::list<SRSHit *> > hitsInDetectorPlaneMap = eventbuilder->GetHitsInDetectorPlane();
  for (const auto &detPlane : hitsInDetectorPlaneMap) {
    const auto &detPlaneName = detPlane.first;
    // Assign Detector ID
    TString detName = mapping->GetDetectorFromPlane(detPlaneName);
    const auto detID = mapping->GetDetectorIDFromDetector(detName),
               planeID = (int)(mapping->GetPlaneIDorEtaSector(detPlaneName));
    const auto etaSector = mapping->GetPlaneIDorEtaSector(detPlaneName);

    auto listOfHits = hitsInDetectorPlaneMap[detPlaneName];
    listOfHits.sort(CompareStripNo);
    for (auto *hit : listOfHits) {
      m_strip[m_chID] = hit->GetStripNo();
      m_hit_timeBin[m_chID] = hit->GetSignalPeakBinNumber();

      m_hit_detID[m_chID] = detID;
      m_hit_planeID[m_chID] = planeID;
      m_hit_etaSector[m_chID] = (short)etaSector;

      int nbOfTimeBin = hit->GetTimeBinADCs().size();
      if (nbOfTimeBin <= NbADCTimeFrames) {
        for (int t = 0; t < nbOfTimeBin; t++) {
          timebinCharges[t] = hit->GetTimeBinADCs()[t];
        }
      } else {
        for (int t = 0; t < NbADCTimeFrames; t++) {
          timebinCharges[t] = hit->GetTimeBinADCs()[t];
        }
      }

      m_adc0[m_chID] = (short)timebinCharges[0];
      m_adc1[m_chID] = (short)timebinCharges[1];
      m_adc2[m_chID] = (short)timebinCharges[2];
      m_adc3[m_chID] = (short)timebinCharges[3];
      m_adc4[m_chID] = (short)timebinCharges[4];
      m_adc5[m_chID] = (short)timebinCharges[5];
      m_adc6[m_chID] = (short)timebinCharges[6];
      m_adc7[m_chID] = (short)timebinCharges[7];
      m_adc8[m_chID] = (short)timebinCharges[8];
      m_adc9[m_chID] = (short)timebinCharges[9];
      m_adc10[m_chID] = (short)timebinCharges[10];
      m_adc11[m_chID] = (short)timebinCharges[11];
      m_adc12[m_chID] = (short)timebinCharges[12];
      m_adc13[m_chID] = (short)timebinCharges[13];
      m_adc14[m_chID] = (short)timebinCharges[14];
      m_adc15[m_chID] = (short)timebinCharges[15];
      m_adc16[m_chID] = (short)timebinCharges[16];
      m_adc17[m_chID] = (short)timebinCharges[17];
      m_adc18[m_chID] = (short)timebinCharges[18];
      m_adc19[m_chID] = (short)timebinCharges[19];
      m_adc20[m_chID] = (short)timebinCharges[20];
      m_adc21[m_chID] = (short)timebinCharges[21];
      m_adc22[m_chID] = (short)timebinCharges[22];
      m_adc23[m_chID] = (short)timebinCharges[23];
      m_adc24[m_chID] = (short)timebinCharges[24];
      m_adc25[m_chID] = (short)timebinCharges[25];
      m_adc26[m_chID] = (short)timebinCharges[26];
      m_adc27[m_chID] = (short)timebinCharges[27];
      m_adc28[m_chID] = (short)timebinCharges[28];
      m_adc29[m_chID] = (short)timebinCharges[29];
      m_chID++;
      timebinCharges.clear();
    }
    listOfHits.clear();
  }
  hitsInDetectorPlaneMap.clear();
  fHitTree->Fill();
}

void SRSOutputROOT::FillClustersTree(SRSEventBuilder *eventbuilder) {
  SRS_DEBUG("SRSOutputROOT:FillClustersTree");
  m_nclust = 0;

  auto mapping = SRSMapping::GetInstance();

  std::map<TString, std::list<SRSCluster *> > clustersInDetectorPlaneMap = eventbuilder->GetClustersInDetectorPlane();
  for (auto &detPlane : clustersInDetectorPlaneMap) {
    const auto &detPlaneName = detPlane.first;
    //Assign Detector ID
    const auto detName = mapping->GetDetectorFromPlane(detPlaneName);
    const auto detID = mapping->GetDetectorIDFromDetector(detName),
               planeID = (int)(mapping->GetPlaneIDorEtaSector(detPlaneName));
    const auto etaSector = mapping->GetPlaneIDorEtaSector(detPlaneName);

    for (auto *cluster : detPlane.second) {
      m_clustTimeBin[m_nclust] = cluster->GetClusterPeakTimeBin();
      m_clustSize[m_nclust] = cluster->GetNbOfHits();
      m_clustPos[m_nclust] = cluster->GetClusterPosition();
      m_clustADCs[m_nclust] = cluster->GetClusterADCs();

      m_planeID[m_nclust] = planeID;
      m_detID[m_nclust] = detID;
      m_etaSector[m_nclust] = (short)etaSector;
      m_nclust++;
    }
    detPlane.second.clear();
  }
  clustersInDetectorPlaneMap.clear();
  fClusterTree->Fill();
}

void SRSOutputROOT::FillTrackingTree(SRSEventBuilder *eventbuilder, SRSTrack *track) {
  SRS_DEBUG("SRSOutputROOT::FillResidualHistos") << "ENTER IN";

  for (int i = 0; i < 3; i++) {
    m_REF1[i] = -919191;
    m_REF2[i] = -919191;
    m_REF3[i] = -919191;
    m_REF4[i] = -919191;
    m_SBS1[i] = -919191;
    m_UVAEIC[i] = -919191;
  }

  if (track->IsAGoodTrack(eventbuilder)) {
    auto rawData = track->GetRawDataSpacePoints();
    for (const auto &dl : track->GetDetectorList()) {
      const auto &detector = dl.first;
      const auto &rawPoint = rawData.at(detector);
      if (rawPoint.size() == 0)
        continue;

      if (detector == "TrkGEM0") {
        for (int i = 0; i < 3; i++)
          m_REF1[i] = rawPoint[i];
      } else if (detector == "TrkGEM1") {
        for (int i = 0; i < 3; i++)
          m_REF2[i] = rawPoint[i];
      } else if (detector == "TrkGEM2") {
        for (int i = 0; i < 3; i++)
          m_REF4[i] = rawPoint[i];
      } else if (detector == "SBSGEM2") {
        for (int i = 0; i < 3; i++)
          m_REF3[i] = rawPoint[i];
      } else if (detector == "SBSGEM1") {
        for (int i = 0; i < 3; i++)
          m_SBS1[i] = rawPoint[i];
      } else if (detector == "EIC1") {
        for (int i = 0; i < 3; i++)
          m_UVAEIC[i] = rawPoint[i];
      }
    }
    fTrackingTree->Fill();
    track->GetDetectorList().clear();
    for (auto &rawdata : rawData)
      rawdata.second.clear();
    rawData.clear();
  }
}

//void SRSOutputROOT::InitRootFile(const char * cfgname) {
void SRSOutputROOT::InitRootFile() {
  TString fileName = fRunName + "_dataTree.root";

  if (fRunFilePrefix == "BeamPosition")
    fileName = fRunName + "_BP" + fRunFileValue + "_dataTree.root";
  if (fRunFilePrefix == "HVScan")
    fileName = fRunName + "_HV" + fRunFileValue + "_dataTree.root";
  SRS_INFO("SRSOutputROOT:InitRootFile") << "Init Root file '" << fileName.Data() << "'.";

  m_evtID = 0;
  m_chID = 0;
  m_nclust = 0;

  fFile = TFile::Open(fileName, "RECREATE");

  if (fZeroSupCut == 0) {
    SRS_INFO("SRSOutputROOT:InitRootFile") << "Creating the Hit Tree: fHitTree.";
    fHitTree = new TTree("THit", "GEM Hit Data Rootfile");
    fHitTree->SetDirectory(fFile);

    m_strip = new int[10000];
    m_hit_detID = new int[10000];
    m_hit_planeID = new int[10000];
    m_hit_timeBin = new int[10000];
    m_hit_etaSector = new short[10000];

    m_adc0 = new short[10000];
    m_adc1 = new short[10000];
    m_adc2 = new short[10000];
    m_adc3 = new short[10000];
    m_adc4 = new short[10000];
    m_adc5 = new short[10000];
    m_adc6 = new short[10000];
    m_adc7 = new short[10000];
    m_adc8 = new short[10000];
    m_adc9 = new short[10000];
    m_adc10 = new short[10000];
    m_adc11 = new short[10000];
    m_adc12 = new short[10000];
    m_adc13 = new short[10000];
    m_adc14 = new short[10000];
    m_adc15 = new short[10000];
    m_adc16 = new short[10000];
    m_adc17 = new short[10000];
    m_adc18 = new short[10000];
    m_adc19 = new short[10000];
    m_adc20 = new short[10000];
    m_adc21 = new short[10000];
    m_adc22 = new short[10000];
    m_adc23 = new short[10000];
    m_adc24 = new short[10000];
    m_adc25 = new short[10000];
    m_adc26 = new short[10000];
    m_adc27 = new short[10000];
    m_adc28 = new short[10000];
    m_adc29 = new short[10000];

    SRS_INFO("SRSOutputROOT:InitRootFile") << "Initialising the branches for fHitTree.";
    fHitTree->Branch("evtID", &m_evtID, "evtID/I");
    fHitTree->Branch("nch", &m_chID, "nch/I");
    fHitTree->Branch("strip", m_strip, "strip[nch]/I");
    fHitTree->Branch("hitTimebin", m_hit_timeBin, "hitTimeBin[nch]/I");

    fHitTree->Branch("adc0", m_adc0, "adc0[nch]/S");
    fHitTree->Branch("adc1", m_adc1, "adc1[nch]/S");
    fHitTree->Branch("adc2", m_adc2, "adc2[nch]/S");
    fHitTree->Branch("adc3", m_adc3, "adc3[nch]/S");
    fHitTree->Branch("adc4", m_adc4, "adc4[nch]/S");
    fHitTree->Branch("adc5", m_adc5, "adc5[nch]/S");
    fHitTree->Branch("adc6", m_adc6, "adc6[nch]/S");
    fHitTree->Branch("adc7", m_adc7, "adc7[nch]/S");
    fHitTree->Branch("adc8", m_adc8, "adc8[nch]/S");
    fHitTree->Branch("adc9", m_adc9, "adc9[nch]/S");
    fHitTree->Branch("adc10", m_adc10, "adc10[nch]/S");
    fHitTree->Branch("adc11", m_adc11, "adc11[nch]/S");
    fHitTree->Branch("adc12", m_adc12, "adc12[nch]/S");
    fHitTree->Branch("adc13", m_adc13, "adc13[nch]/S");
    fHitTree->Branch("adc14", m_adc14, "adc14[nch]/S");
    fHitTree->Branch("adc15", m_adc15, "adc15[nch]/S");
    fHitTree->Branch("adc16", m_adc16, "adc16[nch]/S");
    fHitTree->Branch("adc17", m_adc17, "adc17[nch]/S");
    fHitTree->Branch("adc18", m_adc18, "adc18[nch]/S");
    fHitTree->Branch("adc19", m_adc19, "adc19[nch]/S");
    fHitTree->Branch("adc20", m_adc20, "adc20[nch]/S");
    fHitTree->Branch("adc21", m_adc21, "adc21[nch]/S");
    fHitTree->Branch("adc22", m_adc22, "adc22[nch]/S");
    fHitTree->Branch("adc23", m_adc23, "adc23[nch]/S");
    fHitTree->Branch("adc24", m_adc24, "adc24[nch]/S");
    fHitTree->Branch("adc25", m_adc25, "adc25[nch]/S");
    fHitTree->Branch("adc26", m_adc26, "adc26[nch]/S");
    fHitTree->Branch("adc27", m_adc27, "adc27[nch]/S");
    fHitTree->Branch("adc28", m_adc28, "adc28[nch]/S");
    fHitTree->Branch("adc29", m_adc29, "adc29[nch]/S");

    fHitTree->Branch("detID", m_hit_detID, "detID[nch]/I");
    fHitTree->Branch("planeID", m_hit_planeID, "planeID[nch]/I");
    fHitTree->Branch("etaSector", m_hit_etaSector, "etaSector[nch]/S");
  }

  else {
    if (fROOTDataType == "HITS_ONLY") {
      SRS_INFO("SRSOutputROOT:InitRootFile") << "Creating the Hit Tree: fHitTree.";
      fHitTree = new TTree("THit", "GEM Hit Data Rootfile");
      fHitTree->SetDirectory(fFile);

      m_hit_detID = new int[2000];
      m_hit_planeID = new int[2000];
      m_hit_timeBin = new int[2000];
      m_hit_etaSector = new short[2000];

      m_adc0 = new short[2000];
      m_adc1 = new short[2000];
      m_adc2 = new short[2000];
      m_adc3 = new short[2000];
      m_adc4 = new short[2000];
      m_adc5 = new short[2000];
      m_adc6 = new short[2000];
      m_adc7 = new short[2000];
      m_adc8 = new short[2000];
      m_adc9 = new short[2000];
      m_adc10 = new short[2000];
      m_adc11 = new short[2000];
      m_adc12 = new short[2000];
      m_adc13 = new short[2000];
      m_adc14 = new short[2000];
      m_adc15 = new short[2000];
      m_adc16 = new short[2000];
      m_adc17 = new short[2000];
      m_adc18 = new short[2000];
      m_adc19 = new short[2000];
      m_adc20 = new short[2000];
      m_adc21 = new short[2000];
      m_adc22 = new short[2000];
      m_adc23 = new short[2000];
      m_adc24 = new short[2000];
      m_adc25 = new short[2000];
      m_adc26 = new short[2000];
      m_adc27 = new short[2000];
      m_adc28 = new short[2000];
      m_adc29 = new short[2000];
      m_strip = new int[2000];

      SRS_INFO("SRSOutputROOT:InitRootFile") << "Initialising the branches for fHitTree.";
      fHitTree->Branch("evtID", &m_evtID, "evtID/I");
      fHitTree->Branch("nch", &m_chID, "nch/I");
      fHitTree->Branch("strip", m_strip, "strip[nch]/I");
      fHitTree->Branch("hitTimebin", m_hit_timeBin, "hitTimeBin[nch]/I");

      fHitTree->Branch("adc0", m_adc0, "adc0[nch]/S");
      fHitTree->Branch("adc1", m_adc1, "adc1[nch]/S");
      fHitTree->Branch("adc2", m_adc2, "adc2[nch]/S");
      fHitTree->Branch("adc3", m_adc3, "adc3[nch]/S");
      fHitTree->Branch("adc4", m_adc4, "adc4[nch]/S");
      fHitTree->Branch("adc5", m_adc5, "adc5[nch]/S");
      fHitTree->Branch("adc6", m_adc6, "adc6[nch]/S");
      fHitTree->Branch("adc7", m_adc7, "adc7[nch]/S");
      fHitTree->Branch("adc8", m_adc8, "adc8[nch]/S");
      fHitTree->Branch("adc9", m_adc9, "adc9[nch]/S");
      fHitTree->Branch("adc10", m_adc10, "adc10[nch]/S");
      fHitTree->Branch("adc11", m_adc11, "adc11[nch]/S");
      fHitTree->Branch("adc12", m_adc12, "adc12[nch]/S");
      fHitTree->Branch("adc13", m_adc13, "adc13[nch]/S");
      fHitTree->Branch("adc14", m_adc14, "adc14[nch]/S");
      fHitTree->Branch("adc15", m_adc15, "adc15[nch]/S");
      fHitTree->Branch("adc16", m_adc16, "adc16[nch]/S");
      fHitTree->Branch("adc17", m_adc17, "adc17[nch]/S");
      fHitTree->Branch("adc18", m_adc18, "adc18[nch]/S");
      fHitTree->Branch("adc19", m_adc19, "adc19[nch]/S");
      fHitTree->Branch("adc20", m_adc20, "adc20[nch]/S");
      fHitTree->Branch("adc21", m_adc21, "adc21[nch]/S");
      fHitTree->Branch("adc22", m_adc22, "adc22[nch]/S");
      fHitTree->Branch("adc23", m_adc23, "adc23[nch]/S");
      fHitTree->Branch("adc24", m_adc24, "adc24[nch]/S");
      fHitTree->Branch("adc25", m_adc25, "adc25[nch]/S");
      fHitTree->Branch("adc26", m_adc26, "adc26[nch]/S");
      fHitTree->Branch("adc27", m_adc27, "adc27[nch]/S");
      fHitTree->Branch("adc28", m_adc28, "adc28[nch]/S");
      fHitTree->Branch("adc29", m_adc29, "adc29[nch]/S");

      fHitTree->Branch("detID", m_hit_detID, "detID[nch]/I");
      fHitTree->Branch("planeID", m_hit_planeID, "planeID[nch]/I");
      fHitTree->Branch("etaSector", m_hit_etaSector, "etaSector[nch]/S");
    }

    else if (fROOTDataType == "CLUSTERS_ONLY") {
      SRS_INFO("SRSOutputROOT:InitRootFile") << "Creating the Cluster tree: fClusterTree.";
      fClusterTree = new TTree("TCluster", "GEM Cluster Data Rootfile");
      fClusterTree->SetDirectory(fFile);

      m_clustSize = new int[200];
      m_clustTimeBin = new int[200];
      m_clustPos = new float[200];
      m_clustADCs = new float[200];
      m_detID = new int[200];
      m_planeID = new int[200];
      m_etaSector = new short[200];

      SRS_INFO("SRSOutputROOT:InitRootFile") << "Initialising the branches for fClusterTree.";
      fClusterTree->Branch("evtID", &m_evtID, "evtID/I");
      fClusterTree->Branch("nclust", &m_nclust, "nclust/I");
      fClusterTree->Branch("clustPos", m_clustPos, "clustPos[nclust]/F");
      fClusterTree->Branch("clustSize", m_clustSize, "clustSize[nclust]/I");
      fClusterTree->Branch("clustADCs", m_clustADCs, "clustADCs[nclust]/F");
      fClusterTree->Branch("clustTimebin", m_clustTimeBin, "clustTimebin[nclust]/I");

      fClusterTree->Branch("detID", m_detID, "detID[nclust]/I");
      fClusterTree->Branch("planeID", m_planeID, "planeID[nclust]/I");
      fClusterTree->Branch("etaSector", m_etaSector, "etaSector[nclust]/S");
    }

    else if (fROOTDataType == "TRACKING_ONLY") {
      SRS_INFO("SRSOutputROOT:InitRootFile") << "Creating the Tracking tree: fTrackingTree.";

      fTrackingTree = new TTree("TTracking", "Data from FLYSUB Test Beam @ FNAL: ");
      fTrackingTree->SetDirectory(fFile);

      m_REF1 = new float[3];
      m_REF2 = new float[3];
      m_REF3 = new float[3];
      m_REF4 = new float[3];
      m_SBS1 = new float[3];
      m_UVAEIC = new float[3];

      SRS_INFO("SRSOutputROOT:InitRootFile") << "Initialising the branches for fTrackingTree.";
      fTrackingTree->Branch("evtID", &m_evtID, "evtID/I");
      fTrackingTree->Branch("REF1", m_REF1, "REF1[3]/F");
      fTrackingTree->Branch("REF2", m_REF2, "REF2[3]/F");
      fTrackingTree->Branch("REF3", m_REF3, "REF3[3]/F");
      fTrackingTree->Branch("REF4", m_REF4, "REF4[3]/F");
      fTrackingTree->Branch("SBS1", m_SBS1, "SBS1[3]/F");
      fTrackingTree->Branch("UVAEIC", m_UVAEIC, "UVAEIC[3]/F");
    }

    else {
      SRS_INFO("SRSOutputROOT:InitRootFile") << "Creating the Hit Tree: fHitTree.";
      fHitTree = new TTree("THit", "GEM Hit Data Rootfile");
      fHitTree->SetDirectory(fFile);

      m_hit_detID = new int[2000];
      m_hit_planeID = new int[2000];
      m_hit_etaSector = new short[2000];
      m_hit_timeBin = new int[2000];

      m_adc0 = new short[2000];
      m_adc1 = new short[2000];
      m_adc2 = new short[2000];
      m_adc3 = new short[2000];
      m_adc4 = new short[2000];
      m_adc5 = new short[2000];
      m_adc6 = new short[2000];
      m_adc7 = new short[2000];
      m_adc8 = new short[2000];
      m_adc9 = new short[2000];
      m_adc10 = new short[2000];
      m_adc11 = new short[2000];
      m_adc12 = new short[2000];
      m_adc13 = new short[2000];
      m_adc14 = new short[2000];
      m_adc15 = new short[2000];
      m_adc16 = new short[2000];
      m_adc17 = new short[2000];
      m_adc18 = new short[2000];
      m_adc19 = new short[2000];
      m_adc20 = new short[2000];
      m_adc21 = new short[2000];
      m_adc22 = new short[2000];
      m_adc23 = new short[2000];
      m_adc24 = new short[2000];
      m_adc25 = new short[2000];
      m_adc26 = new short[2000];
      m_adc27 = new short[2000];
      m_adc28 = new short[2000];
      m_adc29 = new short[2000];
      m_strip = new int[2000];

      SRS_INFO("SRSOutputROOT:InitRootFile") << "Initialising the branches for fHitTree.";
      fHitTree->Branch("evtID", &m_evtID, "evtID/I");
      fHitTree->Branch("nch", &m_chID, "nch/I");
      fHitTree->Branch("strip", m_strip, "strip[nch]/I");
      fHitTree->Branch("hitTimebin", m_hit_timeBin, "hitTimeBin[nch]/I");

      fHitTree->Branch("adc0", m_adc0, "adc0[nch]/S");
      fHitTree->Branch("adc1", m_adc1, "adc1[nch]/S");
      fHitTree->Branch("adc2", m_adc2, "adc2[nch]/S");
      fHitTree->Branch("adc3", m_adc3, "adc3[nch]/S");
      fHitTree->Branch("adc4", m_adc4, "adc4[nch]/S");
      fHitTree->Branch("adc5", m_adc5, "adc5[nch]/S");
      fHitTree->Branch("adc6", m_adc6, "adc6[nch]/S");
      fHitTree->Branch("adc7", m_adc7, "adc7[nch]/S");
      fHitTree->Branch("adc8", m_adc8, "adc8[nch]/S");
      fHitTree->Branch("adc9", m_adc9, "adc9[nch]/S");
      fHitTree->Branch("adc10", m_adc10, "adc10[nch]/S");
      fHitTree->Branch("adc11", m_adc11, "adc11[nch]/S");
      fHitTree->Branch("adc12", m_adc12, "adc12[nch]/S");
      fHitTree->Branch("adc13", m_adc13, "adc13[nch]/S");
      fHitTree->Branch("adc14", m_adc14, "adc14[nch]/S");
      fHitTree->Branch("adc15", m_adc15, "adc15[nch]/S");
      fHitTree->Branch("adc16", m_adc16, "adc16[nch]/S");
      fHitTree->Branch("adc17", m_adc17, "adc17[nch]/S");
      fHitTree->Branch("adc18", m_adc18, "adc18[nch]/S");
      fHitTree->Branch("adc19", m_adc19, "adc19[nch]/S");
      fHitTree->Branch("adc20", m_adc20, "adc20[nch]/S");
      fHitTree->Branch("adc21", m_adc21, "adc21[nch]/S");
      fHitTree->Branch("adc22", m_adc22, "adc22[nch]/S");
      fHitTree->Branch("adc23", m_adc23, "adc23[nch]/S");
      fHitTree->Branch("adc24", m_adc24, "adc24[nch]/S");
      fHitTree->Branch("adc25", m_adc25, "adc25[nch]/S");
      fHitTree->Branch("adc26", m_adc26, "adc26[nch]/S");
      fHitTree->Branch("adc27", m_adc27, "adc27[nch]/S");
      fHitTree->Branch("adc28", m_adc28, "adc28[nch]/S");
      fHitTree->Branch("adc29", m_adc29, "adc29[nch]/S");

      fHitTree->Branch("detID", m_hit_detID, "detID[nch]/I");
      fHitTree->Branch("planeID", m_hit_planeID, "planeID[nch]/I");
      fHitTree->Branch("etaSector", m_hit_etaSector, "etaSector[nch]/S");

      SRS_INFO("SRSOutputROOT:InitRootFile") << "Creating the Cluster Tree: fClusterTree.";
      fClusterTree = new TTree("TCluster", "GEM Cluster Data Rootfile");
      fClusterTree->SetDirectory(fFile);

      m_clustSize = new int[200];
      m_clustTimeBin = new int[200];
      m_clustPos = new float[200];
      m_clustADCs = new float[200];
      m_detID = new int[200];
      m_planeID = new int[200];
      m_etaSector = new short[200];

      SRS_INFO("SRSOutputROOT:InitRootFile") << "Initialising the branches for fClusterTree.";
      fClusterTree->Branch("evtID", &m_evtID, "evtID/I");
      fClusterTree->Branch("nclust", &m_nclust, "nclust/I");
      fClusterTree->Branch("clustPos", m_clustPos, "clustPos[nclust]/F");
      fClusterTree->Branch("clustSize", m_clustSize, "clustSize[nclust]/I");
      fClusterTree->Branch("clustADCs", m_clustADCs, "clustADCs[nclust]/F");
      fClusterTree->Branch("clustTimebin", m_clustTimeBin, "clustTimeBin[nclust]/I");

      fClusterTree->Branch("detID", m_detID, "detID[nclust]/I");
      fClusterTree->Branch("planeID", m_planeID, "planeID[nclust]/I");
      fClusterTree->Branch("etaSector", m_etaSector, "etaSector[nclust]/S");
    }
  }
}

void SRSOutputROOT::WriteRootFile() {
  if (fZeroSupCut == 0)
    fHitTree->Write();

  else {
    if (fROOTDataType == "HITS_ONLY")
      fHitTree->Write();
    else if (fROOTDataType == "CLUSTERS_ONLY")
      fClusterTree->Write();
    else if (fROOTDataType == "TRACKING_ONLY")
      fTrackingTree->Write();
    else {
      fHitTree->Write();
      fClusterTree->Write();
    }
  }
  fFile->Write();
}
