#include <TCanvas.h>
#include <TColor.h>
#include <TFile.h>
#include <TMath.h>
#include <TStyle.h>

#include <sstream>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSPedestal.h"
#include "srsutils/Logging.h"

ClassImp(SRSPedestal);

SRSPedestal::SRSPedestal(int nbOfAPVs, int chMaskCut) : fNbOfAPVs(nbOfAPVs), fChMaskCut(chMaskCut) { Init(); }

SRSPedestal::~SRSPedestal() {
  Clear();
  ClearMaps();
}

void SRSPedestal::Clear(Option_t *) {
  if (!fRMSDist.empty())
    fRMSDist.clear();

  if (!fNoises.empty())
    fNoises.clear();

  if (!fOffsets.empty())
    fOffsets.clear();

  if (!fMaskedChannels.empty())
    fMaskedChannels.clear();
  if (!fPedHistos.empty())
    fPedHistos.clear();
  if (!fPed2DHistos.empty())
    fPed2DHistos.clear();
}

TString SRSPedestal::GetHistoName(int apvKey, TString dataType, TString dataNb) {
  auto mapping = SRSMapping::GetInstance();

  int apvID = mapping->GetAPVIDFromAPVNo(apvKey);
  int fecID = mapping->GetFECIDFromAPVID(apvID);
  int adcCh = mapping->GetADCChannelFromAPVID(apvID);

  TString apvName = mapping->GetAPVFromID(apvID);
  std::ostringstream out;

  out << apvID;
  TString apvIDStr = out.str();
  out.str("");

  out << fecID;
  TString fecIDStr = out.str();
  out.str("");

  out << adcCh;
  TString adcChStr = out.str();
  out.str("");

  out << apvKey;
  TString apvNoStr = out.str();
  out.str("");

  TString histoName =
      dataType + dataNb + "apvNo" + apvNoStr + apvName + "_Id" + apvIDStr + "_adcCh" + adcChStr + "_FecId" + fecIDStr;
  return histoName;
}

void SRSPedestal::ClearMaps() { fPedestalData.clear(); }

void SRSPedestal::Reset() { Clear(); }

void SRSPedestal::Init() {
  Clear();
  fIsFirstEvent = true;

  auto mapping = SRSMapping::GetInstance();

  //int nbOfPlanes = mapping->GetNbOfDetectorPlane();

  for (int apvKey = 0; apvKey < fNbOfAPVs; apvKey++) {
    fNoises.push_back(BookHistos(apvKey, "noise_", ""));
    fOffsets.push_back(BookHistos(apvKey, "offset_", ""));
    fMaskedChannels.push_back(BookHistos(apvKey, "maskedCh_", ""));
    fPed2DHistos.push_back(Book2DHistos(apvKey));
  }

  fRMSDist.push_back(new TH1F("allstripsAPVsPedestalRMSDist", "allstripsAPVsPedestalRMSDist", 1000, 0, 100));
  fRMSDist.push_back(new TH1F("allXstripsAPVsPedestalRMSDist", "allXstripsAPVsPedestalRMSDist", 1000, 0, 100));
  fRMSDist.push_back(new TH1F("allYstripsAPVsPedestalRMSDist", "allYstripsAPVsPedestalRMSDist", 1000, 0, 100));

  for (size_t chNo = 0; chNo < SRSAPVEvent::kNumChannels; chNo++) {
    for (int apvKey = 0; apvKey < fNbOfAPVs; apvKey++) {
      std::ostringstream out;
      out << chNo;
      TString chNoStr = out.str();
      fPedHistos.push_back(BookHistos(apvKey, "hped_", chNoStr));
    }
  }
  SRS_INFO("SRSPedestal:Init") << "leaving Pedestal init.";
}

TH1F *SRSPedestal::BookHistos(int apvKey, TString dataType, TString dataNb) {
  std::ostringstream out;
  out << apvKey;
  TString apvKeyStr = out.str();

  float min = -0.5;
  float max = 127.5;
  int nbin = 128;

  TString histoName = GetHistoName(apvKey, dataType, dataNb);

  if (dataType.Contains("hped")) {
    min = -2048;
    max = 2048;
    nbin = 4097;
  }

  TH1F *h = new TH1F(histoName, histoName, nbin, min, max);
  //h->StatOverflows(true);
  return h;
}

TH2F *SRSPedestal::Book2DHistos(int apvKey) {
  std::ostringstream out;
  out << apvKey;
  TString apvKeyStr = out.str();

  float min = -0.5;
  float max = 127.5;
  int nbin = 128;

  TString pedName = "ped2D_apvNo" + apvKeyStr;

  min = 0;
  max = 100;
  nbin = 101;

  TH2F *h = new TH2F(pedName, pedName, 128, 0, 127, nbin, min, max);
  //h->StatOverflows(true);
  return h;
}

void SRSPedestal::FillPedestalHistos(SRSFECPedestalDecoder *pedestalDecoder, SRSRawPedestal *rawped) {
  fEventNb++;
  auto mapping = SRSMapping::GetInstance();

  TList *listOfAPVEvents = pedestalDecoder->GetFECEvents();
  TIter nextAPVEvent(listOfAPVEvents);

  while (SRSAPVEvent *apvEvent = (SRSAPVEvent *)nextAPVEvent()) {
    int apvID = apvEvent->GetAPVID();
    int apvKey = mapping->GetAPVNoFromID(apvID);

    apvEvent->SetRawPedestals(rawped->GetAPVNoises(apvID), rawped->GetAPVOffsets(apvID));
    apvEvent->ComputeMeanTimeBinPedestalData();

    int chNo = 0;
    fPedestalData = apvEvent->GetPedestalData();
    for (const auto &data : fPedestalData) {
      fPedHistos[SRSAPVEvent::kNumChannels * apvKey + chNo]->Fill(data);
      int stripNo = apvEvent->StripMapping(chNo);
      fPed2DHistos[apvKey]->Fill(stripNo, fEventNb, data);
      ++chNo;
    }
  }
  listOfAPVEvents->Delete();
}

void SRSPedestal::ComputePedestalData() {
  auto mapping = SRSMapping::GetInstance();
  int nbAPVs = mapping->GetNbOfAPVs();
  SRS_INFO("SRSPedestal:ComputePedestal") << "Compute the pedestals for " << nbAPVs << " APVs.";
  for (int apvKey = 0; apvKey < nbAPVs; apvKey++) {
    SRS_DEBUG("SRSPedestal:ComputePedestal") << "Compute the pedestals for APVs=" << apvKey << ".";

    for (size_t chNo = 0; chNo < SRSAPVEvent::kNumChannels; chNo++) {
      float offset = fPedHistos[SRSAPVEvent::kNumChannels * apvKey + chNo]->GetMean();
      float noise = fPedHistos[SRSAPVEvent::kNumChannels * apvKey + chNo]->GetRMS();
      fNoises[apvKey]->Fill(chNo, noise);
      fOffsets[apvKey]->Fill(chNo, offset);
      int apvID = mapping->GetAPVIDFromAPVNo(apvKey);
      TString apvName = mapping->GetAPVFromID(apvID);
      fRMSDist[0]->Fill(noise);

      //      if(apvName.Contains("X")) fXorUstripsRMSDist->Fill(noise) ;
      //      if(apvName.Contains("Y")) fYorVstripsRMSDist->Fill(noise) ;
      if (apvName.Contains("X") || apvName.Contains("U")) {
        if (apvName.Contains("X")) {
          fRMSDist[1]->SetName("allXstripsPedestalRMSDist");
          fRMSDist[1]->SetTitle("allXstripsPedestalRMSDist");
        }
        if (apvName.Contains("U")) {
          fRMSDist[1]->SetName("allUstripsPedestalRMSDist");
          fRMSDist[1]->SetTitle("allUstripsPedestalRMSDist");
        }
        fRMSDist[1]->Fill(noise);
      }

      if (apvName.Contains("Y") || apvName.Contains("V")) {
        if (apvName.Contains("Y")) {
          fRMSDist[2]->SetName("allYstripsPedestalRMSDist");
          fRMSDist[2]->SetTitle("allYstripsPedestalRMSDist");
        }
        if (apvName.Contains("V")) {
          fRMSDist[2]->SetName("allVstripsPedestalRMSDist");
          fRMSDist[2]->SetTitle("allVstripsPedestalRMSDist");
        }
        fRMSDist[2]->Fill(noise);
      }
      SRS_DEBUG("SRSPedestal:ComputePedestal")
          << "apvKey=" << apvKey << ", chNo=" << chNo << ", noise=" << noise << ".";
    }
    fNoises[apvKey]->Write();
    fOffsets[apvKey]->Write();
  }
  fRMSDist[0]->Write();
  fRMSDist[1]->Write();
  fRMSDist[2]->Write();

  fIsPedestalComputed = true;
}

void SRSPedestal::ComputeMaskedChannels() {
  float meanNoise = fRMSDist[0]->GetMean();
  auto mapping = SRSMapping::GetInstance();
  int nbAPVs = mapping->GetNbOfAPVs();
  SRS_INFO("SRSPedestal:ComputeMaskedChannels") << "Compute the masked channels for " << nbAPVs << " APVs.";

  std::map<int, std::vector<float> > meanDetectorStripNoise;
  meanDetectorStripNoise.clear();

  const auto apvIDListFromDetNameMap = mapping->GetAPVIDListFromDetectorMap();
  for (const auto &det : apvIDListFromDetNameMap) {
    for (const auto &apv : det.second) {
      int apvKey = mapping->GetAPVNoFromID(apv);
      for (size_t chNo = 1; chNo <= SRSAPVEvent::kNumChannels; chNo++)
        meanDetectorStripNoise[apvKey].push_back(fNoises[apvKey]->GetBinContent(chNo));
    }
  }

  for (const auto &det : apvIDListFromDetNameMap) {
    const auto &detName = det.first;
    for (const auto &apv : det.second) {
      const auto apvKey = mapping->GetAPVNoFromID(apv);
      meanNoise = TMath::Mean(meanDetectorStripNoise[apvKey].begin(), meanDetectorStripNoise[apvKey].end());
      for (size_t chNo = 0; chNo < SRSAPVEvent::kNumChannels; chNo++) {
        int binNumber = chNo + 1;
        float noise = fNoises[apvKey]->GetBinContent(binNumber);
        fMaskedChannels[apvKey]->Fill(chNo, 0);
        float cut = (float)(fChMaskCut * meanNoise);

        if (cut > 0 && noise > cut) {
          fMaskedChannels[apvKey]->Fill(chNo, 1);
          SRS_INFO("SRSPedestal:ComputeMaskedChannels")
              << "detName = '" << detName.Data() << "', apvNo = " << apvKey << ", noise = " << noise
              << ", meanNoise = " << meanNoise << ", cut = " << cut << ".";
        }
      }
      fMaskedChannels[apvKey]->Write();
    }
    meanDetectorStripNoise.clear();
  }
  fIsMaskedChComputed = true;
}

float SRSPedestal::GetOnlinePedestalMean(int apvID, int chNo) {
  auto mapping = SRSMapping::GetInstance();
  int apvKey = mapping->GetAPVNoFromID(apvID);
  return fPedHistos[SRSAPVEvent::kNumChannels * apvKey + chNo]->GetMean();
}

float SRSPedestal::GetOnlinePedestalRMS(int apvID, int chNo) {
  auto mapping = SRSMapping::GetInstance();
  int apvKey = mapping->GetAPVNoFromID(apvID);
  return fPedHistos[SRSAPVEvent::kNumChannels * apvKey + chNo]->GetRMS();
}

void SRSPedestal::LoadPedestalData(const char *filename) {
  TFile f(filename);
  auto mapping = SRSMapping::GetInstance();
  int nbAPVs = mapping->GetNbOfAPVs();
  SRS_INFO("SRSPedestal:LoadPedestalData")
      << "Compute the pedestals from '" << filename << "' for " << nbAPVs << " APVs.";

  for (int apvKey = 0; apvKey < nbAPVs; apvKey++) {
    std::ostringstream out;
    out << apvKey;
    TString apvKeyStr = out.str();

    //The pedestals data
    TString noiseName = GetHistoName(apvKey, "noise_", "");
    TString offsetName = GetHistoName(apvKey, "offset_", "");
    TString maskedChName = GetHistoName(apvKey, "maskedCh_", "");

    auto *noiseHisto = f.Get<TH1F>(noiseName);
    auto *offsetHisto = f.Get<TH1F>(offsetName);
    auto *maskedChHisto = f.Get<TH1F>(maskedChName);

    for (size_t chNo = 0; chNo < SRSAPVEvent::kNumChannels; chNo++) {
      int binNumber = chNo + 1;  // This is an issue with ROOT Histo bin numbering with:
      // ==========================================//
      // bin = 0 is underflow bin                  //
      // bin = 1 is the first bin of the histogram //
      // bin = nbin is the last bin                //
      // bin = nbin + 1 is the overflow bin        //
      // ==========================================//
      const auto noise = noiseHisto->GetBinContent(binNumber), offset = offsetHisto->GetBinContent(binNumber),
                 maskedCh = maskedChHisto->GetBinContent(binNumber);
      fNoises[apvKey]->Fill(chNo, noise);
      fOffsets[apvKey]->Fill(chNo, offset);
      fMaskedChannels[apvKey]->Fill(chNo, maskedCh);
    }
  }
  fIsMaskedChComputed = true;
  fIsPedestalComputed = true;
  f.Close();
}

float SRSPedestal::GetMaskedChannelStatus(int apvID, int chNo) {
  auto mapping = SRSMapping::GetInstance();
  if (!fIsPedestalComputed) {
    SRS_WARNING("SRSPedestal:GetMaskedChannelStatus") << "Pedestals & Noise not yet computed.";
    return 0;
  }
  if (!fIsMaskedChComputed) {
    SRS_WARNING("SRSPedestal:GetMaskedChannelStatus") << "masked channel not yet computed.";
    return 0;
  }
  int apvKey = mapping->GetAPVNoFromID(apvID);
  int binNumber = chNo + 1;  // This is an issue with ROOT Histo bin numbering with:
  // ==========================================//
  // bin = 0 is underflow bin                  //
  // bin = 1 is the first bin of the histogram //
  // bin = nbin is the last bin                //
  // bin = nbin + 1 is the overflow bin        //
  // ==========================================//
  return fMaskedChannels[apvKey]->GetBinContent(binNumber);
}

float SRSPedestal::GetNoise(int apvID, int chNo) {
  auto mapping = SRSMapping::GetInstance();
  if (!fIsPedestalComputed) {
    SRS_WARNING("SRSPedestal:GetNoise") << "Pedestals & Noise not yet computed.";
    return 0;
  }
  int apvKey = mapping->GetAPVNoFromID(apvID);
  int binNumber = chNo + 1;  // This is an issue with ROOT Histo bin numbering with:
  // ==========================================//
  // bin = 0 is underflow bin                  //
  // bin = 1 is the first bin of the histogram //
  // bin = nbin is the last bin                //
  // bin = nbin + 1 is the overflow bin        //
  // ==========================================//
  return fNoises[apvKey]->GetBinContent(binNumber);
}

float SRSPedestal::GetOffset(int apvID, int chNo) {
  auto mapping = SRSMapping::GetInstance();
  if (!fIsPedestalComputed) {
    SRS_WARNING("SRSPedestal:GetPedestal") << "Pedestals & Noises not yet computed.";
    return 0;
  }

  int apvKey = mapping->GetAPVNoFromID(apvID);
  int binNumber = chNo + 1;  // This is an issue with ROOT Histo bin numbering with:
  // ==========================================//
  // bin = 0 is underflow bin                  //
  // bin = 1 is the first bin of the histogram //
  // bin = nbin is the last bin                //
  // bin = nbin + 1 is the overflow bin        //
  // ==========================================//
  return fOffsets[apvKey]->GetBinContent(binNumber);
}

std::vector<float> SRSPedestal::GetAPVNoises(int apvID) {
  auto mapping = SRSMapping::GetInstance();
  std::vector<float> apvNoises;
  if (!fIsPedestalComputed) {
    SRS_WARNING("SRSPedestal:GetAPVNoise") << "Pedestals & Noise not yet computed.";
    for (size_t chNo = 1; chNo <= SRSAPVEvent::kNumChannels; ++chNo)
      apvNoises.push_back(0);
  }
  int apvKey = mapping->GetAPVNoFromID(apvID);
  for (size_t chNo = 1; chNo <= SRSAPVEvent::kNumChannels; ++chNo)
    apvNoises.push_back(fNoises[apvKey]->GetBinContent(chNo));
  SRS_DEBUG("SRSPedestal:GetAPVNoises") << "Got the pedestals.";
  return apvNoises;
}

std::vector<float> SRSPedestal::GetAPVOffsets(int apvID) {
  auto mapping = SRSMapping::GetInstance();
  std::vector<float> apvOffsets(SRSAPVEvent::kNumChannels, 0.);
  if (!fIsPedestalComputed)
    SRS_WARNING("SRSPedestal:GetAPVOffsets") << "Pedestals & Noises not yet computed.";
  int apvKey = mapping->GetAPVNoFromID(apvID);
  for (size_t chNo = 0; chNo < SRSAPVEvent::kNumChannels; ++chNo)
    apvOffsets[chNo] = fOffsets[apvKey]->GetBinContent(chNo + 1);
  SRS_DEBUG("SRSPedestal:GetAPVOffsets") << "Got the pedestals.";
  return apvOffsets;
}

std::vector<float> SRSPedestal::GetAPVMaskedChannels(int apvID) {
  auto mapping = SRSMapping::GetInstance();
  std::vector<float> apvMaskedChannels(SRSAPVEvent::kNumChannels, 0.);
  if (!fIsMaskedChComputed)
    SRS_WARNING("SRSPedestal:GetAPVMaskedChannels") << "Pedestals & Noises not yet computed.";
  int apvKey = mapping->GetAPVNoFromID(apvID);
  for (size_t chNo = 1; chNo <= SRSAPVEvent::kNumChannels; ++chNo)
    apvMaskedChannels.push_back(fMaskedChannels[apvKey]->GetBinContent(chNo));
  SRS_DEBUG("SRSPedestal:GetAPVMaskedChannels") << "Got the pedestals.";
  return apvMaskedChannels;
}

TH1F *SRSPedestal::GetPedHisto(int apvID, int chNo) {
  auto mapping = SRSMapping::GetInstance();
  if (!fIsPedestalComputed) {
    SRS_WARNING("GetPedestal:GetPedHisto") << "Pedestals not yet computed.";
    return 0;
  }
  int apvKey = mapping->GetAPVNoFromID(apvID);
  return fPedHistos[SRSAPVEvent::kNumChannels * apvKey + chNo];
}

SRSPedestal *SRSPedestal::GetPedestalRootFile(const char *filename) {
  TFile *f = new TFile(filename, "read");
  SRSPedestal *pedestalToUse = (SRSPedestal *)f->Get("SRSPedestal");
  SRS_DEBUG("SRSPedestal:GetPedestalRootFile") << "load pedestal root file '" << filename << "'.";
  f->Close();
  return pedestalToUse;
}

void SRSPedestal::SavePedestalRunHistos() {
  ComputePedestalData();
  ComputeMaskedChannels();

  auto mapping = SRSMapping::GetInstance();
  int nbAPVs = mapping->GetNbOfAPVs();

  SRS_INFO("SRSPedestal:SavePedestalRunHistos") << "Get Mapping.";

  TCanvas c("c1", "c1", 80, 80, 1200, 600);
  c.cd();

  TString distname = fRMSDist[0]->GetName();
  //  TString distName = fRunName + "pedRMSDist.png" ;
  TString distName = fRunName + "_" + distname + ".png";
  fRMSDist[0]->Draw("");
  fRMSDist[0]->SetYTitle("Frequency");
  fRMSDist[0]->SetXTitle("Pedestal RMS (ADC count)");
  gStyle->SetOptStat(1111);
  c.SaveAs(distName);

  distname = fRMSDist[1]->GetName();
  //  distName = fRunName + "pedYStripsRMSDist.png" ;
  distName = fRunName + "_" + distname + ".png";
  fRMSDist[1]->Draw("");
  fRMSDist[1]->SetYTitle("Frequency");
  fRMSDist[1]->SetXTitle("Y-Strips pedestal RMS (ADC count)");
  gStyle->SetOptStat(1111);
  c.SaveAs(distName);

  distname = fRMSDist[2]->GetName();
  //  distName = fRunName + "pedXStripsRMSDist.png" ;
  distName = fRunName + "_" + distname + ".png";
  fRMSDist[2]->Draw("");
  fRMSDist[2]->SetYTitle("Frequency");
  fRMSDist[2]->SetXTitle("X-Strips pedestal RMS (ADC count)");
  gStyle->SetOptStat(1111);
  c.SaveAs(distName);

  SRS_INFO("SRSPedestal:SavePedestalRunHistos") << "save ped dist plots.";

  GetStyle();

  for (int apvKey = 0; apvKey < nbAPVs; apvKey++) {
    TString histoname = fNoises[apvKey]->GetName();
    TString picturename = fRunName + "_ped_" + histoname + ".png";
    fNoises[apvKey]->Draw("");
    fNoises[apvKey]->UseCurrentStyle();
    fNoises[apvKey]->SetXTitle("APV Channel No");
    fNoises[apvKey]->SetYTitle("Pedestal Noise  (ADC counts)");
    c.SaveAs(picturename);
    /*
		 histoname = fPed2DHistos[apvKey]->GetName() ;
		 picturename = fRunName + "_ped_" +  histoname + ".png" ;
		 fPed2DHistos[apvKey]->Draw("LEGO2") ;
		 fPed2DHistos[apvKey]->SetXTitle("APV Channel No") ;
		 fPed2DHistos[apvKey]->SetYTitle("Event No") ;
		 fPed2DHistos[apvKey]->SetZTitle("ADC charge (A.U.)") ;
		 c.SaveAs(picturename) ;

		 histoname = fOffsets[apvKey]->GetName() ;
		 picturename = fRunName + "_ped_" +  histoname + ".png" ;
		 fOffsets[apvKey]->Draw("") ;
		 fOffsets[apvKey]->UseCurrentStyle() ;
		 fOffsets[apvKey]->SetXTitle("APV Channel No") ;
		 fOffsets[apvKey]->SetYTitle("Pedestal Offset  (ADC counts)") ;
		 c.SaveAs(picturename) ;

		 histoname = fMaskedChannels[apvKey]->GetName() ;
		 picturename = fRunName + "_ped_" +  histoname + ".png" ;
		 fMaskedChannels[apvKey]->Draw("") ;
		 fMaskedChannels[apvKey]->UseCurrentStyle() ;
		 fMaskedChannels[apvKey]->SetXTitle("APV Channel No") ;
		 fMaskedChannels[apvKey]->SetYTitle("maskChannels") ;
		 c.SaveAs(picturename) ;
		 */
  }
}

void SRSPedestal::GetStyle() {
  gStyle->SetOptStat(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetCanvasBorderMode(0);

  gStyle->SetLabelFont(62, "xyz");
  gStyle->SetLabelSize(0.03, "xyz");
  gStyle->SetLabelColor(1, "xyz");
  gStyle->SetTitleBorderSize(0);
  gStyle->SetTitleFillColor(0);
  gStyle->SetTitleSize(0.05, "xyz");
  gStyle->SetTitleOffset(1., "xy");
  gStyle->SetTitleOffset(1., "z");
  gStyle->SetPalette(1);

  const int NRGBs = 5;
  const int NCont = 32;
  double stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
  double red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
  double green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
  double blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}
