#include "srsreco/Utils.h"
#include "srsutils/Logging.h"

namespace srsreco {
  namespace utils {
    std::vector<float> getDirection(const std::vector<float>& u) {
      if (const auto size = u.size(); size != 3)
        SRS_WARNING("srs:utils:getDirection")
            << "point vector's size = " << size << ",!= 3  !!! Check cluster Multiplicity !!!";
      return std::vector<float>{
          u[0] / normVec(u),  // x
          u[1] / normVec(u),  // y
          u[2] / normVec(u)   // z
      };
    }

    float projectedAngleXY(const std::vector<float>& u, TString xORy) {
      const auto v = getDirection(u);
      auto vz = v[2];
      const auto vproj = (xORy == "X" ? v[0] : v[1]), vplane = std::hypot(vz, vproj);
      if (vplane <= 0.)
        return 0.;
      vz /= vplane;
      auto projAngle = std::acos(-vz);
      if (vproj < 0.)
        projAngle *= -1.;
      return projAngle;
    }

    float projectedAngleXZ(const std::vector<float>& u, TString xORz) {
      const auto v = getDirection(u);
      auto vy = v[1];
      const auto vproj = (xORz == "X" ? v[0] : v[2]), vplane = std::hypot(vy, vproj);
      if (vplane <= 0.)
        return 0.;
      vy /= vplane;
      auto projAngle = std::acos(-vy);
      if (vproj < 0.)
        projAngle *= -1.;
      return projAngle;
    }

    float normVec(const std::vector<float>& u) {
      if (const auto size = u.size(); size != 3)
        SRS_WARNING("srsreco:utils:normVec") << "point vector's size = " << size << ",!= 3.";
      return std::sqrt(dotVec(u, u));
    }

    float dotVec(const std::vector<float>& u, const std::vector<float>& v) {
      if (const auto sizeU = u.size(); sizeU != 3)
        SRS_WARNING("srsreco:utils:dotVec") << "U point vector's size = " << sizeU << ",!= 3.";
      if (const auto sizeV = v.size(); sizeV != 3)
        SRS_WARNING("srsreco:utils:dotVec") << "V point vector's size = " << sizeV << ",!= 3.";
      return v[0] * u[0] + v[1] * u[1] + v[2] * u[2];
    }

    std::vector<float> subVec(const std::vector<float>& u, const std::vector<float>& v) {
      if (const auto sizeU = u.size(); sizeU != 3)
        SRS_WARNING("srsreco:utils:subVec") << "U point vector's size = " << sizeU << ",!= 3.";
      if (const auto sizeV = v.size(); sizeV != 3)
        SRS_WARNING("srsreco:utils:subVec") << "V point vector's size = " << sizeV << ",!= 3.";
      return std::vector<float>{v[0] - u[0], v[1] - u[1], v[2] - u[2]};
    }

    std::vector<float> addVec(const std::vector<float>& u, const std::vector<float>& v) {
      if (const auto sizeU = u.size(); sizeU != 3)
        SRS_WARNING("srsreco:utils:addVec") << "U point vector's size = " << sizeU << ",!= 3.";
      if (const auto sizeV = v.size(); sizeV != 3)
        SRS_WARNING("srsreco:utils:addVec") << "V point vector's size = " << sizeV << ",!= 3.";
      return std::vector<float>{u[0] + v[0], u[1] + v[1], u[2] + v[2]};
    }

    std::vector<float> prodVec(float a, const std::vector<float> u) {
      if (const auto sizeU = u.size(); sizeU != 3)
        SRS_WARNING("srsreco:utils:prodVec") << "U point vector's size = " << sizeU << ",!= 3.";
      return std::vector<float>{a * u[0], a * u[1], a * u[2]};
    }

    std::vector<float> directionVectorFrom2Points(const std::vector<float>& u, const std::vector<float>& v) {
      if (const auto sizeU = u.size(); sizeU != 3)
        SRS_WARNING("srsreco:utils:directionVectorFrom2Points") << "U point vector's size = " << sizeU << ",!= 3.";
      if (const auto sizeV = v.size(); sizeV != 3)
        SRS_WARNING("srsreco:utils:directionVectorFrom2Points") << "V point vector's size = " << sizeV << ",!= 3.";
      const auto sub = std::vector<float>{v[0] - u[0], v[1] - u[1], v[2] - u[2]};
      const auto norm = normVec(sub);
      return std::vector<float>{sub[0] / norm, sub[1] / norm, sub[2] / norm};
    }

    float getAngleTo(const std::vector<float>& u, const std::vector<float>& v) {
      if (const auto sizeU = u.size(); sizeU != 3)
        SRS_WARNING("srsreco:utils:getAngleTo") << "U point vector's size = " << sizeU << ",!= 3.";
      if (const auto sizeV = v.size(); sizeV != 3)
        SRS_WARNING("srsreco:utils:getAngleTo") << "V point vector's size = " << sizeV << ",!= 3.";
      const auto argument = dotVec(u, v) / (normVec(u) * normVec(v));
      float angle = std::acos(argument);
      if (angle != angle)  // deal with NaN
        return 0.;
      return angle;
    }

    std::vector<float> getXandYKnowingZ(const std::vector<float>& w, const std::vector<float>& v, float z0) {
      const auto d = directionVectorFrom2Points(v, w);
      const float t0 = (w[2] - z0) / d[2];
      return std::vector<float>{w[0] - d[0] * t0,  // x0
                                w[1] - d[1] * t0,  // y0
                                z0};
    }

    float convertRadianToDegree(float angle_rad) { return float(180. * angle_rad * M_1_PI); }

    float convertDegreeToRadian(float angle_deg) { return float(M_PI * angle_deg / 180.); }
  }  // namespace utils
}  // namespace srsreco
