// Author: Kondo GNANVO 01/05/2011
#include "srsreco/SRSTrackFit.h"
#include "srsreco/Utils.h"
#include "srsutils/Logging.h"

SRSTrackFit::SRSTrackFit(std::map<TString, std::vector<float> > trackerData,
                         std::map<TString, std::vector<float> > rawData)
    : fTrackerData(trackerData), fRawData(rawData) {
  ClearTracks();
  WeightedLeastSquareLinearFitTrack();
}

SRSTrackFit::~SRSTrackFit() { ClearTracks(); }

/*void SRSTrackFit::LeastSquareLinearFitTrack() {
  float sumx = 0, sumy = 0, sumz = 0;
  float squareSumz = 0;
  float crossSumxz = 0, crossSumyz = 0;
  const auto n = fTrackerData.size();

  for (const auto& tracker : fTrackerData) {
    const auto& trackerName = tracker.first;
    const auto& point = tracker.second;
    float x_coordinate = point[0];
    float y_coordinate = point[1];
    float z_coordinate = point[2];

    sumx += x_coordinate;
    sumy += y_coordinate;
    sumz += z_coordinate;
    squareSumz += z_coordinate * z_coordinate;
    crossSumxz += x_coordinate * z_coordinate;
    crossSumyz += y_coordinate * z_coordinate;
  }

  fFitParameters["xOffset"] = (sumz * crossSumxz - sumx * squareSumz) / (sumz * sumz - n * squareSumz);
  fFitParameters["yOffset"] = (sumz * crossSumyz - sumy * squareSumz) / (sumz * sumz - n * squareSumz);
  fFitParameters["xDirection"] = (sumz * sumx - n * crossSumxz) / (sumz * sumz - n * squareSumz);
  fFitParameters["yDirection"] = (sumz * sumy - n * crossSumyz) / (sumz * sumz - n * squareSumz);

  for (const auto& track : fTrackerData) {
    const auto& trackerName = track.first;
    const auto& point = track.second;
    if (const auto size = fTrackFittedData[trackerName].size(); size != 0)
      fTrackFittedData[trackerName].clear();

    fTrackFittedData[trackerName].push_back(fFitParameters["xOffset"] + fFitParameters["xDirection"] * point[2]);
    fTrackFittedData[trackerName].push_back(fFitParameters["yOffset"] + fFitParameters["yDirection"] * point[2]);
    fTrackFittedData[trackerName].push_back(point[2]);
  }

  for (const auto& res : fRawData) {
    const auto& trackerName = res.first;
    const auto& point = res.second;
    if (const auto size = fTrackFittedData[trackerName].size(); size != 0)
      fTrackFittedData[trackerName].clear();
    fTrackFittedData[trackerName].push_back(fFitParameters["xOffset"] + fFitParameters["xDirection"] * point[2]);
    fTrackFittedData[trackerName].push_back(fFitParameters["yOffset"] + fFitParameters["yDirection"] * point[2]);
    fTrackFittedData[trackerName].push_back(point[2]);
  }
}*/

void SRSTrackFit::WeightedLeastSquareLinearFitTrack() {
  float sumx = 0, sumxz = 0, squareSumxz = 0, crossSumxz = 0, sumxRes = 0;
  float sumy = 0, sumyz = 0, squareSumyz = 0, crossSumyz = 0, sumyRes = 0;
  float xresol = 1, yresol = 1;

  //const auto n = fTrackerData.size();
  for (const auto& tracker : fTrackerData) {
    const auto& trackerName = tracker.first;
    const auto& point = tracker.second;

    TString xplane = trackerName + "X";
    TString yplane = trackerName + "Y";

    float x_coordinate = point[0];
    float y_coordinate = point[1];
    float z_coordinate = point[2];

    sumx += x_coordinate / (xresol * xresol);
    sumxz += z_coordinate / (xresol * xresol);
    squareSumxz += (z_coordinate * z_coordinate) / (xresol * xresol);
    crossSumxz += (x_coordinate * z_coordinate) / (xresol * xresol);
    sumxRes += 1 / (xresol * xresol);

    sumy += y_coordinate / (yresol * yresol);
    sumyz += z_coordinate / (yresol * yresol);
    squareSumyz += (z_coordinate * z_coordinate) / (yresol * yresol);
    crossSumyz += (y_coordinate * z_coordinate) / (yresol * yresol);
    sumyRes += 1 / (yresol * yresol);
  }

  fFitParameters["xOffset"] = (sumxz * crossSumxz - sumx * squareSumxz) / (sumxz * sumxz - sumxRes * squareSumxz);
  fFitParameters["yOffset"] = (sumyz * crossSumyz - sumy * squareSumyz) / (sumyz * sumyz - sumyRes * squareSumyz);
  fFitParameters["xDirection"] = (sumxz * sumx - sumxRes * crossSumxz) / (sumxz * sumxz - sumxRes * squareSumxz);
  fFitParameters["yDirection"] = (sumyz * sumy - sumyRes * crossSumyz) / (sumyz * sumyz - sumyRes * squareSumyz);

  for (const auto& track : fTrackerData) {
    const auto& trackerName = track.first;
    const auto& point = track.second;
    //    int size =  fTrackFittedData[trackerName].size() ;
    //    if (size != 0)  fTrackFittedData[trackerName].clear() ;
    fTrackFittedData[trackerName].clear();
    fTrackFittedData[trackerName].push_back(fFitParameters["xOffset"] + fFitParameters["xDirection"] * point.at(2));
    fTrackFittedData[trackerName].push_back(fFitParameters["yOffset"] + fFitParameters["yDirection"] * point.at(2));
    fTrackFittedData[trackerName].push_back(point.at(2));
  }
  for (const auto& res : fRawData) {
    const auto& trackerName = res.first;
    const auto& point = res.second;
    //    int size =  fTrackFittedData[trackerName].size() ;
    //    if (size != 0)  fTrackFittedData[trackerName].clear() ;
    fTrackFittedData[trackerName].clear();
    fTrackFittedData[trackerName].push_back(fFitParameters["xOffset"] + fFitParameters["xDirection"] * point.at(2));
    fTrackFittedData[trackerName].push_back(fFitParameters["yOffset"] + fFitParameters["yDirection"] * point.at(2));
    fTrackFittedData[trackerName].push_back(point.at(2));
    //    size = fTrackFittedData[trackerName].size() ;
  }
}

void SRSTrackFit::ClearTracks() {
  fFitParameters.clear();
  for (auto& point : fTrackerData)
    point.second.clear();
  fTrackerData.clear();
  for (auto& point : fTrackFittedData)
    point.second.clear();
  fTrackFittedData.clear();
  for (auto& point : fRawData)
    point.second.clear();
  fRawData.clear();
}

float SRSTrackFit::GetAngleBetweenTwoTracks(const std::map<TString, std::vector<float> >& firstTrack,
                                            const std::map<TString, std::vector<float> >& secondTrack) {
  std::vector<TString> firstTrackTrackerName, secondTrackTrackerName;
  for (const auto& tracker : firstTrack)
    firstTrackTrackerName.push_back(tracker.first);
  for (const auto& tracker2 : secondTrack)
    secondTrackTrackerName.push_back(tracker2.first);
  const auto u = srsreco::utils::subVec(firstTrack.at(firstTrackTrackerName.at(0)),
                                        firstTrack.at(firstTrackTrackerName.at(firstTrackTrackerName.size() - 1))),
             v = srsreco::utils::subVec(secondTrack.at(secondTrackTrackerName.at(0)),
                                        secondTrack.at(secondTrackTrackerName.at(secondTrackTrackerName.size() - 1)));
  return srsreco::utils::getAngleTo(u, v);  // angle between two tracks
}

void SRSTrackFit::PlaneRotationAlongZaxis(float alpha, std::vector<float>& u) {
  if (const auto sizeU = u.size(); sizeU != 3)
    SRS_WARNING("SRSTrackFit:PlaneRotationAlongZaxis") << "U point vector's size = " << sizeU << ",!= 3.";
  u[0] = u[0] * std::cos(alpha) + u[1] * std::sin(alpha);
  u[1] = u[1] * std::cos(alpha) - u[0] * std::sin(alpha);
  //u[2] = u[2]
}
