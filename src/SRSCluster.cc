#include <TF1.h>
#include <TFile.h>
#include <TH1F.h>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSCluster.h"
#include "srsutils/Logging.h"

ClassImp(SRSCluster);

SRSCluster::SRSCluster(int minClusterSize, int maxClusterSize, TString isMaximumOrTotalADCs)
    : fArrayOfHits(new TObjArray(maxClusterSize)),
      fMinClusterSize(minClusterSize),
      fMaxClusterSize(maxClusterSize),
      fIsClusterMaxOrSumADCs(isMaximumOrTotalADCs) {
  SRS_DEBUG("SRSCluster");
}

SRSCluster::~SRSCluster() { fClusterTimeBinADCs.clear(); }

int SRSCluster::GetClusterTimeBin() {
  //float q;
  TObjArray &temp = *fArrayOfHits;
  fClusterTimeBinADC = 0;
  for (int i = 0; i < GetNbOfHits(); i++) {
    std::vector<float> timeBinADCs = ((SRSHit *)temp[i])->GetTimeBinADCs();
    int nbOfTimeBins = timeBinADCs.size();
    fClusterTimeBinADCs.resize(nbOfTimeBins);
    for (int k = 0; k < nbOfTimeBins; k++)
      fClusterTimeBinADCs[k] += timeBinADCs[k];
    timeBinADCs.clear();
  }
  Timing();
  return fClusterTimeBin;
}

void SRSCluster::Timing() {
  //  Bool_t timingStatus = kTRUE ;
  int nBins = fClusterTimeBinADCs.size();
  TH1F *timeBinHist = new TH1F("timeBinHist", "timeBinHist", nBins, 0, (nBins - 1));
  for (int k = 0; k < nBins; k++) {
    timeBinHist->Fill(k, fClusterTimeBinADCs[k]);
  }
  fClusterTimeBin = timeBinHist->GetMaximumBin();
  delete timeBinHist;
}

void SRSCluster::ClusterPositionPulseHeghtWeight() {  // Calculate the fposition and the total fClusterSumADCs
  float hitposition, q;
  TObjArray &temp = *fArrayOfHits;
  int nbofhits = GetNbOfHits();
  for (int i = 0; i < nbofhits; i++) {
    q = ((SRSHit *)temp[i])->GetHitADCs();
    hitposition = ((SRSHit *)temp[i])->GetStripPosition();
    fClusterSumADCs += q;
    fposition += q * hitposition;

    if (q > fClusterPeakADCs) {
      fClusterPeakTimeBin = ((SRSHit *)temp[i])->GetSignalPeakBinNumber();
      fClusterPeakADCs = q;
    }
  }
  fposition /= fClusterSumADCs;
  SRS_DEBUG("SRSCluster:ClusterPositionPulseHeghtWeight") << "clusterPosition = " << fposition << ".";
}

void SRSCluster::ClusterPositionHistoMean() {  // Calculate the fposition and the total fClusterSumADCs
  float hitposition, q;
  TObjArray &temp = *fArrayOfHits;
  int nbofhits = GetNbOfHits();
  int indexLastHist = nbofhits - 1;
  float pitch = fPlaneSize / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane);
  float min = ((SRSHit *)temp[0])->GetStripPosition() - pitch;
  float max = ((SRSHit *)temp[indexLastHist])->GetStripPosition() + pitch;
  int nbin = nbofhits + 2;
  auto h = std::make_unique<TH1F>("coordHisto", "coordinate", nbin, min, max);
  for (int i = 0; i < nbofhits; i++) {
    q = ((SRSHit *)temp[i])->GetHitADCs();
    hitposition = ((SRSHit *)temp[i])->GetStripPosition();
    h->Fill(hitposition, q);
    fClusterSumADCs += q;
    if (q > fClusterPeakADCs)
      fClusterPeakADCs = q;
  }
  fposition = h->GetMean();
  SRS_DEBUG("SRSCluster:ClusterPositionGausFitMean") << "clusterPosition = " << fposition << ".";
}

void SRSCluster::ClusterPositionGausFitMean() {  // Calculate the fposition and the total fClusterSumADCs
  float hitposition, q;
  TObjArray &temp = *fArrayOfHits;
  int nbofhits = GetNbOfHits();
  int indexLastHist = nbofhits - 1;
  float pitch = fPlaneSize / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane);
  float min = ((SRSHit *)temp[0])->GetStripPosition() - pitch;
  float max = ((SRSHit *)temp[indexLastHist])->GetStripPosition() + pitch;
  int nbin = nbofhits + 2;
  auto h = std::make_unique<TH1F>("coordHisto", "coordinate", nbin, min, max);
  for (int i = 0; i < nbofhits; i++) {
    q = ((SRSHit *)temp[i])->GetHitADCs();
    hitposition = ((SRSHit *)temp[i])->GetStripPosition();
    h->Fill(hitposition, q);
    fClusterSumADCs += q;
    if (q > fClusterPeakADCs)
      fClusterPeakADCs = q;
  }
  h->Fit("gaus", "Q", "", min, max);
  fposition = h->GetFunction("gaus")->GetParameter(1);
  SRS_DEBUG("SRSCluster:ClusterPositionGausFitMean") << "clusterPosition = " << fposition << ".";
}

void SRSCluster::ClusterCentralStrip() {
  float p, dp;
  float dpmin = 99;
  TObjArray &temp = *fArrayOfHits;
  int nbofhits = GetNbOfHits();
  for (int i = 0; i < nbofhits; i++) {
    p = ((SRSHit *)temp[i])->GetStripPosition();
    dp = fabs(fposition - p);
    if (dp <= dpmin) {
      fclusterCentralStrip = p;
      dpmin = dp;
    }
  }
}

float SRSCluster::GetClusterADCs() {
  if (fIsClusterMaxOrSumADCs == "maximumADCs")
    return fClusterPeakADCs;
  return fClusterSumADCs;
}

void SRSCluster::Dump() const {
  SRS_INFO("SRSCluster:Dump") << "*** APV Cluster dump ***";
  TObject::Dump();
}

void SRSCluster::AddHit(SRSHit *h) { fArrayOfHits->AddLast(h); }

void SRSCluster::ClearArrayOfHits() { fArrayOfHits->Clear(); }

Bool_t SRSCluster::IsGoodCluster() {
  fIsGoodCluster = kTRUE;
  fNbOfHits = fArrayOfHits->GetEntries();
  if (fNbOfHits > fMaxClusterSize || fNbOfHits < fMinClusterSize) {
    ClearArrayOfHits();
    fIsGoodCluster = false;
    fNbOfHits = fArrayOfHits->GetEntries();
  }
  return fIsGoodCluster;
}

int SRSCluster::Compare(const TObject *obj) const {
  int compare = (fClusterSumADCs < ((SRSCluster *)obj)->GetClusterADCs()) ? 1 : -1;
  return compare;
}

void SRSCluster::ComputeClusterPositionWithoutCorrection() {
  ClusterPositionPulseHeghtWeight();
  ClusterCentralStrip();
}

/*void SRSCluster::ComputeClusterPositionWithCorrection(const char *filename) {
  ComputeClusterPositionWithoutCorrection();

  if (fNbOfHits > 1) {
    auto file = std::make_unique<TFile>(filename, "read");
    if (!file->IsOpen())
      SRS_ERROR("SRSEventBuilder:LoadPositionCorrectionRootFile") << "Can not open file '" << filename << "'.";

    float pitch = fPlaneSize / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane);
    if (fPlane.Contains("EIC"))
      pitch = 2 * pitch;
    float eta = (fposition - fclusterCentralStrip) / pitch;

    TString histoName = "etaFunction" + fPlane;
    if (eta > 0) {
      if (fNbOfHits == 2)
        histoName = "eta2FunctionPos" + fPlane;
      else if (fNbOfHits == 3)
        histoName = "eta3FunctionPos" + fPlane;
      else if (fNbOfHits == 4)
        histoName = "eta4FunctionPos" + fPlane;
      else if (fNbOfHits == 5)
        histoName = "eta5FunctionPos" + fPlane;
      else if (fNbOfHits > 5)
        histoName = "eta6PlusFunctionPos" + fPlane;
    } else if (eta < 0) {
      if (fNbOfHits == 2)
        histoName = "eta2FunctionNeg" + fPlane;
      else if (fNbOfHits == 3)
        histoName = "eta3FunctionNeg" + fPlane;
      else if (fNbOfHits == 4)
        histoName = "eta4FunctionNeg" + fPlane;
      else if (fNbOfHits == 5)
        histoName = "eta5FunctionNeg" + fPlane;
      else if (fNbOfHits > 5)
        histoName = "eta6PlusFunctionNeg" + fPlane;
    }

    auto *posCorrectionHistos = file->Get<TH1F>(histoName);

    TString afterhistoname = posCorrectionHistos->GetName();
    TString fitName = afterhistoname + "_FIT";
    TF1 *correctionFunctionFit = (TF1 *)posCorrectionHistos->GetFunction(fitName);
    float posCorrecFuncution = 0;
    if (eta > 0)
      posCorrecFuncution = correctionFunctionFit->Integral(0, eta) / correctionFunctionFit->Integral(0, 0.5);
    if (eta < 0)
      posCorrecFuncution = correctionFunctionFit->Integral(eta, 0) / correctionFunctionFit->Integral(-0.5, 0);
    delete correctionFunctionFit;

    fposition = fclusterCentralStrip - (pitch * (0.5 - posCorrecFuncution));
    delete posCorrectionHistos;
  }
}*/

void SRSCluster::ComputeClusterPositionWithCorrection(const char *filename) {
  ComputeClusterPositionWithoutCorrection();

  if (fNbOfHits > 1) {
    auto file = std::make_unique<TFile>(filename, "read");
    if (!file->IsOpen())
      SRS_ERROR("SRSEventBuilder:LoadPositionCorrectionRootFile") << "Can not open file '" << filename << "'.";

    float pitch = fPlaneSize / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane);
    if (fPlane.Contains("EIC"))
      pitch = 2 * pitch;
    float eta = (fposition - fclusterCentralStrip) / pitch;

    TString histoNamePos = "etaFunction" + fPlane;
    TString histoNameNeg = "etaFunction" + fPlane;

    if (fNbOfHits == 2) {
      histoNamePos = "eta2FunctionPos" + fPlane;
      histoNameNeg = "eta2FunctionNeg" + fPlane;
    } else if (fNbOfHits == 3) {
      histoNamePos = "eta3FunctionPos" + fPlane;
      histoNameNeg = "eta3FunctionNeg" + fPlane;
    } else if (fNbOfHits == 4) {
      histoNamePos = "eta4FunctionPos" + fPlane;
      histoNameNeg = "eta4FunctionNeg" + fPlane;
    } else if (fNbOfHits == 5) {
      histoNamePos = "eta5FunctionPos" + fPlane;
      histoNameNeg = "eta5FunctionNeg" + fPlane;
    } else if (fNbOfHits > 5) {
      histoNamePos = "eta6PlusFunctionPos" + fPlane;
      histoNameNeg = "eta6PlusFunctionNeg" + fPlane;
    }
    {
      std::unique_ptr<TH1F> posCorrectionPosHistos(file->Get<TH1F>(histoNamePos)),
          posCorrectionNegHistos(file->Get<TH1F>(histoNameNeg));
      float posCorrecFuncution = 0.;

      int firstBin = posCorrectionNegHistos->FindBin(-0.5);
      int zeroBin = posCorrectionPosHistos->FindBin(0);
      int etaBin = posCorrectionPosHistos->FindBin(eta);
      if (eta < 0)
        etaBin = posCorrectionNegHistos->FindBin(eta);

      float integralPos = posCorrectionPosHistos->Integral();
      float integralNeg = posCorrectionPosHistos->Integral();
      float integralSum = integralNeg + integralPos;

      if (eta > 0)
        posCorrecFuncution = (integralNeg + posCorrectionPosHistos->Integral(zeroBin, etaBin)) / integralSum;
      if (eta < 0)
        posCorrecFuncution = (posCorrectionPosHistos->Integral(firstBin, etaBin)) / integralSum;
      /*{
        std::unique_ptr<TF1> correctionFunctionPosFit(
            (TF1 *)posCorrectionPosHistos->GetFunction(posCorrectionPosHistos->GetName() + "_FIT")),
            correctionFunctionNegFit(
                (TF1 *)posCorrectionNegHistos->GetFunction(posCorrectionNegHistos->GetName() + "_FIT"));
        float integralPos = correctionFunctionPosFit->Integral(0, 0.5);
        float integralNeg = correctionFunctionNegFit->Integral(-0.5, 0);
        float integralSum = integralNeg + integralPos;
        if (eta > 0)
          posCorrecFuncution = (integralNeg + correctionFunctionPosFit->Integral(0, eta)) / integralSum;
        if (eta < 0)
          posCorrecFuncution = (correctionFunctionNegFit->Integral(-0.5, eta)) / integralSum;
      }*/
      fposition = fclusterCentralStrip - (pitch * (0.5 - posCorrecFuncution));
    }
  }
}
