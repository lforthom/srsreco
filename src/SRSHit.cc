#include <TH1F.h>
#include <stdlib.h>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSHit.h"
#include "srsutils/Logging.h"

ClassImp(SRSHit);

void SRSHit::Timing() {
  //bool timingStatus = true;
  int nBins = fTimeBinADCs.size();
  auto timeBinHist = std::make_unique<TH1F>("timeBinHist", "timeBinHist", nBins, 0, (nBins - 1));
  for (int k = 0; k < nBins; k++)
    timeBinHist->Fill(k, fTimeBinADCs[k]);
  fSignalPeakBinNumber = timeBinHist->GetMaximumBin();
}

void SRSHit::SetHitADCs(int zeroSupCut, float charges, TString isHitMaxOrTotalADCs) {
  fIsHitMaxOrTotalADCs = isHitMaxOrTotalADCs;

  if (zeroSupCut > 0) {
    float totalADCs = 0, maxADCs = 0;
    int nBins = fTimeBinADCs.size();
    for (int k = 0; k < nBins; k++) {
      totalADCs += fTimeBinADCs[k];
      if (fTimeBinADCs[k] > maxADCs)
        maxADCs = fTimeBinADCs[k];
    }
    //    totalADCs /= nBins ;
    if (fIsHitMaxOrTotalADCs == "signalPeak")
      fHitADCs = maxADCs;
    else
      fHitADCs = totalADCs;
  }

  else {
    fHitADCs = charges;
  }
}

void SRSHit::ComputePosition() {
  float pitch = fPlaneSize / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane);

  if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
    pitch = 2 * fTrapezoidDetOuterRadius / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane);
    fStripPosition = -0.5 * (fTrapezoidDetOuterRadius - pitch) +
                     ((fTrapezoidDetOuterRadius / ((SRSAPVEvent::kNumChannels / 2) * fNbAPVsOnPlane)) * fStripNo);
  }

  else if (fReadoutBoard == "PADPLANE") {
    float padSizeX = fPadDetectorMap[0];
    float padSizeY = fPadDetectorMap[1];
    int nbPadX = (int)fPadDetectorMap[2];
    //int nbPadY = (int)fPadDetectorMap[3];
    //int nbConn = (int)fPadDetectorMap[4];
    float posX = ((fPadNo % nbPadX) + 0.5) * padSizeX;
    float posY = ((int)(fPadNo / nbPadX) + 0.5) * padSizeY;
    fPadPosition.push_back(posX);
    fPadPosition.push_back(posY);
    fPadPosition.resize(2);
    fStripPosition = fStripNo;
  }

  else {
    fStripPosition =
        -0.5 * (fPlaneSize - pitch) + ((fPlaneSize / (SRSAPVEvent::kNumChannels * fNbAPVsOnPlane)) * fStripNo);
  }
}

void SRSHit::SetStripNo(int stripNo) {
  fAbsoluteStripNo = stripNo;

  if ((fReadoutBoard == "UV_ANGLE") && (fDetectorType == "EICPROTO1")) {
    int apvIndexOnPlaneEICBOT = 0;
    if (fapvIndexOnPlane < 4)
      apvIndexOnPlaneEICBOT = fapvIndexOnPlane + 8;
    else
      apvIndexOnPlaneEICBOT = fapvIndexOnPlane - 4;

    if ((fAPVOrientation == 0 && fPlane.Contains("TOP") && fapvIndexOnPlane > 3) ||
        (fPlane.Contains("BOT") && apvIndexOnPlaneEICBOT < 8))
      stripNo = 63 - stripNo;

    if (fPlane.Contains("TOP"))
      fStripNo = stripNo + (64 * (fapvIndexOnPlane % fNbAPVsOnPlane));
    else
      fStripNo = stripNo + (64 * (apvIndexOnPlaneEICBOT % fNbAPVsOnPlane));
  }

  else if ((fReadoutBoard == "1DSTRIPS") || (fReadoutBoard == "CARTESIAN") || (fReadoutBoard == "CMSGEM")) {
    if (fAPVOrientation == 0)
      stripNo = 127 - stripNo;

    if (fapvIndexOnPlane > fNbAPVsOnPlane)
      fStripNo = -1000000;
    else
      fStripNo = stripNo + (SRSAPVEvent::kNumChannels * (fapvIndexOnPlane % fNbAPVsOnPlane));
    SRS_DEBUG("SRSHit:ComputePosition") << "stripNo=" << stripNo << ", fStripNo = " << fStripNo
                                        << ", fapvIndexOnPlane=" << fapvIndexOnPlane << ".";
  }

  else if (fReadoutBoard == "PADPLANE")
    fStripNo = stripNo;

  else if (fAPVOrientation == 0)
    stripNo = 127 - stripNo;
}
