#include <TF1.h>
#include <TFile.h>
#include <TObjString.h>

#include <fstream>
#include <sstream>

#include "srsreco/SRSTrack.h"
#include "srsreco/Utils.h"
#include "srsutils/Logging.h"

ClassImp(SRSTrack);

SRSTrack::SRSTrack(const char *cfgname,
                   TString /*offsetDir*/,
                   TString zeroSupCut,
                   TString maxClustSize,
                   TString minClustSize,
                   TString maxClustMult,
                   TString amoreAgentId) {
  fAmoreAgentID = amoreAgentId;
  if (fZeroSupCut = zeroSupCut.Atoi(); fZeroSupCut != 0) {
    fMaxClusterSize = maxClustSize.Atoi();
    fMinClusterSize = minClustSize.Atoi();
  }

  fMaxClusterMultiplicity = maxClustMult.Atoi();

  ReadCfg(cfgname);
  const auto nbOfDetectors = fDetectorList.size(), nbOfTriggers = fTriggerList.size(),
             nbOfTrackers = fTrackerList.size();
  SRS_INFO("SRSTrack") << "zeroSupCut = " << fZeroSupCut << ", minClustSize = " << fMinClusterSize
                       << ", maxClustSize = " << fMaxClusterSize << ", nbDetectors = " << nbOfDetectors
                       << ", nbOfTriggers = " << nbOfTriggers << ", nbOfTrackers = " << nbOfTrackers << ".";
}

SRSTrack::~SRSTrack() {
  ClearSpacePoints(fTrackSpacePointMap);
  ClearSpacePoints(fFittedSpacePointMap);
  ClearSpacePoints(fRawDataSpacePointMap);
  ClearSpacePoints(fEICstripClusterRawDataYMap);
}

void SRSTrack::DeleteClustersInDetectorPlaneMap(std::map<TString, std::list<SRSCluster *> > &stringListMap) {
  for (auto &sl : stringListMap) {
    for (auto &cluster : sl.second)
      delete cluster;
    sl.second.clear();
  }
  stringListMap.clear();
}

void SRSTrack::ClearSpacePoints(std::map<TString, std::vector<float> > &spacePointMap) {
  for (auto &sp : spacePointMap)
    sp.second.clear();
  spacePointMap.clear();
}

void SRSTrack::ReadCfg(const char *cfgname) {
  SRS_INFO("SRSTrack:ReadCfg") << "Loading SRS Tracking Configuration from '" << cfgname << "'.";

  fTriggerList.clear();
  fTrackerList.clear();
  fDetectorList.clear();

  ClearSpacePoints(fTrackSpacePointMap);
  ClearSpacePoints(fRawDataSpacePointMap);
  ClearSpacePoints(fFittedSpacePointMap);

  std::ifstream file(cfgname, std::ifstream::in);
  int iline = 0;
  TString line;

  while (line.ReadLine(file)) {
    iline++;
    line.Remove(TString::kLeading, ' ');  // strip leading spaces
    if (line.BeginsWith("#"))
      continue;  // and skip comments

    {
      // Create an array of the tokens separated by "," in the line; lines without "," are skipped
      std::unique_ptr<TObjArray> tokens(line.Tokenize(","));
      TIter myiter(tokens.get());                             // iterator on the tokens array
      while (TObjString *st = (TObjString *)myiter.Next()) {  // inner loop (loop over the line);

        auto s = TString(st->GetString()).Remove(TString::kLeading, ' ');  // remove leading spaces
        s.Remove(TString::kTrailing, ' ');                                 // remove trailing spaces

        if (!s.Contains("@"))
          continue;

        if (s == "@RUNFILE") {
          fRunFilePrefix = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          fRunFileValue = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
        } else if (s == "@TRACKANGLECUT") {
          const auto angleCutMinX = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     angleCutMaxX = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     angleCutMinY = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     angleCutMaxY = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          fAngleCutMinX = angleCutMinX * M_PI / 180;
          fAngleCutMaxX = angleCutMaxX * M_PI / 180;
          fAngleCutMinY = angleCutMinY * M_PI / 180;
          fAngleCutMaxY = angleCutMaxY * M_PI / 180;
          SRS_INFO("SRSTrack:ReadCfg") << s.Data() << " -> anglecut_minX = " << angleCutMinX
                                       << " deg, anglecut_maxX = " << angleCutMaxX
                                       << " deg, anglecut_minY = " << angleCutMinY
                                       << " deg, anglecut_maxY = " << angleCutMaxY << " deg.";
          SRS_INFO("SRSTrack:ReadCfg") << s.Data() << " -> anglecut_minX = " << fAngleCutMinX
                                       << " rad , anglecut_maxX = " << fAngleCutMaxX
                                       << " rad, anglecut_minY = " << fAngleCutMinY
                                       << " rad , anglecut_maxY = " << fAngleCutMaxY << " rad.";
          continue;
        } else if (s == "@EVENT3DDISPLAY") {
          fNtupleTitle = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          fNtupleSizeX = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          fNtupleSizeY = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          fNtupleSizeZ = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          SRS_INFO("SRSTrack:ReadCfg") << s.Data() << " -> fNtupleSizeX = " << fNtupleSizeX
                                       << " mm, fNtupleSizeY = " << fNtupleSizeY
                                       << " mm, fNtupleSizeZ = " << fNtupleSizeZ << " mm.";
          continue;
        } else if (s == "@DETECTORS") {
          const auto name = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     triggerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     trackerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          const auto xOffset = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     yOffset = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     zPosition = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          (void)xOffset;
          (void)yOffset;
          //	const auto inputResolution = std::stod(((TObjString*) myiter.Next())->GetString().Data());
          const auto xnbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto xmin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     xmax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          const auto ynbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto ymin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     ymax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          //	SetDetectorConfig(name, triggerType, trackerType, zPosition, inputResolution, inputResolution, xnbin, xmin, xmax, ynbin, ymin, ymax);
          SetDetectorConfig(name, triggerType, trackerType, zPosition, xnbin, xmin, xmax, ynbin, ymin, ymax);
          continue;
        } else if (s == "@TRACKING1") {
          const auto name = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     triggerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     trackerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          const auto xOffset = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     yOffset = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     zPosition = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          (void)xOffset;
          (void)yOffset;
          //	float inputResolution = std::stoi(((TObjString*) myiter.Next()->GetString().Data());
          const auto xnbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto xmin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     xmax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          const auto ynbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto ymin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     ymax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          //	SetDetectorConfig(name, triggerType, trackerType,  zPosition, inputResolution, inputResolution, xnbin, xmin, xmax, ynbin, ymin, ymax);
          SetDetectorConfig(name, triggerType, trackerType, zPosition, xnbin, xmin, xmax, ynbin, ymin, ymax);
          continue;
        } else if (s == "@TRACKING2D") {
          const auto name = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     triggerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     trackerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          const auto zPosition = std::stod(TString(((TObjString *)myiter.Next())->GetString()).Data());
          //	float xinputResol = std::stod(((TObjString*) myiter.Next())->GetString().Data());
          //	float yinputResol = std::stod(((TObjString*) myiter.Next())->GetString().Data());
          const auto xnbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto xmin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     xmax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          const auto ynbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto ymin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     ymax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          //	SetDetectorConfig(name, triggerType, trackerType, zPosition, xinputResol, yinputResol, xnbin, xmin, xmax, ynbin, ymin, ymax);
          SetDetectorConfig(name, triggerType, trackerType, zPosition, xnbin, xmin, xmax, ynbin, ymin, ymax);
          continue;
        } else if (s == "@TRACKING1D") {
          const auto name = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     triggerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' '),
                     trackerType = TString(((TObjString *)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          const auto zPosition = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          //	float inputResolution = std::stod(((TObjString*) myiter.Next())->GetString().Data());
          const auto xnbin = std::stoi(((TObjString *)myiter.Next())->GetString().Data());
          const auto xmin = std::stod(((TObjString *)myiter.Next())->GetString().Data()),
                     xmax = std::stod(((TObjString *)myiter.Next())->GetString().Data());
          //   	SetDetectorConfig(name, triggerType, trackerType, zPosition, inputResolution, inputResolution, xnbin, xmin, xmax, xnbin, xmin, xmax);
          SetDetectorConfig(name, triggerType, trackerType, zPosition, xnbin, xmin, xmax, xnbin, xmin, xmax);
          continue;
        } else
          SRS_WARNING("SRSTrack:ReadCfg") << "Unsupported module name: '" << s.Data() << "'.";
      }
    }
  }
  SRS_INFO("SRSTrack:ReadCfg") << "Exit SRS Tracking Configuration.";
}

TString SRSTrack::RunIdStream() {
  std::ostringstream runIdStrStr;
  runIdStrStr << RunId();
  return runIdStrStr.str();
}

int SRSTrack::RunId() {
  return (fRunFileValue && fRunFileValue.Atoi() > 0) ? fRunFileValue.Atoi() : fAmoreAgentID.Atoi();
}

void SRSTrack::LoadFTBFalignementParametersRootFile(TString offsetDir) {
  SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile") << "Enter '" << offsetDir.Data() << "'.";

  /*const auto runIdStr = RunIdStream();
  TString xyOffsetFilename = offsetDir + "xyOffset_Run" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("BeamPos"))
    xyOffsetFilename = offsetDir + "xyOffset_P" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("HVScan"))
    xyOffsetFilename = offsetDir + "xyOffset_HV" + runIdStr + ".root";
  SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
      << "load FTBF x & y offsets parameters root file '" << xyOffsetFilename.Data() << "'.";
  {
    auto xyOffsetFile = std::make_unique<TFile>(xyOffsetFilename, "read");
    if (!xyOffsetFile->IsOpen()) {
      SRS_ERROR("SRSTrack:LoadFTBFalignementParametersRootFile")
          << "Cannot open file '" << xyOffsetFilename.Data() << "'.";
      for (const auto &det : fDetectorList) {
        const auto &detName = det.first;
        fDetXOffset[detName] = 0;
        fDetYOffset[detName] = 0;
      }
    } else {
      for (const auto& det : fDetectorList) {
        const auto& detName = det.first;
        fDetXOffset[detName] = 0;
        fDetYOffset[detName] = 0;

        if (detName == "TrkGEM0") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM0X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM0Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM2") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM2X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM2Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM3") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM3X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM3Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM4") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM4X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM4Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM5") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM5X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM5Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM6") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM6X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM6Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM7") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM7X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM7Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM8") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM8X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM8Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM9") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM9X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM9Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM10") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM10X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetTrkGEM10Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "SBSGEM2") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetSBSGEM2X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetSBSGEM2Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "SBSGEM1") {
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetSBSGEM1X")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetSBSGEM1Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "EIC1") {
          const auto fileNb = RunId();
          if ((fileNb < 7 || fileNb > 19) && fRunFilePrefix.Contains("25GeVHadronBeam"))
            continue;
          if ((fileNb > 3 && fileNb < 7) && fRunFilePrefix.Contains("120GeVProtonBeam"))
            continue;
          fDetXOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetEIC1TOP")->GetFunction("fitFunction")->GetParameter("Mean");
          fDetYOffset[detName] =
              xyOffsetFile->Get<TH1F>("offsetEIC1BOT")->GetFunction("fitFunction")->GetParameter("Mean");
        }
        SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
            << "load FTBF: det = '" << detName.Data() << "', offsetX = " << fDetXOffset[detName]
            << ", offsetY = " fDetYOffset[detName] << ".";
      }
    }
  }*/

  //*******************************************************************************************************************************************************************
  // COARSE OFFSETS PARAMETERS
  //*******************************************************************************************************************************************************************
  TString runIdStr = RunIdStream();
  TString Coarse_xyOffsetFilename = offsetDir + "coarse_xyOffset_Run" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("BeamPos"))
    Coarse_xyOffsetFilename = offsetDir + "coarse_xyOffset_P" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("HVScan"))
    Coarse_xyOffsetFilename = offsetDir + "coarse_xyOffset_HV" + runIdStr + ".root";
  SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
      << "load FTBF x & y offsets parameters root file '" << Coarse_xyOffsetFilename.Data() << "'.";
  {
    auto Coarse_xyOffsetFile = std::make_unique<TFile>(Coarse_xyOffsetFilename, "read");
    if (!Coarse_xyOffsetFile->IsOpen()) {
      SRS_ERROR("SRSTrack:LoadFTBFalignementParametersRootFile")
          << "Cannot open file '" << Coarse_xyOffsetFilename.Data() << "'.";
      for (const auto &det : fDetectorList)
        fCoarseDetXOffset[det.first] = fCoarseDetYOffset[det.first] = 0;
    }

    else {
      for (const auto &det : fDetectorList) {
        const auto &detName = det.first;
        fCoarseDetXOffset[detName] = 0;
        fCoarseDetYOffset[detName] = 0;

        if (detName == "TrkGEM0") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM0X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM0Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM2") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM2X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM2Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM3") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM3X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM3Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM4") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM4X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM4Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM5") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM5X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM5Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM6") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM6X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM6Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM7") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM7X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM7Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM8") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM8X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM8Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM9") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM9X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM9Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM10") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM10X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetTrkGEM10Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "SBSGEM2") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetSBSGEM2X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetSBSGEM2Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "SBSGEM1") {
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetSBSGEM1X")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetSBSGEM1Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "EIC1") {
          const auto fileNb = RunId();
          if ((fileNb < 7 || fileNb > 19) && fRunFilePrefix.Contains("25GeVHadronBeam"))
            continue;
          if (fileNb > 3 && fileNb < 7 && fRunFilePrefix.Contains("120GeVProtonBeam"))
            continue;
          fCoarseDetXOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetEIC1TOP")->GetFunction("fitFunction")->GetParameter("Mean");
          fCoarseDetYOffset[detName] =
              Coarse_xyOffsetFile->Get<TH1F>("offsetEIC1BOT")->GetFunction("fitFunction")->GetParameter("Mean");
        }
        SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
            << "load FTBF: det = '" << detName.Data() << "', coarse offsetX = " << fCoarseDetXOffset[detName]
            << ", coarse offsetY = " << fCoarseDetYOffset[detName] << ".";
        fDetXOffset[detName] = fCoarseDetXOffset[detName] + fFineDetXOffset[detName];
        fDetYOffset[detName] = fCoarseDetYOffset[detName] + fFineDetXOffset[detName];
      }
    }
  }

  //*******************************************************************************************************************************************************************
  // ROTATION PARAMETERS
  //*******************************************************************************************************************************************************************
  TString xyPlaneRotationFilename = offsetDir + "xyPlaneRotation_Run" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("BeamPos"))
    xyPlaneRotationFilename = offsetDir + "xyPlaneRotation_P" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("HVScan"))
    xyPlaneRotationFilename = offsetDir + "xyPlaneRotation_HV" + runIdStr + ".root";
  SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
      << "load FTBF x/y plane rotation parameters root file '" << xyPlaneRotationFilename.Data() << "'.";
  {
    auto xyRotationPlaneFile = std::make_unique<TFile>(xyPlaneRotationFilename, "read");
    if (!xyRotationPlaneFile->IsOpen()) {
      SRS_ERROR("SRSTrack:LoadFTBFalignementParametersRootFile")
          << "Cannot open file '" << xyPlaneRotationFilename.Data() << "'.";
      for (const auto &det : fDetectorList)
        fDetPlaneRotationCorrection[det.first] = 0;
    } else {
      for (const auto &det : fDetectorList) {
        const auto &detName = det.first;
        fDetPlaneRotationCorrection[detName] = 0;
        if (detName == "TrkGEM0")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM0")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM2")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM2")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM3")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM3")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM4")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM4")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM5")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM5")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM6")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM6")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM7")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM7")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM8")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM8")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM9")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM9")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "TrkGEM10")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationTrkGEM10")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "SBSGEM2")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationSBSGEM2")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "SBSGEM1")
          fDetPlaneRotationCorrection[detName] =
              xyRotationPlaneFile->Get<TH1F>("rotationSBSGEM1")->GetFunction("fitFunction")->GetParameter("Mean");
        else if (detName == "EIC1") {
          int fileNb = RunId();
          if ((fileNb < 7 || fileNb > 19) && fRunFilePrefix.Contains("25GeVHadronBeam"))
            continue;
          if (fileNb > 3 && fileNb < 7 && fRunFilePrefix.Contains("120GeVProtonBeam"))
            continue;
          fDetPlaneRotationCorrection[detName] +=
              xyRotationPlaneFile->Get<TH1F>("rotationEIC1")->GetFunction("fitFunction")->GetParameter("Mean");
        }
        SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
            << "load FTBF: det = '" << detName.Data()
            << "', x/y plane rotation angle = " << fDetPlaneRotationCorrection[detName] << ".";
      }
    }
  }

  //*******************************************************************************************************************************************************************
  // FINE OFFSETS PARAMETERS
  //*******************************************************************************************************************************************************************
  //  TString runIdStr = RunIdStream() ;
  TString Fine_xyOffsetFilename = offsetDir + "fine_xyOffset_Run" + runIdStr + ".root";
  if (fRunFilePrefix.Contains("BeamPos"))
    Fine_xyOffsetFilename = offsetDir + "fine_xyOffset_P" + runIdStr + ".root";
  else if (fRunFilePrefix.Contains("HVScan"))
    Fine_xyOffsetFilename = offsetDir + "fine_xyOffset_HV" + runIdStr + ".root";
  SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
      << "load FTBF x & y offsets parameters root file '" << Fine_xyOffsetFilename.Data() << "'.";
  {
    auto Fine_xyOffsetFile = std::make_unique<TFile>(Fine_xyOffsetFilename, "read");
    if (!Fine_xyOffsetFile->IsOpen()) {
      SRS_ERROR("SRSTrack:LoadFTBFalignementParametersRootFile")
          << "Cannot open file '" << Fine_xyOffsetFilename.Data() << "'.";
      for (const auto &det : fDetectorList)
        fFineDetXOffset[det.first] = fFineDetYOffset[det.first] = 0;
    } else {
      for (const auto &det : fDetectorList) {
        const auto &detName = det.first;
        if (detName == "TrkGEM0") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM0X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM0Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM2") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM2X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM2Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM3") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM3X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM3Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM4") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM4X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM4Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM5") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM5X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM5Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM6") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM6X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM6Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM7") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM7X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM7Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM8") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM8X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM8Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM9") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM9X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM9Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "TrkGEM10") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM10X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetTrkGEM10Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "SBSGEM2") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetSBSGEM2X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetSBSGEM2Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "SBSGEM1") {
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetSBSGEM1X")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetSBSGEM1Y")->GetFunction("fitFunction")->GetParameter("Mean");
        } else if (detName == "EIC1") {
          int fileNb = RunId();
          if ((fileNb < 7 || fileNb > 19) && fRunFilePrefix.Contains("25GeVHadronBeam"))
            continue;
          if (fileNb > 3 && fileNb < 7 && fRunFilePrefix.Contains("120GeVProtonBeam"))
            continue;
          fFineDetXOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetEIC1TOP")->GetFunction("fitFunction")->GetParameter("Mean");
          fFineDetYOffset[detName] +=
              Fine_xyOffsetFile->Get<TH1F>("offsetEIC1BOT")->GetFunction("fitFunction")->GetParameter("Mean");
        }
        SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile")
            << "load FTBF: det = '" << detName.Data() << "', total offsetX = " << fFineDetXOffset[detName]
            << ", totaloffsetY = " << fFineDetYOffset[detName] << ".";
      }
    }
  }
  SRS_INFO("SRSTrack:LoadFTBFalignementParametersRootFile") << "Exit";
}

void SRSTrack::SetDetectorConfig(TString detName,
                                 TString triggerType,
                                 TString trackerType,
                                 float zPosition,
                                 int xnbin,
                                 float xmin,
                                 float xmax,
                                 int ynbin,
                                 float ymin,
                                 float ymax) {
  if (detName == "EIC1") {
    const auto fileNb = RunId();
    if (fRunFilePrefix.Contains("25GeVHadronBeam") && fileNb > 3 && fileNb < 7) {
      trackerType = "noTracker";
      triggerType = "noTrigger";
    } else if (fRunFilePrefix.Contains("120GeVProtonBeam") && (fileNb < 7 || fileNb > 19)) {
      trackerType = "noTracker";
      triggerType = "noTrigger";
    }
  }

  SRS_INFO("SRSTrack:SetDetectorConfig") << detName.Data() << ",   " << fRunFilePrefix.Data() << ",   "
                                         << fRunFileValue.Data() << ",   " << triggerType.Data() << ",   "
                                         << trackerType.Data() << ",   " << zPosition << ".";

  fDetZPosition[detName] = zPosition;
  fDetectorList[detName] = detName;

  if (trackerType == "isTracker" || trackerType == "IsTracker")
    fTrackerList[detName] = trackerType;
  if (triggerType == "isTrigger" || triggerType == "IsTrigger")
    fTriggerList[detName] = triggerType;

  fXNBinResiduals[detName] = xnbin;
  fXRangeMinResiduals[detName] = xmin;
  fXRangeMaxResiduals[detName] = xmax;

  fYNBinResiduals[detName] = ynbin;
  fYRangeMinResiduals[detName] = ymin;
  fYRangeMaxResiduals[detName] = ymax;

  fRNBinResiduals[detName] = xnbin;
  fRRangeMinResiduals[detName] = xmin;
  fRRangeMaxResiduals[detName] = xmax;

  fPHINBinResiduals[detName] = ynbin;
  fPHIRangeMinResiduals[detName] = ymin;
  fPHIRangeMaxResiduals[detName] = ymax;
}

bool SRSTrack::IsTracker(TString detName) { return fTrackerList.find(detName) != fTrackerList.end(); }

bool SRSTrack::IsTrigger(TString detName) { return fTriggerList.find(detName) != fTriggerList.end(); }

void SRSTrack::BuildRawDataSpacePoints(SRSEventBuilder *eventbuilder) {
  ClearSpacePoints(fRawDataSpacePointMap);
  ClearSpacePoints(fEICstripClusterRawDataYMap);

  fIsGoodEvent = eventbuilder->IsAGoodEvent();
  if (fIsGoodEvent) {
    fIsGoodTrack = true;
    auto mapping = SRSMapping::GetInstance();
    for (const auto &det : fDetectorList) {
      const auto &detName = det.first;
      if (!eventbuilder->IsAGoodEventInDetector(detName))
        continue;

      std::map<int, std::vector<float> > detectorEvent = eventbuilder->GetDetectorCluster(detName);
      int clusterMult = detectorEvent.size();

      for (int k = 0; k < clusterMult; k++) {
        fRawDataSpacePointMap[detName].push_back(detectorEvent[k][0] + fCoarseDetXOffset[detName]);
        fRawDataSpacePointMap[detName].push_back(detectorEvent[k][1] + fCoarseDetYOffset[detName]);
        fRawDataSpacePointMap[detName].push_back(fDetZPosition[detName]);
        fEICstripClusterRawDataYMap[detName].push_back(detectorEvent[k][6]);
        fEICstripClusterRawDataYMap[detName].push_back(detectorEvent[k][7]);
        fEICstripClusterRawDataYMap[detName].push_back(fDetZPosition[detName]);
        detectorEvent[k].clear();
      }

      /*fRawDataSpacePointMap[detName].push_back(detectorEvent[0][0] + fCoarseDetXOffset[detName]);
      fRawDataSpacePointMap[detName].push_back(detectorEvent[0][1] + fCoarseDetYOffset[detName]);
      fRawDataSpacePointMap[detName].push_back(fDetZPosition[detName]);
      fEICstripClusterRawDataYMap[detName].push_back(detectorEvent[0][6]);
      fEICstripClusterRawDataYMap[detName].push_back(detectorEvent[0][7]);
      fEICstripClusterRawDataYMap[detName].push_back(fDetZPosition[detName]);
      for (int k = 0; k < clusterMult; k++)
        detectorEvent[k].clear();*/

      PlaneRotationCorrection(fDetPlaneRotationCorrection[detName], fRawDataSpacePointMap[detName]);
      fRawDataSpacePointMap[detName][0] += fFineDetXOffset[detName];
      fRawDataSpacePointMap[detName][1] += fFineDetYOffset[detName];

      detectorEvent.clear();
    }
  }
}

void SRSTrack::BuildTrack() {
  fIsGoodTrack = false;
  SRS_DEBUG("SRSTrack:BuildTrack");
  ClearSpacePoints(fTrackSpacePointMap);
  if (fIsGoodEvent) {
    fIsGoodTrack = true;
    for (const auto &tracker : fTrackerList)
      fTrackSpacePointMap[tracker.first] = fRawDataSpacePointMap[tracker.first];
  }
}

void SRSTrack::DoTracking() {
  SRS_DEBUG("SRSTrack:DoTracking");
  fIsGoodTrack = false;
  if (fIsGoodEvent) {
    ClearSpacePoints(fFittedSpacePointMap);
    auto trackFit = std::make_unique<SRSTrackFit>(fTrackSpacePointMap, fRawDataSpacePointMap);
    fFittedSpacePointMap = trackFit->GetTrackFittedData();
    fFitParameters = trackFit->GetFitParameters();
    const auto angleX = std::fabs(fFitParameters["xDirection"]), angleY = std::fabs(fFitParameters["yDirection"]);
    if (angleX > fAngleCutMinX && angleX < fAngleCutMaxX && angleY > fAngleCutMinY && angleY < fAngleCutMaxY)
      fIsGoodTrack = true;
  }
}

bool SRSTrack::IsAGoodTrack(SRSEventBuilder *eventbuilder) {
  fIsGoodTrack = false;
  BuildRawDataSpacePoints(eventbuilder);
  if (fZeroSupCut != 0 && fTrackerList.size() != 0) {
    BuildTrack();
    DoTracking();
  }
  return fIsGoodTrack;
}

float SRSTrack::getAngleAmplitude(const std::vector<float> u, const std::vector<float> v) {
  if (const auto sizeU = u.size(); sizeU != 3)
    SRS_WARNING("SRSTrack:getAngleAmplitude")
        << "U point vector's size = " << sizeU << ",!= 3   !!! Check cluster Multiplicity !!!";
  if (const auto sizeV = v.size(); sizeV != 3)
    SRS_WARNING("SRSTrack:getAngleAmplitude")
        << "V point vector's size = " << sizeV << ",!= 3   !!! Check cluster Multiplicity !!!";
  float argument = std::fabs(srsreco::utils::dotVec(u, v)) / (srsreco::utils::normVec(u) * srsreco::utils::normVec(v));
  const auto angle = std::acos(argument);
  if (angle != angle)  // deals with infty
    return 0.;
  return angle;
}

void SRSTrack::PlaneRotationCorrection(float alpha, std::vector<float> &u) {
  if (const auto sizeU = u.size(); sizeU != 3)
    SRS_WARNING("SRSTrack:PlaneRotationCorrection")
        << "U point vector's size = " << sizeU << " != 3  !!! Check cluster Multiplicity !!!";
  alpha = -1 * alpha;
  u[0] = u[0] * TMath::Cos(alpha) + u[1] * TMath::Sin(alpha);
  u[1] = u[1] * TMath::Cos(alpha) - u[0] * TMath::Sin(alpha);
  u[2] = u[2];
}
