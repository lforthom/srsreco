#include <TObjArray.h>
#include <TObjString.h>

#include <fstream>

#include "srsreco/SRSMapping.h"
#include "srsutils/Logging.h"

void SRSMapping::SetCartesianStripsReadoutMap(TString readoutBoard,
                                              TString detectorType,
                                              TString detector,
                                              int detID,
                                              TString planeX,
                                              float sizeX,
                                              int connectorsX,
                                              int orientX,
                                              TString planeY,
                                              float sizeY,
                                              int connectorsY,
                                              int orientY) {
  SRS_INFO("SRSMapping:SetDetectorMap") << "readout='" << readoutBoard.Data() << "', detType='" << detectorType.Data()
                                        << "', det='" << detector.Data() << "', detID=" << detID << ", planeX='"
                                        << planeX.Data() << "', SizeX=" << sizeX << ", connectorsX=" << connectorsX
                                        << ", orientationX=" << orientX << ", planeY='" << planeY.Data()
                                        << "', SizeY=" << sizeY << ", connectorsY=" << connectorsY
                                        << ", orientationY=" << orientY << ".";

  fDetectorFromIDMap[detID] = detector;
  fReadoutBoardFromIDMap[detID] = readoutBoard;

  fDetectorIDFromDetectorMap[detector] = detID;
  fReadoutBoardFromDetectorMap[detector] = readoutBoard;
  fDetectorTypeFromDetectorMap[detector] = detectorType;

  fDetectorListFromDetectorTypeMap[detectorType].push_back(detector);
  fDetectorListFromReadoutBoardMap[readoutBoard].push_back(detector);

  fPlaneIDFromPlaneMap[planeX] = 0;
  fPlaneIDFromPlaneMap[planeY] = 1;

  fDetectorFromPlaneMap[planeX] = detector;
  fDetectorFromPlaneMap[planeY] = detector;

  fDetectorPlaneListFromDetectorMap[detector].push_back(planeX);
  fDetectorPlaneListFromDetectorMap[detector].push_back(planeY);

  fCartesianPlaneMap[planeX].push_back(0);
  fCartesianPlaneMap[planeX].push_back(sizeX);
  fCartesianPlaneMap[planeX].push_back(connectorsX);
  fCartesianPlaneMap[planeX].push_back(orientX);
  fCartesianPlaneMap[planeY].push_back(1);
  fCartesianPlaneMap[planeY].push_back(sizeY);
  fCartesianPlaneMap[planeY].push_back(connectorsY);
  fCartesianPlaneMap[planeY].push_back(orientY);
}

void SRSMapping::SetUVStripsReadoutMap(TString readoutBoard,
                                       TString detectorType,
                                       TString detector,
                                       int detID,
                                       float length,
                                       float innerR,
                                       float outerR,
                                       TString planeTop,
                                       int conectTop,
                                       int orientTop,
                                       TString planeBot,
                                       int connectBot,
                                       int orientBot) {
  /*SRS_DEBUG("SRSMapping:SetDetectorMap") << "readout='" << readoutBoard.Data() << "', detType='" << detectorType.Data()
                                         << "', det='" << detector.Data() << "', detID=" << detID << ", planeX='"
                                         << planeX.Data() << "', SizeX=" << sizeX << ", connectorsX=" << connectorsX
                                         << ", orientationX=" << orientX << ", planeY='" << planeY.Data()
                                         << "', SizeY=" << sizeY << ", connectorsY=" << connectorsY
                                         << ", orientationY=" << orientY << ".";*/

  fDetectorFromIDMap[detID] = detector;
  fReadoutBoardFromIDMap[detID] = readoutBoard;

  fDetectorIDFromDetectorMap[detector] = detID;
  fReadoutBoardFromDetectorMap[detector] = readoutBoard;
  fDetectorTypeFromDetectorMap[detector] = detectorType;

  fDetectorListFromDetectorTypeMap[detectorType].push_back(detector);
  fDetectorListFromReadoutBoardMap[readoutBoard].push_back(detector);

  fPlaneIDFromPlaneMap[planeTop] = 0;
  fPlaneIDFromPlaneMap[planeBot] = 1;

  fDetectorFromPlaneMap[planeTop] = detector;
  fDetectorFromPlaneMap[planeBot] = detector;

  fDetectorPlaneListFromDetectorMap[detector].push_back(planeTop);
  fDetectorPlaneListFromDetectorMap[detector].push_back(planeBot);

  fUVangleReadoutMap[detector].push_back(length);
  fUVangleReadoutMap[detector].push_back(innerR);
  fUVangleReadoutMap[detector].push_back(outerR);

  fUVangleReadoutMap[planeTop].push_back(0);
  fUVangleReadoutMap[planeTop].push_back(conectTop);
  fUVangleReadoutMap[planeTop].push_back(orientTop);
  fUVangleReadoutMap[planeBot].push_back(1);
  fUVangleReadoutMap[planeBot].push_back(connectBot);
  fUVangleReadoutMap[planeBot].push_back(orientBot);
}

void SRSMapping::Set1DStripsReadoutMap(TString readoutBoard,
                                       TString detectorType,
                                       TString detector,
                                       int detID,
                                       TString plane,
                                       float size,
                                       int connectors,
                                       int orient) {
  SRS_INFO("SRSMapping:SetDetectorMap") << "readout='" << readoutBoard.Data() << "', detType='" << detectorType.Data()
                                        << "', det='" << detector.Data() << "', detID=" << detID << ", plane='"
                                        << plane.Data() << "', Size=" << size << ", connectors=" << connectors
                                        << ", orientation=" << orient << ".",

      fDetectorFromIDMap[detID] = detector;
  fReadoutBoardFromIDMap[detID] = readoutBoard;

  fDetectorIDFromDetectorMap[detector] = detID;
  fReadoutBoardFromDetectorMap[detector] = readoutBoard;
  fDetectorTypeFromDetectorMap[detector] = detectorType;

  fDetectorListFromDetectorTypeMap[detectorType].push_back(detector);
  fDetectorListFromReadoutBoardMap[readoutBoard].push_back(detector);

  fPlaneIDFromPlaneMap[plane] = 0;
  fDetectorFromPlaneMap[plane] = detector;
  fDetectorPlaneListFromDetectorMap[detector].push_back(plane);

  f1DStripsPlaneMap[plane].push_back(0);
  f1DStripsPlaneMap[plane].push_back(size);
  f1DStripsPlaneMap[plane].push_back(connectors);
  f1DStripsPlaneMap[plane].push_back(orient);
}

void SRSMapping::SetCMSGEMReadoutMap(TString readoutBoard,
                                     TString detectorType,
                                     TString detector,
                                     int detID,
                                     TString EtaSector,
                                     float etaSectorPos,
                                     float etaSectorSize,
                                     float nbOfSectorConnectors,
                                     int apvOrientOnEtaSector) {
  SRS_INFO("SRSMapping:SetDetectorMap") << "readout='" << readoutBoard.Data() << "', detType='" << detectorType.Data()
                                        << "', det='" << detector.Data() << "', detID=" << detID << ", EtaSector='"
                                        << EtaSector.Data() << "', etaSectorSize=" << etaSectorSize
                                        << ", nbOSectorfConnectors=" << nbOfSectorConnectors
                                        << ", apvOrientOnEtaSector=" << apvOrientOnEtaSector << ".";

  fDetectorFromIDMap[detID] = detector;
  fReadoutBoardFromIDMap[detID] = readoutBoard;

  fDetectorIDFromDetectorMap[detector] = detID;
  fReadoutBoardFromDetectorMap[detector] = readoutBoard;
  fDetectorTypeFromDetectorMap[detector] = detectorType;
  fDetectorListFromDetectorTypeMap[detectorType].push_back(detector);
  fDetectorListFromReadoutBoardMap[readoutBoard].push_back(detector);

  fDetectorFromPlaneMap[EtaSector] = detector;
  fDetectorPlaneListFromDetectorMap[detector].push_back(EtaSector);

  fCMSGEMDetectorMap[EtaSector].push_back(etaSectorPos);
  fCMSGEMDetectorMap[EtaSector].push_back(etaSectorSize);
  fCMSGEMDetectorMap[EtaSector].push_back(nbOfSectorConnectors);
  fCMSGEMDetectorMap[EtaSector].push_back(apvOrientOnEtaSector);
}

void SRSMapping::SetPadsReadoutMap(TString readoutBoard,
                                   TString detectorType,
                                   TString detector,
                                   int detID,
                                   TString padPlane,
                                   float padSizeX,
                                   float padSizeY,
                                   float nbOfPadX,
                                   float nbOfPadY,
                                   float nbOfConnectors) {
  SRS_INFO("SRSMapping:SetDetectorMap") << "readout='" << readoutBoard.Data() << "', detType='" << detectorType.Data()
                                        << "', det='" << detector.Data() << "', detID=" << detID << ", padPlane='"
                                        << padPlane.Data() << "', nbOfPadX=" << nbOfPadX << ", padSizeX=" << padSizeX
                                        << ", nbOfPadY=" << nbOfPadY << ", padSizeY=" << padSizeY
                                        << ", nbOfConnectors=" << nbOfConnectors << ".";

  fDetectorFromIDMap[detID] = detector;
  fReadoutBoardFromIDMap[detID] = readoutBoard;

  fDetectorIDFromDetectorMap[detector] = detID;
  fReadoutBoardFromDetectorMap[detector] = readoutBoard;
  fDetectorTypeFromDetectorMap[detector] = detectorType;
  fDetectorListFromDetectorTypeMap[detectorType].push_back(detector);
  fDetectorListFromReadoutBoardMap[readoutBoard].push_back(detector);

  fDetectorFromPlaneMap[padPlane] = detector;
  fDetectorPlaneListFromDetectorMap[detector].push_back(padPlane);
  fPlaneIDFromPlaneMap[padPlane] = 0;

  fPadDetectorMap[detector].push_back(padSizeX);
  fPadDetectorMap[detector].push_back(padSizeY);
  fPadDetectorMap[detector].push_back(nbOfPadX);
  fPadDetectorMap[detector].push_back(nbOfPadY);
  fPadDetectorMap[detector].push_back(nbOfConnectors);
}

TString SRSMapping::GetAPV(TString detPlane, int fecId, int adcCh, int apvNo, int apvIndex, int apvID) {
  std::ostringstream out;
  out << apvID;
  TString apvIDStr = out.str();
  out.str("");
  out << fecId;
  TString fecIDStr = out.str();
  out.str("");
  out << adcCh;
  TString adcChStr = out.str();
  out.str("");
  out << apvNo;
  TString apvNoStr = out.str();
  out.str("");
  out << apvIndex;
  TString apvIndexStr = out.str();
  out.str("");
  TString apvName = "apv" + apvNoStr + "_Id" + apvIDStr + "_" + detPlane + "_adcCh" + adcChStr + "_FecId" + fecIDStr;
  return apvName;
}

void SRSMapping::SetAPVMap(TString detPlane, int fecId, int adcCh, int apvNo, int apvOrient, int apvIndex, int apvHdr) {
  int apvID = (fecId << 4) | adcCh;

  TString apvName = GetAPV(detPlane, fecId, adcCh, apvNo, apvIndex, apvID);

  fAPVNoFromIDMap[apvID] = apvNo;
  fAPVIDFromAPVNoMap[apvNo] = apvID;
  fAPVFromIDMap[apvID] = apvName;
  fAPVHeaderLevelFromIDMap[apvID] = apvHdr;
  fAPVOrientationFromIDMap[apvID] = apvOrient;
  fAPVIndexOnPlaneFromIDMap[apvID] = apvIndex;

  fAPVIDFromNameMap[apvName] = apvID;
  fDetectorPlaneFromAPVIDMap[apvID] = detPlane;

  fAPVIDListFromFECIDMap[fecId].push_back(apvID);
  fFECIDListFromDetectorPlaneMap[detPlane].push_back(fecId);
  fAPVIDListFromDetectorPlaneMap[detPlane].push_back(apvID);

  TString detector = GetDetectorFromPlane(detPlane);
  fAPVIDListFromDetectorMap[detector].push_back(apvID);
}

void SRSMapping::SetAPVtoPadMapping(int fecId, int adcCh, int padId, int apvCh) {
  int apvID = (fecId << 4) | adcCh;
  int apvChPadCh = (padId << 8) | apvCh;
  fAPVToPadChannelMap[apvID].push_back(apvChPadCh);
}

void SRSMapping::PrintMapping() {
  for (const auto& det : fDetectorPlaneListFromDetectorMap) {
    const auto& detector = det.first;
    SRS_INFO("SRSMapping:PrintMapping") << "Detector = '" << detector.Data() << "'.";
    for (const auto& detPlane : det.second) {
      for (const auto& fecId : GetFECIDListFromDetectorPlane(detPlane)) {
        SRS_INFO("SRSMapping:PrintMapping") << "Plane = '" << detPlane.Data() << "',        FEC = " << fecId << ".";
        for (const auto& apvID : GetAPVIDListFromDetectorPlane(detPlane)) {
          int apvNo = GetAPVNoFromID(apvID);
          int apvIndex = GetAPVIndexOnPlane(apvID);
          int apvOrient = GetAPVOrientation(apvID);
          int fecID = GetFECIDFromAPVID(apvID);
          int adcCh = GetADCChannelFromAPVID(apvID);
          int apvHdrLevel = GetAPVHeaderLevelFromID(apvID);
          TString apvName = GetAPVFromID(apvID);
          if (fecID == fecId)
            SRS_INFO("SRSMapping:PrintMapping")
                << "adcCh=" << adcCh << ",  apvName='" << apvName.Data() << "',  apvID=" << apvID << ", apvNo=" << apvNo
                << ",  apvIndex=" << apvIndex << ",  apvOrientation=" << apvOrient << ",  apvHdr=" << apvHdrLevel
                << ".";
        }
      }
    }
  }
  SRS_INFO("SRSMapping:PrintMapping") << "Mapping of " << GetNbOfDetectors() << " detectors, " << GetNbOfDetectorPlane()
                                      << " planes, " << GetNbOfFECs() << " FECs, " << GetNbOfAPVs() << " APVs.";
}

void SRSMapping::SaveMapping(const char* file) {
  SRS_INFO("SRSMapping:SaveMapping") << "Saving SRS Mapping to file '" << file << "'.";
  std::ofstream f(file);
  f << "#################################################################################################\n"
    << "         readoutType  Detector    Plane  DetNo   Plane   size (mm)  connectors  orientation\n"
    << "#################################################################################################\n";

  for (const auto& det : fDetectorPlaneListFromDetectorMap) {
    const auto& detector = det.first;
    TString readoutBoard = GetReadoutBoardFromDetector(detector);
    TString detectorType = GetDetectorTypeFromDetector(detector);

    if ((readoutBoard == "CARTESIAN") || (readoutBoard == "UV_ANGLE_OLD")) {
      const auto& detPlaneList = det.second;
      const auto& planeX = detPlaneList.front();
      const auto cartesianPlaneX = GetCartesianReadoutMap(planeX);
      const auto sizeX = cartesianPlaneX.at(1);
      const auto connectorsX = (int)cartesianPlaneX.at(2), orientX = (int)(cartesianPlaneX.at(2));

      TString planeY = detPlaneList.back();
      const auto cartesianPlaneY = GetCartesianReadoutMap(planeY);
      const auto sizeY = cartesianPlaneY.at(1);
      const auto connectorsY = (int)cartesianPlaneY.at(2), orientY = (int)cartesianPlaneY.at(3);
      f << "DET,  " << readoutBoard.Data() << ",   " << detectorType.Data() << ",   " << detector.Data() << ",   "
        << planeX.Data() << ",  " << sizeX << ",   " << connectorsX << ",   " << orientX << ",   " << planeY.Data()
        << ",   " << sizeY << ",   " << connectorsY << ",   " << orientY << "\n";
    } else if (readoutBoard == "UV_ANGLE") {
      const auto& detPlaneList = det.second;
      const auto& planeX = detPlaneList.front();
      const auto cartesianPlaneX = GetCartesianReadoutMap(planeX);
      float sizeX = cartesianPlaneX.at(1);
      const auto connectorsX = (int)cartesianPlaneX.at(2), orientX = (int)cartesianPlaneX.at(3);

      const auto& planeY = detPlaneList.back();
      const auto cartesianPlaneY = GetCartesianReadoutMap(planeY);
      float sizeY = cartesianPlaneY.at(1);
      const auto connectorsY = (int)cartesianPlaneY.at(2), orientY = (int)cartesianPlaneY.at(3);
      f << "DET,  " << readoutBoard.Data() << ",   " << detectorType.Data() << ",   " << detector.Data() << ",   "
        << planeX.Data() << ",  " << sizeX << ",   " << connectorsX << ",   " << orientX << ",   " << planeY.Data()
        << ",   " << sizeY << ",   " << connectorsY << ",   " << orientY << "\n";
    } else if (readoutBoard == "PADPLANE") {
      const auto& detPlaneList = det.second;
      const auto& padPlane = detPlaneList.back();
      const auto padSizeX = GetPadDetectorMap(detector).at(0), padSizeY = GetPadDetectorMap(detector).at(1),
                 nbOfPadX = GetPadDetectorMap(detector).at(2), nbOfPadY = GetPadDetectorMap(detector).at(3),
                 nbOfCons = GetPadDetectorMap(detector).at(4);
      f << "DET,  " << readoutBoard.Data() << ",   " << detectorType.Data() << ",   " << detector.Data() << ",  "
        << padPlane.Data() << ",  " << padSizeX << ",   " << nbOfPadX << ",   " << padSizeY << ",   " << nbOfPadY
        << ",  " << nbOfCons << "\n";
    } /*else if (readoutBoard == "CMSGEM")
      const auto& detPlaneList = det.second;*/
    else {
      SRS_WARNING("SRSMapping:SaveMapping")
          << "detector readout board type '" << readoutBoard.Data() << "' is not yet implemented ==> PLEASE MOVE ON.";
      continue;
    }
  }

  f << "###############################################################\n"
    << "#     fecId   adcCh   detPlane  apvOrient  apvIndex    apvHdr #\n"
    << "###############################################################\n";
  for (const auto& apv : fAPVFromIDMap) {
    int apvID = apv.first;
    int fecId = GetFECIDFromAPVID(apvID);
    int adcCh = GetADCChannelFromAPVID(apvID);
    TString detPlane = GetDetectorPlaneFromAPVID(apvID);
    int apvOrient = GetAPVOrientation(apvID);
    int apvIndex = GetAPVIndexOnPlane(apvID);
    int apvHdr = GetAPVHeaderLevelFromID(apvID);
    f << "APV,   " << fecId << ",     " << adcCh << ",     " << detPlane.Data() << ",     " << apvOrient << ",    "
      << apvIndex << ",   " << apvHdr << "\n";
  }
}

void SRSMapping::LoadDefaultMapping(const char* mappingCfgFilename) {
  Clear();
  SRS_INFO("SRSMapping:LoadDefaultMapping") << "Loading Mapping from '" << mappingCfgFilename << "'.";
  int apvNo = 0;
  int detID = 0;

  std::ifstream filestream(mappingCfgFilename, std::ifstream::in);
  TString line;
  while (line.ReadLine(filestream)) {
    line.Remove(TString::kLeading, ' ');  // strip leading spaces
    if (line.BeginsWith("#"))
      continue;  // and skip comments
    SRS_DEBUG("SRSMapping:LoadDefaultMapping") << "Scanning the mapping cfg file '" << line.Data() << "'.";

    //=== Create an array of the tokens separated by "," in the line;
    TObjArray* tokens = line.Tokenize(",");

    //=== iterator on the tokens array
    TIter myiter(tokens);
    while (TObjString* st = (TObjString*)myiter.Next()) {
      //== Remove leading and trailer spaces
      auto s = TString(st->GetString()).Remove(TString::kLeading, ' ');
      s.Remove(TString::kTrailing, ' ');

      SRS_DEBUG("SRSMapping:LoadDefaultMapping") << "Data ==> '" << s.Data() << "'.";
      if (s == "DET") {
        TString readoutBoard = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
        TString detectorType = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
        TString detector = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');

        if (readoutBoard == "CARTESIAN") {
          TString planeX = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float sizeX = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          int nbOfConnectorsX =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientationX = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();

          TString planeY = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float sizeY = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          int nbOfConnectorsY =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientationY = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          SetCartesianStripsReadoutMap(readoutBoard,
                                       detectorType,
                                       detector,
                                       detID,
                                       planeX,
                                       sizeX,
                                       nbOfConnectorsX,
                                       orientationX,
                                       planeY,
                                       sizeY,
                                       nbOfConnectorsY,
                                       orientationY);
        } else if (readoutBoard == "1DSTRIPS") {
          TString plane = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float size = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          int nbOfConnectors =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientation = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          Set1DStripsReadoutMap(readoutBoard, detectorType, detector, detID, plane, size, nbOfConnectors, orientation);
        } else if (readoutBoard == "UV_ANGLE_OLD") {
          TString planeX = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float sizeX = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          int nbOfConnectorsX =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientationX = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();

          TString planeY = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float sizeY = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          int nbOfConnectorsY =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientationY = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          SetCartesianStripsReadoutMap(readoutBoard,
                                       detectorType,
                                       detector,
                                       detID,
                                       planeX,
                                       sizeX,
                                       nbOfConnectorsX,
                                       orientationX,
                                       planeY,
                                       sizeY,
                                       nbOfConnectorsY,
                                       orientationY);
        } else if (readoutBoard == "UV_ANGLE") {
          float length = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float outerRadius =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float innerRadius =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();

          TString planeTop = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          int nbOfConnectorsTop =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientationTop =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();

          TString planeBot = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          int nbOfConnectorsBot =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          int orientationBot =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          SetUVStripsReadoutMap(readoutBoard,
                                detectorType,
                                detector,
                                detID,
                                length,
                                innerRadius,
                                outerRadius,
                                planeTop,
                                nbOfConnectorsTop,
                                orientationTop,
                                planeBot,
                                nbOfConnectorsBot,
                                orientationBot);
        } else if (readoutBoard == "PADPLANE") {
          TString padPlane = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float padSizeX = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float nbPadX = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float padSizeY = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float nbPadY = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float nbConnectors =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          SetPadsReadoutMap(
              readoutBoard, detectorType, detector, detID, padPlane, padSizeX, padSizeY, nbPadX, nbPadY, nbConnectors);
        } else if (readoutBoard == "CMSGEM") {
          TString etaSector = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
          float etaSectorPos =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float etaSectorSize =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          float nbConnectors =
              (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atof();
          int orientation = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
          SetCMSGEMReadoutMap(readoutBoard,
                              detectorType,
                              detector,
                              detID,
                              etaSector,
                              etaSectorPos,
                              etaSectorSize,
                              nbConnectors,
                              orientation);
        } else {
          SRS_ERROR("SRSMapping:LoadDefaultMapping") << "detector with this readout board type '" << readoutBoard.Data()
                                                     << "' is not yet implemented ==> PLEASE MOVE ON.";
          continue;
        }
        detID++;
      } else if (s == "APV") {
        int fecId = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        int adcCh = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        TString detPlane = TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ');
        int apvOrient = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        int apvIndex = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        int apvheader = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        if (detPlane == "NULL")
          continue;
        SetAPVMap(detPlane, fecId, adcCh, apvNo, apvOrient, apvIndex, apvheader);
        apvNo++;
      }
    }
    tokens->Delete();
  }
}

void SRSMapping::LoadAPVtoPadMapping(const char* mappingCfgFilename) {
  //  Clear() ;
  SRS_INFO("SRSMapping:LoadAPVtoPadMapping") << "Loading Mapping from '" << mappingCfgFilename << "'.";
  std::ifstream filestream(mappingCfgFilename, std::ifstream::in);
  TString line;
  while (line.ReadLine(filestream)) {
    line.Remove(TString::kLeading, ' ');  // strip leading spaces
    if (line.BeginsWith("#"))
      continue;  // and skip comments
    SRS_DEBUG("SRSMapping:LoadAPVtoPadMapping") << "Scanning the mapping cfg file '" << line.Data() << "'.";

    //=== Create an array of the tokens separated by "," in the line;
    TObjArray* tokens = line.Tokenize(",");

    //=== iterator on the tokens array
    TIter myiter(tokens);
    while (TObjString* st = (TObjString*)myiter.Next()) {
      //== Remove leading and trailer spaces
      auto s = TString(st->GetString()).Remove(TString::kLeading, ' ');
      s.Remove(TString::kTrailing, ' ');
      if (s == "PAD") {
        int apvCh = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        int padId = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        ;
        int fecId = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        int adcCh = (TString(((TObjString*)myiter.Next())->GetString()).Remove(TString::kLeading, ' ')).Atoi();
        SetAPVtoPadMapping(fecId, adcCh, padId, apvCh);
      }
    }
    tokens->Delete();
  }
}

void SRSMapping::Clear(Option_t*) {
  SRS_INFO("SRSMapping:Clear") << "Clearing Previous Mapping.";

  fAPVIDFromAPVNoMap.clear();
  fAPVIDFromNameMap.clear();
  fAPVIDListFromDetectorMap.clear();
  fAPVIDListFromDetectorPlaneMap.clear();
  fAPVNoFromIDMap.clear();
  fAPVFromIDMap.clear();
  fAPVHeaderLevelFromIDMap.clear();

  fPlaneIDFromPlaneMap.clear();
  fDetectorIDFromDetectorMap.clear();
  fDetectorFromIDMap.clear();
  fDetectorFromAPVIDMap.clear();
  fDetectorFromPlaneMap.clear();

  fDetectorPlaneFromAPVIDMap.clear();

  fReadoutBoardFromIDMap.clear();
  fReadoutBoardFromDetectorMap.clear();

  fNbOfAPVsFromDetectorMap.clear();

  fAPVOrientationFromIDMap.clear();
  fAPVIndexOnPlaneFromIDMap.clear();

  SRS_INFO("SRSMapping:Clear") << "Previous Mapping cleared.";
}

template <typename M>
void ClearMapOfList(M& amap) {
  for (auto& elem : amap)
    elem.second.clear();
  amap.clear();
}
