#include "srsreco/SRSFECPedestalDecoder.h"
#include "srsutils/Logging.h"

ClassImp(SRSFECPedestalDecoder);

SRSFECPedestalDecoder::SRSFECPedestalDecoder(int nwords, unsigned int *buf)
    : fBuf(buf), fNWords(nwords), fFECEvents(new TList) {
  auto mapping = SRSMapping::GetInstance();
  const auto apvNoFromApvIDMap = mapping->GetAPVNoFromIDMap();

  fActiveFecChannelsMap.clear();
  for (const auto &fecChannel : apvNoFromApvIDMap) {
    const auto &apvid = fecChannel.first;
    const auto activeChannel = apvid & 0xF, fecId = (apvid >> 4) & 0xF;
    fActiveFecChannelsMap[fecId].push_back(activeChannel);
    SRS_DEBUG("SRSFECPedestalDecoder") << "List of  fecNo=" << fecId << ", activeChannel = " << activeChannel << ".";
  }

  int current_offset = 0, fecID = 0;
  //==========================================================================//
  // Needed as the key to link apvID (or fecChannel) to the apvEvent in the TList  //
  // Should be < to 15 (max 16 APV channel in the FEC)                        //
  //==========================================================================//

  int fecChannel = 0, apvID = 0;
  unsigned int currentAPVPacketHdr;
  int previousAPVPacketSize = 0;

  std::vector<unsigned int> data32BitsVector;
  data32BitsVector.clear();

  //===============================================================================//
  // Dealing with the 7 Equipment header words. We just skip the first 2 words     //
  // and go straight to the 3rd word  where we extract the FEC no (Equip Id)       //
  //===============================================================================//
  current_offset += 2;
  unsigned int eqHeaderWord = fBuf[current_offset];
  fecID = eqHeaderWord & 0xff;
  fActiveFecChannels.clear();
  fActiveFecChannels = fActiveFecChannelsMap[fecID];
  SRS_DEBUG("SRSFECPedestalDecoder") << "List of  fecNo = " << fecID << ".";

  //=== The next 4 words are Equip word, we dont care
  current_offset += 5;

  //================================================================================//
  // Start looking at the APV data word from here                                   //
  //================================================================================//
  while (current_offset < fNWords) {
    unsigned int rawdata = fBuf[current_offset];
    if (((rawdata >> 8) & 0xffffff) == 0x414443)
      SRS_DEBUG("SRSFECPedestalDecoder") << "dataWord=0x" << std::hex << rawdata << ".";
    //=============================================================================//
    // end of event ==> break: add the data from the last sample here              //
    //=============================================================================//

    if (rawdata == 0xfafafafa) {
      //===================================================================================================//
      // last word of the previous packet added for Filippo in DATE to count the eventNb x 16 UDP packets  //
      // We dont need it here, will just skip it We remove it from the vector of data                      //
      //===================================================================================================//
      if (!data32BitsVector.empty()) {
        BuildAPVEvent(data32BitsVector, fecID, fecChannel);
      }

      fecChannel = 0;
      data32BitsVector.clear();
      current_offset++;
      break;
    }

    //==========================================================================================//
    // Word with the event number (trigger count) and the packet size information               //
    //==========================================================================================//
    /*if (fIsNewPacket) {
      previousAPVPacketSize = (rawdata & 0xffff);
      fEventNb = (rawdata & 0xffff);
      SRS_DEBUG("SRSFECPedestalDecoder") << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                         << ", packet size = " << previousAPVPacketSize << ".";
      fIsNewPacket = kFALSE;
      current_offset++;
      continue;
    }*/

    //==========================================================================================//
    //                                     size of APV packet                                    //
    //==========================================================================================//
    if (fIsNewPacket) {
      //    if (((rawdata >> 8) & 0xffff) == 0xaabb) {
      fPacketSize = (rawdata & 0xffff);
      SRS_DEBUG("SRSFECPedestalDecoder") << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                         << ", packet size = " << fPacketSize << "\n"
                                         << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                         << ", packet size = " << previousAPVPacketSize << ".";
      data32BitsVector.clear();
      fIsNewPacket = kFALSE;
      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         New packet (or frame) FEC channel data in the equipment                                         //
    //=========================================================================================================//
    if (((rawdata >> 8) & 0xffffff) == 0x414443) {
      int datasize = data32BitsVector.size();
      SRS_DEBUG("SRSFECPedestalDecoder") << "dataWord=0x" << std::hex << rawdata << std::dec << ", fecID=" << fecID
                                         << ", apvID=" << apvID << ", FEC Channel=" << fecChannel
                                         << ", data size=" << datasize << ".";

      data32BitsVector.pop_back();
      if (!data32BitsVector.empty())
        BuildAPVEvent(data32BitsVector, fecID, fecChannel);

      currentAPVPacketHdr = rawdata;
      fecChannel = currentAPVPacketHdr & 0xff;
      SRS_DEBUG("SRSFECPedestalDecoder") << "List of  fecNo=" << fecID << ", adcChannel = " << fecChannel << ".";

      //=== REINITIALISE EVERYTHING
      if (fecChannel > 15) {
        SRS_ERROR("SRSFECPedestalDecoder")
            << "fecID=" << fecID << ", apvID=" << apvID << ", FEC Channel = " << fecChannel << " > 15.";
        break;
      }

      data32BitsVector.clear();
      fIsNewPacket = kTRUE;
      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         apv data in the packet (frame)                                                                 //
    //========================================================================================================//
    if (!fIsNewPacket) {
      data32BitsVector.push_back(rawdata);
      current_offset++;
      continue;
    }
  }
}

void SRSFECPedestalDecoder::BuildAPVEvent(std::vector<unsigned int> data32bits, int fec_no, int fec_channel) {
  int apvID = (fec_no << 4) | fec_channel;
  if (find(fActiveFecChannels.begin(), fActiveFecChannels.end(), fec_channel) != fActiveFecChannels.end()) {
    int sigmaCut = 0;
    auto *apvEvent = new SRSAPVEvent(fec_no, fec_channel, apvID, sigmaCut, fEventNb, fPacketSize);
    for (const auto &data : data32bits)
      apvEvent->Add32BitsRawData(data);
    fFECEvents->Add(apvEvent);
  }
}

SRSAPVEvent *SRSFECPedestalDecoder::GetAPVEvent(int apvID) {
  auto mapping = SRSMapping::GetInstance();
  return (SRSAPVEvent *)(fFECEvents->At(mapping->GetAPVNoFromID(apvID)));
}

SRSFECPedestalDecoder::~SRSFECPedestalDecoder() {
  for (auto &activeChannel : fActiveFecChannelsMap)
    activeChannel.second.clear();
  fActiveFecChannelsMap.clear();
  TIter nextAPVEvent(fFECEvents.get());
  while (SRSAPVEvent *apvEvent = (SRSAPVEvent *)nextAPVEvent())
    delete apvEvent;
  fFECEvents->Delete();
}
