#include "srsreco/SRSFECEventDecoder.h"
#include "srsutils/Logging.h"

ClassImp(SRSFECEventDecoder);

SRSFECEventDecoder::SRSFECEventDecoder(int nwords, unsigned int* buf, SRSEventBuilder* eventBuilder)
    : fBuf(buf), fNWords(nwords) {
  auto mapping = SRSMapping::GetInstance();
  const auto apvNoFromApvIDMap = mapping->GetAPVNoFromIDMap();

  int size = apvNoFromApvIDMap.size();
  SRS_DEBUG("SRSFECEventDecoder") << "List of size = " << size << ".";

  fActiveFecChannelsMap.clear();
  for (const auto& adcChannel : apvNoFromApvIDMap) {
    const auto& apvid = adcChannel.first;
    const auto activeChannel = apvid & 0xF, fecId = (apvid >> 4) & 0xF;
    fActiveFecChannelsMap[fecId].push_back(activeChannel);
    SRS_DEBUG("SRSFECEventDecoder") << "List of  fecNo=" << fecId << ", activeChannel = " << activeChannel << ".";
  }

  //==========================================================================//
  // Needed as the key to link apvID (or adcChannel) to the apvEvent in the TList  //
  // Should be < to 15 (max 16 APV channel in the FEC)                        //
  //==========================================================================//

  size_t current_offset = 0, fecID = 0;
  size_t adcChannel = 0, apvID = 0;
  size_t previousAPVPacketSize = 0;

  std::vector<unsigned int> data32BitsVector;

  //===============================================================================//
  // Dealing with the 7 Equipment header words. We just skip the first 2 words     //
  // and go straight to the 3rd word  where we extract the FEC no (Equip Id)       //
  //===============================================================================//
  current_offset += 2;
  unsigned int eqHeaderWord = fBuf[current_offset];
  fecID = eqHeaderWord & 0xff;
  fActiveFecChannels.clear();
  fActiveFecChannels = fActiveFecChannelsMap[fecID];

  SRS_DEBUG("SRSFECEventDecoder") << "List of  fecNo=" << fecID << ".";

  //=== The next 4 words are Equip word, we dont care
  current_offset += 5;

  //================================================================================//
  // Start looking at the APV data word from here                                   //
  //================================================================================//
  while (current_offset < fNWords) {
    unsigned int rawdata = fBuf[current_offset];
    //if (((rawdata >> 8) & 0xffffff) == 0x414443)
    SRS_DEBUG("SRSFECEventDecoder") << "dataWord=0x" << std::hex << rawdata << ".";
    //=============================================================================//
    // end of event ==> break: add the data from the last sample here              //
    //=============================================================================//

    if (rawdata == 0xfafafafa) {
      //===================================================================================================//
      // last word of the previous packet added for Filippo in DATE to count the eventNb x 16 UDP packets  //
      // We dont need it here, will just skip it We remove it from the vector of data                      //
      //===================================================================================================//
      if (!data32BitsVector.empty()) {
        apvID = (fecID << 4) | adcChannel;
        BuildRawAPVEvents(data32BitsVector, fecID, adcChannel, eventBuilder);
        //int datasize = data32BitsVector.size();
      }

      adcChannel = 0;
      data32BitsVector.clear();
      current_offset++;
      break;
    }

    //==========================================================================================//
    // Word with the event number (trigger count) and the packet size information               //
    //                                     size of APV packet                                    //
    //==========================================================================================//
    if (fIsNewPacket) {
      //    if (((rawdata >> 8) & 0xffff) == 0xaabb) {
      fPacketSize = (rawdata & 0xffff);
      SRS_DEBUG("SRSFECEventDecoder") << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                      << ", packet size=" << fPacketSize << "\n"
                                      << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                      << ", packet size=" << previousAPVPacketSize << ".";
      data32BitsVector.clear();
      fIsNewPacket = kFALSE;
      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         New packet (or frame) FEC channel data in the equipment                                         //
    //=========================================================================================================//
    if (((rawdata >> 8) & 0xffffff) == 0x414443) {
      data32BitsVector.pop_back();
      if (!data32BitsVector.empty()) {
        apvID = (fecID << 4) | adcChannel;
        BuildRawAPVEvents(data32BitsVector, fecID, adcChannel, eventBuilder);
      }

      const auto currentAPVPacketHdr = rawdata;
      adcChannel = currentAPVPacketHdr & 0xff;

      //=== REINITIALISE EVERYTHING
      if (adcChannel > 15) {
        SRS_ERROR("SRSFECEventDecoder") << "fecID=" << fecID << ", ADC Channel=" << adcChannel << ", apvID=" << apvID
                                        << ".";
        break;
      }

      data32BitsVector.clear();
      fIsNewPacket = kTRUE;

      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         apv data in the packet (frame)                                                                 //
    //========================================================================================================//
    if (!fIsNewPacket) {
      data32BitsVector.push_back(rawdata);
      current_offset++;
      continue;
    }
  }
}

SRSFECEventDecoder::SRSFECEventDecoder(
    int nwords, unsigned int* buf, SRSPedestal* ped, SRSPositionCorrection* clusterPosCorr, int zeroSupCut)
    : fBuf(buf), fNWords(nwords) {
  auto mapping = SRSMapping::GetInstance();
  const auto apvNoFromApvIDMap = mapping->GetAPVNoFromIDMap();

  SRS_DEBUG("SRSFECEventDecoder") << "List of size = " << apvNoFromApvIDMap.size() << ".";

  fActiveFecChannelsMap.clear();
  for (const auto& adcChannel : apvNoFromApvIDMap) {
    const auto& apvid = adcChannel.first;
    const auto activeChannel = apvid & 0xF, fecId = (apvid >> 4) & 0xF;
    fActiveFecChannelsMap[fecId].push_back(activeChannel);
    SRS_DEBUG("SRSFECEventDecoder") << "List of  fecNo=" << fecId << ", activeChannel = " << activeChannel << ".";
  }

  //===============================================================================//
  // Needed as the key to link apvID (or adcChannel) to the apvEvent in the TList  //
  // Should be < to 15 (max 16 APV channel in the FEC)                             //
  //===============================================================================//

  size_t current_offset = 0, fecID = 0;
  size_t adcChannel = 0, apvID = 0;
  size_t previousAPVPacketSize = 0;

  std::vector<unsigned int> data32BitsVector;

  //===============================================================================//
  // Dealing with the 7 Equipment header words. We just skip the first 2 words     //
  // and go straight to the 3rd word  where we extract the FEC no (Equip Id)       //
  //===============================================================================//
  current_offset += 2;
  unsigned int eqHeaderWord = fBuf[current_offset];
  fecID = eqHeaderWord & 0xff;
  fActiveFecChannels.clear();
  fActiveFecChannels = fActiveFecChannelsMap[fecID];

  //=== The next 4 words are Equip word, we dont care
  current_offset += 5;

  //================================================================================//
  // Start looking at the APV data word from here                                   //
  //================================================================================//
  while (current_offset < fNWords) {
    unsigned int rawdata = fBuf[current_offset];
    //if (((rawdata >> 8) & 0xffffff) == 0x414443)
    SRS_DEBUG("SRSFECEventDecoder") << "dataWord=0x" << std::hex << rawdata << ".";
    //=============================================================================//
    // end of event ==> break: add the data from the last sample here              //
    //=============================================================================//

    if (rawdata == 0xfafafafa) {
      //===================================================================================================//
      // last word of the previous packet added for Filippo in DATE to count the eventNb x 16 UDP packets  //
      // We dont need it here, will just skip it We remove it from the vector of data                      //
      //===================================================================================================//
      if (!data32BitsVector.empty()) {
        apvID = (fecID << 4) | adcChannel;
        BuildHitForPositionCorrection(data32BitsVector, fecID, adcChannel, ped, clusterPosCorr, zeroSupCut);
        //int datasize = data32BitsVector.size();
      }

      adcChannel = 0;
      data32BitsVector.clear();
      current_offset++;
      break;
    }

    //==========================================================================================//
    // Word with the event number (trigger count) and the packet size information               //
    //                                     size of APV packet                                    //
    //==========================================================================================//
    if (fIsNewPacket) {
      //    if (((rawdata >> 8) & 0xffff) == 0xaabb) {
      fPacketSize = (rawdata & 0xffff);
      SRS_DEBUG("SRSFECEventDecoder") << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                      << ", packet size=" << fPacketSize << "\n"
                                      << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                      << ", packet size=" << previousAPVPacketSize << ".";
      data32BitsVector.clear();
      fIsNewPacket = kFALSE;
      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         New packet (or frame) FEC channel data in the equipment                                         //
    //=========================================================================================================//
    if (((rawdata >> 8) & 0xffffff) == 0x414443) {
      data32BitsVector.pop_back();
      if (!data32BitsVector.empty()) {
        apvID = (fecID << 4) | adcChannel;
        BuildHitForPositionCorrection(data32BitsVector, fecID, adcChannel, ped, clusterPosCorr, zeroSupCut);
      }

      const auto currentAPVPacketHdr = rawdata;
      adcChannel = currentAPVPacketHdr & 0xff;

      //=== REINITIALISE EVERYTHING
      if (adcChannel > 15) {
        SRS_INFO("SRSFECEventDecoder") << "fecID=" << fecID << ", ADC Channel=" << adcChannel << ", apvID=" << apvID
                                       << ".";
        break;
      }

      data32BitsVector.clear();
      fIsNewPacket = kTRUE;

      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         apv data in the packet (frame)                                                                 //
    //========================================================================================================//
    if (!fIsNewPacket) {
      data32BitsVector.push_back(rawdata);
      current_offset++;
      continue;
    }
  }
}

SRSFECEventDecoder::SRSFECEventDecoder(
    int nwords, unsigned int* buf, SRSPedestal* ped, SRSEventBuilder* eventBuilder, int zeroSupCut) {
  auto mapping = SRSMapping::GetInstance();
  std::map<int, int> apvNoFromApvIDMap = mapping->GetAPVNoFromIDMap();

  SRS_DEBUG("SRSFECEventDecoder") << "List of size = " << apvNoFromApvIDMap.size() << ".";

  fBuf = buf;
  fNWords = nwords;
  fEventNb = -1;
  fIsNewPacket = kFALSE;
  fPacketSize = 4000;

  fActiveFecChannelsMap.clear();
  for (const auto& adcChannel : apvNoFromApvIDMap) {
    const auto& apvid = adcChannel.first;
    const auto activeChannel = apvid & 0xF, fecId = (apvid >> 4) & 0xF;
    fActiveFecChannelsMap[fecId].push_back(activeChannel);
    SRS_DEBUG("SRSFECEventDecoder") << "List of  fecNo=" << fecId << ", activeChannel = " << activeChannel << ".";
  }

  //==========================================================================//
  // Needed as the key to link apvID (or adcChannel) to the apvEvent in the TList  //
  // Should be < to 15 (max 16 APV channel in the FEC)                        //
  //==========================================================================//

  size_t current_offset = 0, fecID = 0;
  size_t adcChannel = 0, apvID = 0;
  size_t previousAPVPacketSize = 0;

  std::vector<unsigned int> data32BitsVector;

  //===============================================================================//
  // Dealing with the 7 Equipment header words. We just skip the first 2 words     //
  // and go straight to the 3rd word  where we extract the FEC no (Equip Id)       //
  //===============================================================================//
  current_offset += 2;
  unsigned int eqHeaderWord = fBuf[current_offset];
  fecID = eqHeaderWord & 0xff;
  fActiveFecChannels.clear();
  fActiveFecChannels = fActiveFecChannelsMap[fecID];

  //=== The next 4 words are Equip word, we dont care
  current_offset += 5;

  //================================================================================//
  // Start looking at the APV data word from here                                   //
  //================================================================================//
  while (current_offset < fNWords) {
    unsigned int rawdata = fBuf[current_offset];
    if (((rawdata >> 8) & 0xffffff) == 0x414443)
      SRS_DEBUG("SRSFECEventDecoder") << "dataWord=0x" << std::hex << rawdata << ".";
    //=============================================================================//
    // end of event ==> break: add the data from the last sample here              //
    //=============================================================================//

    if (rawdata == 0xfafafafa) {
      //===================================================================================================//
      // last word of the previous packet added for Filippo in DATE to count the eventNb x 16 UDP packets  //
      // We dont need it here, will just skip it We remove it from the vector of data                      //
      //===================================================================================================//
      if (!data32BitsVector.empty()) {
        apvID = (fecID << 4) | adcChannel;
        BuildHits(data32BitsVector, fecID, adcChannel, ped, eventBuilder, zeroSupCut);
        //int datasize = data32BitsVector.size();
      }

      adcChannel = 0;
      data32BitsVector.clear();
      current_offset++;
      break;
    }

    //==========================================================================================//
    // Word with the event number (trigger count) and the packet size information               //
    //                                     size of APV packet                                    //
    //==========================================================================================//
    if (fIsNewPacket) {
      //    if (((rawdata >> 8) & 0xffff) == 0xaabb) {
      fPacketSize = (rawdata & 0xffff);
      SRS_DEBUG("SRSFECEventDecoder") << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                      << ", packet size=" << fPacketSize << "\n"
                                      << "Sorin 2nd header word=0x" << std::hex << rawdata << std::dec
                                      << ", packet size=" << previousAPVPacketSize << ".";
      data32BitsVector.clear();
      fIsNewPacket = kFALSE;
      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         New packet (or frame) FEC channel data in the equipment                                         //
    //=========================================================================================================//
    if (((rawdata >> 8) & 0xffffff) == 0x414443) {
      data32BitsVector.pop_back();
      if (!data32BitsVector.empty()) {
        apvID = (fecID << 4) | adcChannel;
        BuildHits(data32BitsVector, fecID, adcChannel, ped, eventBuilder, zeroSupCut);
      }

      const auto currentAPVPacketHdr = rawdata;
      adcChannel = currentAPVPacketHdr & 0xff;

      //=== REINITIALISE EVERYTHING
      if (adcChannel > 15) {
        SRS_ERROR("SRSFECEventDecoder") << "fecID=" << fecID << ", ADC Channel=" << adcChannel << ", apvID=" << apvID
                                        << ".";
        break;
      }

      data32BitsVector.clear();
      fIsNewPacket = kTRUE;

      current_offset++;
      continue;
    }

    //=========================================================================================================//
    //         apv data in the packet (frame)                                                                 //
    //========================================================================================================//
    if (!fIsNewPacket) {
      data32BitsVector.push_back(rawdata);
      current_offset++;
      continue;
    }
  }
}

void SRSFECEventDecoder::BuildHits(std::vector<unsigned int> data32bits,
                                   int fec_id,
                                   int adc_channel,
                                   SRSPedestal* ped,
                                   SRSEventBuilder* eventBuilder,
                                   int zeroSupCut) {
  auto mapping = SRSMapping::GetInstance();
  int apvID = (fec_id << 4) | adc_channel;
  if (find(fActiveFecChannels.begin(), fActiveFecChannels.end(), adc_channel) != fActiveFecChannels.end()) {
    auto apvEvent = std::make_unique<SRSAPVEvent>(fec_id, adc_channel, apvID, zeroSupCut, fEventNb, fPacketSize);
    apvEvent->SetHitMaxOrTotalADCs(eventBuilder->GetHitMaxOrTotalADCs());
    for (const auto& data : data32bits)
      apvEvent->Add32BitsRawData(data);

    apvEvent->SetAllFlags(kTRUE, kTRUE);
    apvEvent->SetPedestals(ped->GetAPVNoises(apvEvent->GetAPVID()),
                           ped->GetAPVOffsets(apvEvent->GetAPVID()),
                           ped->GetAPVMaskedChannels(apvEvent->GetAPVID()));
    auto listOfHits = apvEvent->ComputeListOfAPVHits();
    for (auto* hit : listOfHits)
      eventBuilder->AddHitInDetectorPlane(hit);
    eventBuilder->AddMeanDetectorPlaneNoise(apvEvent->GetPlane(), apvEvent->GetMeanAPVnoise());
    listOfHits.clear();
  }
}

void SRSFECEventDecoder::BuildHitForPositionCorrection(std::vector<unsigned int> data32bits,
                                                       int fec_id,
                                                       int adc_channel,
                                                       SRSPedestal* ped,
                                                       SRSPositionCorrection* clusterPosCorr,
                                                       int zeroSupCut) {
  auto mapping = SRSMapping::GetInstance();
  int apvID = (fec_id << 4) | adc_channel;
  if (find(fActiveFecChannels.begin(), fActiveFecChannels.end(), adc_channel) != fActiveFecChannels.end()) {
    auto apvEvent = std::make_unique<SRSAPVEvent>(fec_id, adc_channel, apvID, zeroSupCut, fEventNb, fPacketSize);
    for (const auto& data : data32bits)
      apvEvent->Add32BitsRawData(data);
    apvEvent->SetAllFlags(kTRUE, kTRUE);
    apvEvent->SetPedestals(ped->GetAPVNoises(apvEvent->GetAPVID()),
                           ped->GetAPVOffsets(apvEvent->GetAPVID()),
                           ped->GetAPVMaskedChannels(apvEvent->GetAPVID()));
    auto listOfHits = apvEvent->ComputeListOfAPVHits();
    for (auto* hit : listOfHits)
      clusterPosCorr->AddHitInDetectorPlane(hit);
    listOfHits.clear();
  }
}

void SRSFECEventDecoder::BuildRawAPVEvents(std::vector<unsigned int> data32bits,
                                           int fec_id,
                                           int adc_channel,
                                           SRSEventBuilder* eventBuilder) {
  auto mapping = SRSMapping::GetInstance();
  int apvID = (fec_id << 4) | adc_channel;
  SRS_DEBUG("SRSFECEventDecoder:BuildRawAPVEvents")
      << "fecID=" << fec_id << ", ADC Channel=" << adc_channel << ", apvID=" << apvID << ".";

  if (find(fActiveFecChannels.begin(), fActiveFecChannels.end(), adc_channel) != fActiveFecChannels.end()) {
    SRSAPVEvent* apvEvent = new SRSAPVEvent(fec_id, adc_channel, apvID, 0, fEventNb, fPacketSize);
    for (const auto& data : data32bits)
      apvEvent->Add32BitsRawData(data);
    eventBuilder->AddAPVEvent(apvEvent);
  }
}

SRSFECEventDecoder::~SRSFECEventDecoder() {
  for (auto& activeChannel : fActiveFecChannelsMap)
    activeChannel.second.clear();
  fActiveFecChannelsMap.clear();
}
