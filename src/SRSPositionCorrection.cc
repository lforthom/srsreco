#include <TCanvas.h>
#include <TF1.h>
#include <TFile.h>
#include <TH1F.h>
#include <TROOT.h>
#include <TStyle.h>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSPositionCorrection.h"
#include "srsutils/Logging.h"

ClassImp(SRSPositionCorrection);

SRSPositionCorrection::SRSPositionCorrection(TString runname, TString runtype) {
  fRunName = runname;
  fRunType = runtype;
  BookClusterPositionCorrection();
}

SRSPositionCorrection::~SRSPositionCorrection() {
  DeleteHitsInDetectorPlaneMap(fHitsInDetectorPlaneMap);
  DeleteClustersInDetectorPlaneMap(fClustersInDetectorPlaneMap);
  Clear();
}

void SRSPositionCorrection::Clear(Option_t *) {
  SRS_INFO("SRSPositionCorrection::Clear") << "ENTER";

  auto mapping = SRSMapping::GetInstance();
  for (const auto &det : mapping->GetDetectorFromIDMap()) {
    const auto &detID = det.first;
    const auto &detName = det.second;
    for (const auto &plane : mapping->GetDetectorPlaneListFromDetector(detName)) {
      const auto planeID = (int)mapping->GetPlaneIDorEtaSector(plane), histoID = (2 * detID) + planeID;

      fEtaFunctionHistos[histoID]->Delete();
      fEta2PosHistos[histoID]->Delete();
      fEta3PosHistos[histoID]->Delete();
      fEta4PosHistos[histoID]->Delete();
      fEta5PosHistos[histoID]->Delete();
      fEta6PlusPosHistos[histoID]->Delete();

      fEta2NegHistos[histoID]->Delete();
      fEta3NegHistos[histoID]->Delete();
      fEta4NegHistos[histoID]->Delete();
      fEta5NegHistos[histoID]->Delete();
      fEta6PlusNegHistos[histoID]->Delete();
    }
    mapping->GetDetectorPlaneListFromDetector(detName).clear();
  }
  mapping->GetDetectorFromIDMap().clear();
  SRS_INFO("SRSPositionCorrection:Clear") << "After delete Histo one by one.";

  if (fEtaFunctionHistos)
    delete[] fEtaFunctionHistos;
  if (fEta2PosHistos)
    delete[] fEta2PosHistos;
  if (fEta3PosHistos)
    delete[] fEta3PosHistos;
  if (fEta4PosHistos)
    delete[] fEta4PosHistos;
  if (fEta5PosHistos)
    delete[] fEta5PosHistos;
  if (fEta6PlusPosHistos)
    delete[] fEta6PlusPosHistos;

  if (fEta2NegHistos)
    delete[] fEta2NegHistos;
  if (fEta3NegHistos)
    delete[] fEta3NegHistos;
  if (fEta4NegHistos)
    delete[] fEta4NegHistos;
  if (fEta5NegHistos)
    delete[] fEta5NegHistos;
  if (fEta6PlusNegHistos)
    delete[] fEta6PlusNegHistos;

  SRS_INFO("SRSPositionCorrection:Clear") << "After delete array of PosHistos.";
}

void SRSPositionCorrection::BookClusterPositionCorrection() {
  auto mapping = SRSMapping::GetInstance();
  int nbOfPlanes = mapping->GetNbOfDetectorPlane();
  fEtaFunctionHistos = new TH1F *[nbOfPlanes];
  fEta2PosHistos = new TH1F *[nbOfPlanes];
  fEta3PosHistos = new TH1F *[nbOfPlanes];
  fEta4PosHistos = new TH1F *[nbOfPlanes];
  fEta5PosHistos = new TH1F *[nbOfPlanes];
  fEta6PlusPosHistos = new TH1F *[nbOfPlanes];

  fEta2NegHistos = new TH1F *[nbOfPlanes];
  fEta3NegHistos = new TH1F *[nbOfPlanes];
  fEta4NegHistos = new TH1F *[nbOfPlanes];
  fEta5NegHistos = new TH1F *[nbOfPlanes];
  fEta6PlusNegHistos = new TH1F *[nbOfPlanes];

  for (const auto &det : mapping->GetDetectorFromIDMap()) {
    const auto &detID = det.first;
    const auto &detName = det.second;
    for (const auto &plane : mapping->GetDetectorPlaneListFromDetector(detName)) {
      const auto planeID = (int)mapping->GetPlaneIDorEtaSector(plane), histoID = (2 * detID) + planeID;
      fEtaFunctionHistos[histoID] = BookHisto("etaFunction" + plane, "eta Function: " + plane);
      fEtaFunctionHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEtaFunctionHistos[histoID]->SetYTitle("Counts");

      fEta2PosHistos[histoID] = BookHisto("eta2FunctionPos" + plane, "eta2 Function Pos: " + plane);
      fEta2PosHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta2PosHistos[histoID]->SetYTitle("Counts");

      fEta3PosHistos[histoID] = BookHisto("eta3FunctionPos" + plane, "eta3 Function Pos: " + plane);
      fEta3PosHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta3PosHistos[histoID]->SetYTitle("Counts");

      fEta4PosHistos[histoID] = BookHisto("eta4FunctionPos" + plane, "eta4 Function Pos: " + plane);
      fEta4PosHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta4PosHistos[histoID]->SetYTitle("Counts");

      fEta5PosHistos[histoID] = BookHisto("eta5FunctionPos" + plane, "eta5 Function Pos: " + plane);
      fEta5PosHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta5PosHistos[histoID]->SetYTitle("Counts");

      fEta6PlusPosHistos[histoID] = BookHisto("eta6PlusFunctionPos" + plane, "eta6 and more Function Pos: " + plane);
      fEta6PlusPosHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta6PlusPosHistos[histoID]->SetYTitle("Counts");

      fEta2NegHistos[histoID] = BookHisto("eta2FunctionNeg" + plane, "eta2 Function Neg: " + plane);
      fEta2NegHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta2NegHistos[histoID]->SetYTitle("Counts");

      fEta3NegHistos[histoID] = BookHisto("eta3FunctionNeg" + plane, "eta3 Function Neg: " + plane);
      fEta3NegHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta3NegHistos[histoID]->SetYTitle("Counts");

      fEta4NegHistos[histoID] = BookHisto("eta4FunctionNeg" + plane, "eta4 Function Neg: " + plane);
      fEta4NegHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta4NegHistos[histoID]->SetYTitle("Counts");

      fEta5NegHistos[histoID] = BookHisto("eta5FunctionNeg" + plane, "eta5 Function Neg: " + plane);
      fEta5NegHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta5NegHistos[histoID]->SetYTitle("Counts");

      fEta6PlusNegHistos[histoID] = BookHisto("eta6PlusFunctionNeg" + plane, "eta6 and more Function Neg: " + plane);
      fEta6PlusNegHistos[histoID]->SetXTitle("(cluster Pos - Central Strip Pos) / pitch ");
      fEta6PlusNegHistos[histoID]->SetYTitle("Counts");
    }
    mapping->GetDetectorPlaneListFromDetector(detName).clear();
  }
  mapping->GetDetectorFromIDMap().clear();
}

TH1F *SRSPositionCorrection::BookHisto(TString histoName, TString histoTitle) {
  float min = -0.50;
  float max = 0.50;
  if (histoName.Contains("Neg")) {
    min = -0.50;
    max = 0;
  } else if (histoName.Contains("Pos")) {
    min = 0;
    max = 0.5;
  }
  return new TH1F(histoName, histoTitle, 250, min, max);
}

static bool CompareStripNo(TObject *obj1, TObject *obj2) {
  return ((SRSHit *)obj1)->GetStripNo() < ((SRSHit *)obj2)->GetStripNo();
}

void SRSPositionCorrection::ComputeClustersInDetectorPlane() {
  SRS_DEBUG("SRSPositionCorrection:ComputeClustersInDetectorPlane");

  auto mapping = SRSMapping::GetInstance();
  for (auto &listOfHitsVsPlane : fHitsInDetectorPlaneMap) {
    const auto &detPlane = listOfHitsVsPlane.first;
    const auto detector = mapping->GetDetectorFromPlane(detPlane),
               readoutBoard = mapping->GetReadoutBoardFromDetector(detector);

    auto &listOfHits = listOfHitsVsPlane.second;
    listOfHits.sort(CompareStripNo);
    if (const auto listSize = listOfHits.size(); listSize < 2)
      continue;

    int previousStrip = -2;
    int clusterNo = -1;
    std::map<int, SRSCluster *> clustersMap;
    for (auto *hit : listOfHits) {
      int currentStrip = hit->GetStripNo();
      //int apvIndexOnPlane = hit->GetAPVIndexOnPlane();
      if (readoutBoard != "PADPLANE") {
        if (currentStrip != (previousStrip + 1))
          clusterNo++;
      } else
        clusterNo++;
      if (!clustersMap[clusterNo]) {
        clustersMap[clusterNo] = new SRSCluster(2, 10, "totalCharges");
        clustersMap[clusterNo]->SetNbAPVsFromPlane(hit->GetNbAPVsFromPlane());
        clustersMap[clusterNo]->SetPlaneSize(hit->GetPlaneSize());
        clustersMap[clusterNo]->SetPlane(hit->GetPlane());
      }
      clustersMap[clusterNo]->AddHit(hit);
      previousStrip = currentStrip;
    }
    for (auto &cm : clustersMap) {
      auto *cluster = cm.second;
      if (!cluster->IsGoodCluster()) {
        delete cluster;
        continue;
      }
      cluster->SetClusterPositionCorrection(false);
      cluster->ComputeClusterPositionWithoutCorrection();
      fClustersInDetectorPlaneMap[detPlane].push_back(cluster);
    }
    listOfHits.clear();
    clustersMap.clear();
  }
}

void SRSPositionCorrection::FillClusterPositionCorrection() {
  SRS_DEBUG("SRSPositionCorrection:FillClusterPositionCorrection");
  auto mapping = SRSMapping::GetInstance();
  for (auto &dp : fClustersInDetectorPlaneMap) {
    const auto &detPlane = dp.first;
    TString detector = mapping->GetDetectorFromPlane(detPlane);

    int detID = mapping->GetDetectorIDFromDetector(detector);
    int planeID = (int)mapping->GetPlaneIDorEtaSector(detPlane);

    int histoID = (2 * detID) + planeID;
    TString histoName = "etaFunction" + detPlane;

    auto &listOfClusters = dp.second;
    if (listOfClusters.size() == 0) {
      listOfClusters.clear();
      continue;
    }

    for (auto *cluster : listOfClusters) {
      //float clusterADCcounts = cluster->GetClusterADCs();
      float clusterPosition = cluster->GetClusterPosition();
      float clusterCentralStrip = cluster->GetClusterCentralStrip();

      int nbOfAPVsFromPlane = cluster->GetNbAPVsFromPlane();
      float planeSize = cluster->GetPlaneSize();
      float pitch = planeSize / (SRSAPVEvent::kNumChannels * nbOfAPVsFromPlane);
      if (detPlane.Contains("EIC"))
        pitch = 2 * pitch;

      float eta = (clusterPosition - clusterCentralStrip) / pitch;

      fEtaFunctionHistos[histoID]->Fill(eta);

      const auto clusterSize = cluster->GetNbOfHits();
      if (eta > 0) {
        if (clusterSize == 2)
          fEta2PosHistos[histoID]->Fill(eta);
        else if (clusterSize == 3)
          fEta3PosHistos[histoID]->Fill(eta);
        else if (clusterSize == 4)
          fEta4PosHistos[histoID]->Fill(eta);
        else if (clusterSize == 5)
          fEta5PosHistos[histoID]->Fill(eta);
        else if (clusterSize >= 6)
          fEta6PlusPosHistos[histoID]->Fill(eta);
      } else {
        if (clusterSize == 2)
          fEta2NegHistos[histoID]->Fill(eta);
        else if (clusterSize == 3)
          fEta3NegHistos[histoID]->Fill(eta);
        else if (clusterSize == 4)
          fEta4NegHistos[histoID]->Fill(eta);
        else if (clusterSize == 5)
          fEta5NegHistos[histoID]->Fill(eta);
        else if (clusterSize >= 6)
          fEta6PlusNegHistos[histoID]->Fill(eta);
      }
    }
    listOfClusters.clear();
  }
  DeleteHitsInDetectorPlaneMap(fHitsInDetectorPlaneMap);
  DeleteClustersInDetectorPlaneMap(fClustersInDetectorPlaneMap);
}

void SRSPositionCorrection::LoadClusterPositionCorrection(const char *filename) {
  TFile f(filename);
  SRS_INFO("SRSPositionCorrection:LoadClusterPositionCorrection") << "from '" << filename << "'.";
  auto mapping = SRSMapping::GetInstance();
  for (const auto &det : mapping->GetDetectorFromIDMap()) {
    const auto &detID = det.first;
    const auto &detName = det.second;
    for (const auto &plane : mapping->GetDetectorPlaneListFromDetector(detName)) {
      const auto planeID = (int)mapping->GetPlaneIDorEtaSector(plane), histoID = (2 * detID) + planeID;
      auto *EtaFuncHisto = f.Get<TH1F>("etaFunction" + plane);
      for (size_t chNo = 0; chNo < 1000; chNo++)
        fEtaFunctionHistos[histoID]->Fill(chNo, EtaFuncHisto->GetBinContent(chNo + 1));
    }
    mapping->GetDetectorPlaneListFromDetector(detName).clear();
  }
  mapping->GetDetectorFromIDMap().clear();
  fIsEtaFunctionComputed = true;
}

void SRSPositionCorrection::SaveClusterPositionCorrectionHistos() {
  GetStyle();
  GetStyle();
  auto mapping = SRSMapping::GetInstance();
  for (const auto &det : mapping->GetDetectorFromIDMap()) {
    const auto &detID = det.first;
    for (const auto &plane : mapping->GetDetectorPlaneListFromDetector(det.second)) {
      const auto planeID = (int)mapping->GetPlaneIDorEtaSector(plane), histoID = (2 * detID) + planeID;

      PolynomialFit(fEtaFunctionHistos[histoID]);
      fEtaFunctionHistos[histoID]->Write();

      PolynomialFit(fEta2PosHistos[histoID]);
      fEta2PosHistos[histoID]->Write();

      PolynomialFit(fEta3PosHistos[histoID]);
      fEta3PosHistos[histoID]->Write();

      PolynomialFit(fEta4PosHistos[histoID]);
      fEta4PosHistos[histoID]->Write();

      PolynomialFit(fEta5PosHistos[histoID]);
      fEta5PosHistos[histoID]->Write();

      PolynomialFit(fEta6PlusPosHistos[histoID]);
      fEta6PlusPosHistos[histoID]->Write();

      PolynomialFit(fEta2NegHistos[histoID]);
      fEta2NegHistos[histoID]->Write();

      PolynomialFit(fEta3NegHistos[histoID]);
      fEta3NegHistos[histoID]->Write();

      PolynomialFit(fEta4NegHistos[histoID]);
      fEta4NegHistos[histoID]->Write();

      PolynomialFit(fEta5NegHistos[histoID]);
      fEta5NegHistos[histoID]->Write();

      PolynomialFit(fEta6PlusNegHistos[histoID]);
      fEta6PlusNegHistos[histoID]->Write();

      TCanvas c("c1", "c1", 10, 10, 1610, 810);
      c.cd();
      TString histoname = fEtaFunctionHistos[histoID]->GetName();
      TString picturename = fRunName + histoname + ".png";

      fEtaFunctionHistos[histoID]->Draw("");
      fEta2PosHistos[histoID]->Draw("");
      fEta3PosHistos[histoID]->Draw("");
      fEta4PosHistos[histoID]->Draw("");
      fEta5PosHistos[histoID]->Draw("");
      fEta6PlusPosHistos[histoID]->Draw("");

      fEtaFunctionHistos[histoID]->UseCurrentStyle();
      fEta2PosHistos[histoID]->UseCurrentStyle();
      fEta3PosHistos[histoID]->UseCurrentStyle();
      fEta4PosHistos[histoID]->UseCurrentStyle();
      fEta5PosHistos[histoID]->UseCurrentStyle();
      fEta6PlusPosHistos[histoID]->UseCurrentStyle();

      fEta2NegHistos[histoID]->Draw("");
      fEta3NegHistos[histoID]->Draw("");
      fEta4NegHistos[histoID]->Draw("");
      fEta5NegHistos[histoID]->Draw("");
      fEta6PlusNegHistos[histoID]->Draw("");

      fEta2NegHistos[histoID]->UseCurrentStyle();
      fEta3NegHistos[histoID]->UseCurrentStyle();
      fEta4NegHistos[histoID]->UseCurrentStyle();
      fEta5NegHistos[histoID]->UseCurrentStyle();
      fEta6PlusNegHistos[histoID]->UseCurrentStyle();

      c.SaveAs(picturename);
    }
    mapping->GetDetectorPlaneListFromDetector(det.second).clear();
  }
  mapping->GetDetectorFromIDMap().clear();
}

SRSPositionCorrection *SRSPositionCorrection::GetClusterPositionCorrectionRootFile(const char *filename) {
  auto f = std::make_unique<TFile>(filename, "read");
  SRS_INFO("SRSPositionCorrection:GetClusterPositionCorrectionRootFile")
      << "to load cluster position correction root file '" << filename << "'.";
  return f->Get<SRSPositionCorrection>("SRSPositionCorrection");
}

void SRSPositionCorrection::PolynomialFit(TH1F *h) {
  gROOT->Reset();
  gStyle->SetOptFit(1110);
  gStyle->SetStatFontSize(0.02);
  gStyle->SetHistFillColor(kBlue);
  double par[10];

  TString fitFuncName = h->GetName();
  fitFuncName += "_FIT";

  TF1 *poly9 = new TF1(fitFuncName, "pol9", -0.5, 0.5);
  poly9->GetParameters(&par[0]);
  poly9->SetLineColor(kRed);
  poly9->SetLineWidth(2);
  h->Fit(poly9, "WWR+");
}

void SRSPositionCorrection::GetStyle() {
  gROOT->Reset();
  gStyle->SetOptStat(0);
  gStyle->SetCanvasColor(0);
  gStyle->SetCanvasBorderMode(0);

  gStyle->SetLabelFont(62, "xyz");
  gStyle->SetLabelSize(0.03, "xyz");
  gStyle->SetLabelColor(1, "xyz");
  gStyle->SetTitleBorderSize(0);
  gStyle->SetTitleFillColor(0);
  gStyle->SetTitleSize(0.05, "xyz");
  gStyle->SetTitleOffset(1.5, "xy");
  gStyle->SetTitleOffset(1., "z");
  gStyle->SetPalette(1);

  const int NRGBs = 5;
  const int NCont = 32;
  double stops[NRGBs] = {0.00, 0.34, 0.61, 0.84, 1.00};
  double red[NRGBs] = {0.00, 0.00, 0.87, 1.00, 0.51};
  double green[NRGBs] = {0.00, 0.81, 1.00, 0.20, 0.00};
  double blue[NRGBs] = {0.51, 1.00, 0.12, 0.00, 0.00};
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, NCont);
  gStyle->SetNumberContours(NCont);
}

void SRSPositionCorrection::DeleteClustersInDetectorPlaneMap(
    std::map<TString, std::list<SRSCluster *> > &stringListMap) {
  for (auto &sl : stringListMap) {
    for (auto &clust : sl.second)
      delete clust;
    sl.second.clear();
  }
  stringListMap.clear();
}

void SRSPositionCorrection::DeleteHitsInDetectorPlaneMap(std::map<TString, std::list<SRSHit *> > &stringListMap) {
  for (auto &sl : stringListMap) {
    for (auto *hit : sl.second)
      delete hit;
    sl.second.clear();
  }
  stringListMap.clear();
}
