#include <TMath.h>

#include "srsreco/SRSEventBuilder.h"
#include "srsutils/Logging.h"

ClassImp(SRSEventBuilder);

SRSEventBuilder::SRSEventBuilder(int triggerNb,
                                 TString maxClusterSize,
                                 TString minClusterSize,
                                 TString zeroSupCut,
                                 TString runType,
                                 bool isClusterPosCorrection)
    : fTriggerCount(triggerNb),
      fZeroSupCut(zeroSupCut.Atoi()),
      fIsClusterPosCorrection(isClusterPosCorrection),
      fRunType(runType),
      fListOfAPVEvents(new TList),
      fListOfHits(new TList) {
  SRS_DEBUG("SRSEventBuilder") << "triggerNb = " << triggerNb << ".";
  if (fZeroSupCut != 0) {
    fMinClusterSize = minClusterSize.Atoi();
    fMaxClusterSize = maxClusterSize.Atoi();
  }
}

SRSEventBuilder::~SRSEventBuilder() {
  fTriggerList.clear();
  DeleteListOfHits();
  DeleteListOfAPVEvents();
  DeleteHitsInDetectorPlaneMap();
  DeleteClustersInDetectorPlaneMap();
}

template <typename M>
void ClearVectorMap(M &amap) {
  for (auto &elem : amap)
    elem.clear();
  amap.clear();
}

void SRSEventBuilder::DeleteHitsInDetectorPlaneMap() {
  for (auto &sl : fHitsInDetectorPlaneMap) {
    for (auto *hit : sl.second)
      delete hit;
    sl.second.clear();
  }
  fHitsInDetectorPlaneMap.clear();
}

void SRSEventBuilder::DeleteClustersInDetectorPlaneMap() {
  for (auto &sl : fClustersInDetectorPlaneMap) {
    for (auto *cluster : sl.second)
      delete cluster;
    sl.second.clear();
  }
  fClustersInDetectorPlaneMap.clear();
}

void SRSEventBuilder::DeleteListOfAPVEvents() {
  TIter nextAPVEvent(fListOfAPVEvents.get());
  while (auto *apvEvent = (SRSAPVEvent *)nextAPVEvent())
    delete apvEvent;
  fListOfAPVEvents->Clear();
  delete fListOfAPVEvents.release();
}

void SRSEventBuilder::DeleteListOfClusters(TList *listOfClusters) {
  listOfClusters->Clear();
  delete listOfClusters;
}

void SRSEventBuilder::DeleteListOfHits() {
  TIter nextHit(fListOfHits.get());
  while (auto *hit = (SRSHit *)nextHit())
    delete hit;
  fListOfHits->Clear();
  delete fListOfHits.release();
}

static bool CompareStripNo(TObject *obj1, TObject *obj2) {
  return ((SRSHit *)obj1)->GetStripNo() < ((SRSHit *)obj2)->GetStripNo();
}

/*static bool CompareHitADCs(TObject *obj1, TObject *obj2) {
  return ((SRSHit *)obj1)->GetHitADCs() > ((SRSHit *)obj2)->GetHitADCs();
}*/

static bool CompareClusterADCs(TObject *obj1, TObject *obj2) {
  return ((SRSCluster *)obj1)->GetClusterADCs() > ((SRSCluster *)obj2)->GetClusterADCs();
}

void SRSEventBuilder::ComputeClustersInDetectorPlane() {
  SRS_DEBUG("SRSEventBuilder:ComputeClustersInDetectorPlane");

  auto mapping = SRSMapping::GetInstance();
  for (auto &listOfHitsVsDet : fHitsInDetectorPlaneMap) {
    const auto &detPlane = listOfHitsVsDet.first;
    TString detector = mapping->GetDetectorFromPlane(detPlane);
    TString readoutBoard = mapping->GetReadoutBoardFromDetector(detector);
    auto &listOfHits = listOfHitsVsDet.second;
    listOfHits.sort(CompareStripNo);
    int listSize = listOfHits.size();

    if (listSize < fMinClusterSize) {
      fIsGoodEvent = false;
      continue;
    }

    int previousStrip = -2;
    int clusterNo = -1;

    std::map<int, SRSCluster *> clustersMap;
    for (auto *hit : listOfHits) {
      int currentStrip = hit->GetStripNo();
      //int apvIndexOnPlane = hit->GetAPVIndexOnPlane();

      if (readoutBoard != "PADPLANE") {
        if (currentStrip != (previousStrip + 1))
          clusterNo++;
      } else
        clusterNo++;

      if (!clustersMap[clusterNo]) {
        clustersMap[clusterNo] = new SRSCluster(fMinClusterSize, fMaxClusterSize, fIsClusterMaxOrTotalADCs);
        clustersMap[clusterNo]->SetNbAPVsFromPlane(hit->GetNbAPVsFromPlane());
        clustersMap[clusterNo]->SetPlaneSize(hit->GetPlaneSize());
        clustersMap[clusterNo]->SetPlane(hit->GetPlane());
      }
      clustersMap[clusterNo]->AddHit(hit);
      previousStrip = currentStrip;
    }

    for (const auto &cm : clustersMap) {
      auto *cluster = cm.second;
      if (!cluster->IsGoodCluster()) {
        delete cluster;
        continue;
      }
      if (fIsClusterPosCorrection) {
        cluster->SetClusterPositionCorrection(fIsClusterPosCorrection);
        cluster->ComputeClusterPositionWithCorrection(fClusterPositionCorrectionRootFile);
      } else {
        cluster->SetClusterPositionCorrection(false);
        cluster->ComputeClusterPositionWithoutCorrection();
      }
      fClustersInDetectorPlaneMap[detPlane].push_back(cluster);
    }
    fClustersInDetectorPlaneMap[detPlane].sort(CompareClusterADCs);

    listOfHits.clear();
    clustersMap.clear();
  }
}

bool SRSEventBuilder::IsAGoodEventInDetector(TString detector) {
  bool IsGoodEventInDetector = false;
  auto mapping = SRSMapping::GetInstance();
  TString readoutBoard = mapping->GetReadoutBoardFromDetector(detector);

  if (readoutBoard == "PADPLANE") {
    TString padPlane = (mapping->GetDetectorPlaneListFromDetector(detector)).front();
    std::list<SRSHit *> listOfHits = fHitsInDetectorPlaneMap[padPlane];
    int size = listOfHits.size();
    if (size > 0)
      IsGoodEventInDetector = true;
  }

  else if (readoutBoard == "CMSGEM") {
    TString plane = (mapping->GetDetectorPlaneListFromDetector(detector)).front();
    int clusterMultiplicity = fClustersInDetectorPlaneMap[plane.Data()].size();
    if ((clusterMultiplicity == 0) || (clusterMultiplicity > fMaxClusterMultiplicity)) {
      fClustersInDetectorPlaneMap[plane.Data()].clear();
      IsGoodEventInDetector = false;
    } else
      IsGoodEventInDetector = true;
  }

  else if (readoutBoard == "1DSTRIPS") {
    TString plane = (mapping->GetDetectorPlaneListFromDetector(detector)).front();
    int clusterMultiplicity = fClustersInDetectorPlaneMap[plane.Data()].size();
    if ((clusterMultiplicity == 0) || (clusterMultiplicity > fMaxClusterMultiplicity)) {
      fClustersInDetectorPlaneMap[plane.Data()].clear();
      IsGoodEventInDetector = false;
    } else
      IsGoodEventInDetector = true;
  }

  else {
    TString detPlaneX = (mapping->GetDetectorPlaneListFromDetector(detector)).front();
    TString detPlaneY = (mapping->GetDetectorPlaneListFromDetector(detector)).back();
    int clusterMultiplicityX = fClustersInDetectorPlaneMap[detPlaneX.Data()].size();
    int clusterMultiplicityY = fClustersInDetectorPlaneMap[detPlaneY.Data()].size();

    /**
    if ( (fTriggerList[detector] == "isTrigger") and ( (clusterMultiplicityX > 1) or (clusterMultiplicityY > 1)) ) {
      IsGoodEventInDetector = false ;
      fClustersInDetectorPlaneMap[detPlaneX.Data()].clear() ;
      fClustersInDetectorPlaneMap[detPlaneY.Data()].clear() ;
    }
    */

    if ((fTriggerList[detector] == "isTrigger") and
        ((clusterMultiplicityX > fMaxClusterMultiplicity) or (clusterMultiplicityY > fMaxClusterMultiplicity))) {
      IsGoodEventInDetector = false;
      fClustersInDetectorPlaneMap[detPlaneX.Data()].clear();
      fClustersInDetectorPlaneMap[detPlaneY.Data()].clear();
    }

    if ((clusterMultiplicityX == 0) or (clusterMultiplicityY == 0) or
        (clusterMultiplicityX > fMaxClusterMultiplicity) or (clusterMultiplicityY > fMaxClusterMultiplicity)) {
      IsGoodEventInDetector = false;
      fClustersInDetectorPlaneMap[detPlaneX.Data()].clear();
      fClustersInDetectorPlaneMap[detPlaneY.Data()].clear();
    } else {
      IsGoodEventInDetector = true;
    }
  }
  return IsGoodEventInDetector;
}

bool SRSEventBuilder::IsAGoodEvent() {
  fIsGoodEvent = false;

  const auto nbOfDetectorsToBeTriggered = fTriggerList.size();
  if (nbOfDetectorsToBeTriggered == 0)
    fIsGoodEvent = true;

  else {
    size_t nbOfTriggeredDetectors = 0;
    for (const auto &trigger : fTriggerList) {
      if (!IsAGoodEventInDetector(trigger.first))  // detector
        continue;
      else
        nbOfTriggeredDetectors++;
    }
    if (nbOfTriggeredDetectors == nbOfDetectorsToBeTriggered)
      fIsGoodEvent = true;
  }
  return fIsGoodEvent;
}

bool SRSEventBuilder::IsAGoodClusterEvent(TString detPlane) {
  fIsGoodClusterEvent = false;

  auto mapping = SRSMapping::GetInstance();
  TString detector = mapping->GetDetectorFromPlane(detPlane);
  TString readoutBoard = mapping->GetReadoutBoardFromDetector(detector);

  if (readoutBoard == "PADPLANE") {
    std::list<SRSHit *> listOfHits = fHitsInDetectorPlaneMap[detPlane];
    int size = listOfHits.size();
    if (size > 0)
      fIsGoodClusterEvent = true;
    listOfHits.clear();
  }

  else {
    int clusterMult = fClustersInDetectorPlaneMap[detPlane.Data()].size();
    if ((clusterMult == 0) || (clusterMult > fMaxClusterMultiplicity)) {
      fClustersInDetectorPlaneMap[detPlane.Data()].clear();
      fIsGoodClusterEvent = false;
    } else {
      fIsGoodClusterEvent = true;
    }
  }
  return fIsGoodClusterEvent;
}

/**
list <SRSCluster * >  SRSEventBuilder::CrossTalkCorrection( list <SRSCluster * >  listOfClusters) {

  int clusterMultiplicity = listOfClusters.size() ;

  if(clusterMultiplicity == 2) {
    SRSCluster * cluster1 = listOfClusters.front() ;
    SRSCluster * cluster2 = listOfClusters.back() ;
    float diffStripNb    = (fabs(cluster1->GetClusterCentralStrip() - cluster2->GetClusterCentralStrip())) ;
    float ratioADCs = cluster1->GetClusterADCs() /  cluster2->GetClusterADCs() ;

    float criteria32 = fabs(diffStripNb - 32) ;
    float criteria88 = fabs(diffStripNb - 88) ;

    if ((( criteria32 <= 1)  || (criteria88 <=1)) && (ratioADCs > 1) ) {
      cluster1->SetClusterADCs(cluster1->GetClusterADCs() +  cluster2->GetClusterADCs() ) ;
      listOfClusters.pop_back() ;
      clusterMultiplicity = listOfClusters.size() ;
   }
  }
  return listOfClusters ;
}
*/

std::map<int, std::vector<float> > SRSEventBuilder::GetDetectorCluster(TString detector) {
  auto mapping = SRSMapping::GetInstance();

  TString detPlaneX = (mapping->GetDetectorPlaneListFromDetector(detector)).front();
  TString detPlaneY = (mapping->GetDetectorPlaneListFromDetector(detector)).back();
  TString readoutBoard = mapping->GetReadoutBoardFromDetector(detector);

  int planeOrientationX = mapping->GetPlaneOrientation(detPlaneX);
  int planeOrientationY = mapping->GetPlaneOrientation(detPlaneY);

  std::map<int, std::vector<float> > detectorClusterMap;
  std::list<SRSCluster *> listOfClustersX = fClustersInDetectorPlaneMap[detPlaneX.Data()];
  std::list<SRSCluster *> listOfClustersY = fClustersInDetectorPlaneMap[detPlaneY.Data()];

  int clusterMultiplicityX = listOfClustersX.size();
  int clusterMultiplicityY = listOfClustersY.size();

  int clusterMult = clusterMultiplicityX;
  if (clusterMultiplicityY < clusterMult)
    clusterMult = clusterMultiplicityY;
  if (fMaxClusterMultiplicity < clusterMult)
    clusterMult = fMaxClusterMultiplicity;

  TList *clusterListX, *clusterListY;
  clusterListX = new TList;
  clusterListY = new TList;

  for (const auto &clusterX : listOfClustersX)
    clusterListX->Add(clusterX);
  for (const auto &clusterY : listOfClustersY)
    clusterListY->Add(clusterY);

  for (int k = 0; k < clusterMult; k++) {
    float clusterPos1 = ((SRSCluster *)clusterListX->At(k))->GetClusterPosition();
    float clusterPos2 = ((SRSCluster *)clusterListY->At(k))->GetClusterPosition();

    //float x_coord = ((SRSCluster *)clusterListX->At(k))->GetClusterPosition();
    //float y_coord = ((SRSCluster *)clusterListY->At(k))->GetClusterPosition();

    clusterPos1 = planeOrientationX * clusterPos1;
    clusterPos2 = planeOrientationY * clusterPos2;

    float xpos = clusterPos1;
    float ypos = clusterPos2;

    float adcCount1 = ((SRSCluster *)clusterListX->At(k))->GetClusterADCs();
    float adcCount2 = ((SRSCluster *)clusterListY->At(k))->GetClusterADCs();

    float timing1 = ((SRSCluster *)clusterListX->At(k))->GetClusterTimeBin();
    float timing2 = ((SRSCluster *)clusterListY->At(k))->GetClusterTimeBin();

    if (readoutBoard == "UV_ANGLE") {
      float trapezoidDetLength = mapping->GetUVangleReadoutMap(detector)[0];
      float trapezoidDetInnerRadius = mapping->GetUVangleReadoutMap(detector)[1];
      float trapezoidDetOuterRadius = mapping->GetUVangleReadoutMap(detector)[2];
      float uvAngleCosineDirection = (trapezoidDetOuterRadius - trapezoidDetInnerRadius) / (2 * trapezoidDetLength);

      SRS_DEBUG("SRSEventBuilder:GetDetectorCluster")
          << "trapezoidDetLength=" << trapezoidDetLength << ", trapezoidDetInnerRadius=" << trapezoidDetInnerRadius
          << ", trapezoidDetOuterRadius=" << trapezoidDetOuterRadius
          << ", uvAngleCosineDirection=" << uvAngleCosineDirection << ".";

      xpos = 0.5 * (trapezoidDetLength + ((clusterPos1 - clusterPos2) / uvAngleCosineDirection));
      ypos = 0.5 * (clusterPos1 + clusterPos2);
    }

    if (readoutBoard == "PADPLANE") {
      SRSHit *hit = ((SRSCluster *)clusterListX->At(k))->GetHit(0);
      std::vector<float> padPosition = hit->GetPadPosition();
      xpos = hit->GetPadPosition()[0];
      ypos = hit->GetPadPosition()[1];
      adcCount1 = hit->GetHitADCs();
      adcCount2 = hit->GetHitADCs();
    }

    if ((adcCount1 == 0) || (adcCount2 == 0)) {
      fClustersInDetectorPlaneMap[detPlaneX.Data()].clear();
      fClustersInDetectorPlaneMap[detPlaneY.Data()].clear();
      continue;
    }

    detectorClusterMap[k].clear();
    detectorClusterMap[k].push_back(xpos);
    detectorClusterMap[k].push_back(ypos);
    detectorClusterMap[k].push_back(adcCount1);
    detectorClusterMap[k].push_back(adcCount2);
    detectorClusterMap[k].push_back(timing1);
    detectorClusterMap[k].push_back(timing2);
    detectorClusterMap[k].push_back(clusterPos1);
    detectorClusterMap[k].push_back(clusterPos2);
  }

  DeleteListOfClusters(clusterListX);
  DeleteListOfClusters(clusterListY);
  listOfClustersX.clear();
  listOfClustersY.clear();

  return detectorClusterMap;
}

float SRSEventBuilder::GetDetectorPlaneNoise(TString planeName) {
  return TMath::Mean(fDetectorPlaneNoise[planeName].begin(), fDetectorPlaneNoise[planeName].end());
}
