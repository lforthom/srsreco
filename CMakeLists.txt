cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
project(srsreco LANGUAGES CXX)
add_compile_options(-Wall -Wextra -pedantic)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR} ${CMAKE_SOURCE_DIR}/cmake $ENV{ROOTSYS}/cmake)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
set(CMAKE_INSTALL_PREFIX "/usr")
set(CMAKE_CXX_FLAGS_DEBUG "-pg") # for gprof
set(CMAKE_CXX_FLAGS_RELEASE "-Wall -Wextra -O2")
include(GNUInstallDirs)
find_package(ROOT COMPONENTS Core REQUIRED)
find_package(SRS REQUIRED)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

#--- build the core library
file(GLOB sources src/*.cc)
file(GLOB headers srsreco/*.h)
add_library(srsreco SHARED ${sources} ${headers})
target_include_directories(srsreco PRIVATE ${ROOT_INCLUDE_DIRS} ${SRS_INCLUDE_DIR})
target_link_libraries(srsreco ${ROOT_LIBRARIES} ${SRS_LIBRARY})
install(TARGETS srsreco DESTINATION ${CMAKE_INSTALL_LIBDIR} COMPONENT lib)
install(DIRECTORY srsreco DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT devel FILES_MATCHING PATTERN "*.h")

#--- build the main slow_control executable
#add_executable(srsSlowControl ${CMAKE_SOURCE_DIR}/src/slow_control.cc)
#target_link_libraries(srsSlowControl srsreco)
#target_include_directories(srsSlowControl PRIVATE ${PROJECT_SOURCE_DIR}/include)
#install(TARGETS srsSlowControl DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT lib)

#--- add all tests
add_subdirectory(test)
