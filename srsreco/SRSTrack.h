/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSTrack                                                                    *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO, Mike Staib 01/05/2011                                 *
*******************************************************************************/

#ifndef srsreco_SRSTrack_h
#define srsreco_SRSTrack_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>
#include <TString.h>

#include <map>
#include <vector>

#include "srsreco/SRSCluster.h"
#include "srsreco/SRSConfiguration.h"
#include "srsreco/SRSEventBuilder.h"
#include "srsreco/SRSMapping.h"
#include "srsreco/SRSTrackFit.h"

#endif

class SRSTrack : public TObject {
public:
  explicit SRSTrack(const char* cfgname,
                    TString offsetDir,
                    TString zeroSupCut,
                    TString maxClustSize,
                    TString minClustSize,
                    TString maxClustMult,
                    TString amoreAgentId);
  ~SRSTrack();

  void DoTracking();
  void BuildTrack();
  void BuildRawDataSpacePoints(SRSEventBuilder* eventbuilder);

  bool IsAGoodEvent() { return fIsGoodEvent; }
  bool IsAGoodTrack(SRSEventBuilder* eventbuilder);
  bool IsTrigger(TString detName);
  bool IsTracker(TString detName);

  void SetDetectorConfig(TString detName,
                         TString triggerType,
                         TString trackerType,
                         float zOffset,
                         int xnbin,
                         float xmin,
                         float xmax,
                         int ynbin,
                         float ymin,
                         float ymax);

  void ClearSpacePoints(std::map<TString, std::vector<float> >& spacePointMap);
  void DeleteClustersInDetectorPlaneMap(std::map<TString, std::list<SRSCluster*> >& stringListMap);
  void LoadFTBFalignementParametersRootFile(TString offsetDir);
  void ReadCfg(const char* cfgname);

  int GetXNBinResiduals(TString detName) { return fXNBinResiduals[detName]; }
  float GetXRangeMaxResiduals(TString detName) { return fXRangeMaxResiduals[detName]; }
  float GetXRangeMinResiduals(TString detName) { return fXRangeMinResiduals[detName]; }

  int GetYNBinResiduals(TString detName) { return fYNBinResiduals[detName]; }
  float GetYRangeMaxResiduals(TString detName) { return fYRangeMaxResiduals[detName]; }
  float GetYRangeMinResiduals(TString detName) { return fYRangeMinResiduals[detName]; }

  int GetPHINBinResiduals(TString detName) { return fPHINBinResiduals[detName]; }
  float GetPHIRangeMaxResiduals(TString detName) { return fPHIRangeMaxResiduals[detName]; }
  float GetPHIRangeMinResiduals(TString detName) { return fPHIRangeMinResiduals[detName]; }

  int GetRNBinResiduals(TString detName) { return fRNBinResiduals[detName]; }
  float GetRRangeMaxResiduals(TString detName) { return fRRangeMaxResiduals[detName]; }
  float GetRRangeMinResiduals(TString detName) { return fRRangeMinResiduals[detName]; }

  TString GetRunFilePrefix() { return fRunFilePrefix; }
  TString GetRunFileValue() { return fRunFileValue; }

  TString GetNtupleName() { return fNtupleName; }
  TString GetNtupleTitle() { return fNtupleTitle; }

  float GetNtupleSizeX() { return fNtupleSizeX; }
  float GetNtupleSizeY() { return fNtupleSizeY; }
  float GetNtupleSizeZ() { return fNtupleSizeZ; }

  std::map<TString, float> GetFitParameters() { return fFitParameters; }

  std::map<TString, std::vector<float> > GetTrackSpacePoints() { return fTrackSpacePointMap; }
  std::map<TString, std::vector<float> > GetFittedSpacePoints() { return fFittedSpacePointMap; }
  std::map<TString, std::vector<float> > GetRawDataSpacePoints() { return fRawDataSpacePointMap; }
  std::map<TString, std::vector<float> > GetEICstripClusterRawDataY() { return fEICstripClusterRawDataYMap; }

  std::map<TString, TString> GetTriggerList() { return fTriggerList; }
  std::map<TString, TString> GetDetectorList() { return fDetectorList; }
  std::map<TString, TString> GetTrackerList() { return fTrackerList; }

  std::map<TString, int> GetXNBinResiduals() { return fXNBinResiduals; }
  std::map<TString, float> GetXRangeMinResiduals() { return fXRangeMinResiduals; }
  std::map<TString, float> GetXRangeMaxResiduals() { return fXRangeMaxResiduals; }

  std::map<TString, int> GetYNBinResiduals() { return fYNBinResiduals; }
  std::map<TString, float> GetYRangeMinResiduals() { return fYRangeMinResiduals; }
  std::map<TString, float> GetYRangeMaxResiduals() { return fYRangeMaxResiduals; }

  std::map<TString, int> GetRNBinResiduals() { return fRNBinResiduals; }
  std::map<TString, float> GetRRangeMinResiduals() { return fRRangeMinResiduals; }
  std::map<TString, float> GetRRangeMaxResiduals() { return fRRangeMaxResiduals; }

  std::map<TString, int> GetPHINBinResiduals() { return fPHINBinResiduals; }
  std::map<TString, float> GetPHIRangeMinResiduals() { return fPHIRangeMinResiduals; }
  std::map<TString, float> GetPHIRangeMaxResiduals() { return fPHIRangeMaxResiduals; }

  std::map<TString, float> GetCoarseDetXOffset() { return fCoarseDetXOffset; }
  std::map<TString, float> GetCoarseDetYOffset() { return fCoarseDetYOffset; }
  std::map<TString, float> GetFineDetXOffset() { return fFineDetXOffset; }
  std::map<TString, float> GetFineDetYOffset() { return fFineDetYOffset; }
  std::map<TString, float> GetDetXOffset() { return fDetXOffset; }
  std::map<TString, float> GetDetYOffset() { return fDetYOffset; }

  std::map<TString, float> GetDetZPosition() { return fDetZPosition; }
  std::map<TString, float> GetDetPlaneRotationCorrection() { return fDetPlaneRotationCorrection; }

  void PlaneRotationCorrection(float angle, std::vector<float>& u);

  float getAngleAmplitude(const std::vector<float> u, const std::vector<float> v);

  TString GetDetectorType(TString detName) { return fDetectorList[detName]; }
  TString RunIdStream();
  int RunId();

private:
  int fZeroSupCut, fMinClusterSize{0}, fMaxClusterSize{1'000'000}, fMaxClusterMultiplicity;
  float fNtupleSizeX{100}, fNtupleSizeY{100}, fNtupleSizeZ{4000};
  TString fNtupleTitle{"noNtuple"}, fNtupleName, fRunFilePrefix{"std"}, fRunFileValue{"0"}, fOffsetFilename,
      fAmoreAgentID;

  std::map<TString, TString> fTriggerList, fDetectorList, fTrackerList;
  std::map<TString, int> fXNBinResiduals, fYNBinResiduals, fRNBinResiduals, fPHINBinResiduals;

  std::map<TString, float> fDetXOffset, fDetYOffset, fCoarseDetXOffset, fCoarseDetYOffset, fFineDetXOffset,
      fFineDetYOffset, fDetZPosition, fDetPlaneRotationCorrection;

  std::map<TString, float> fXRangeMinResiduals, fXRangeMaxResiduals, fYRangeMinResiduals, fYRangeMaxResiduals;
  std::map<TString, float> fRRangeMinResiduals, fRRangeMaxResiduals, fPHIRangeMinResiduals, fPHIRangeMaxResiduals;

  std::map<TString, float> fFitParameters;

  std::map<TString, std::vector<float> > fTrackSpacePointMap, fFittedSpacePointMap, fRawDataSpacePointMap,
      fEICstripClusterRawDataYMap;

  float fAngleCutMinX{0.}, fAngleCutMinY{0.}, fAngleCutMaxX{360.}, fAngleCutMaxY{360.};
  bool fIsGoodEvent{false}, fIsGoodTrack{false};

  ClassDef(SRSTrack, 1)
};

#endif
