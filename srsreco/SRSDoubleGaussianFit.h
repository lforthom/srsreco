/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSDoubleGaussianFit                                                        *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 28/05/2014                                             *
*******************************************************************************/

#ifndef SRSDOUBLEGAUSSIANFIT_H
#define SRSDOUBLEGAUSSIANFIT_H

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TH1F.h>
#include <TObject.h>

#endif

class SRSDoubleGaussianFit : public TObject {
public:
  explicit SRSDoubleGaussianFit() = default;
  explicit SRSDoubleGaussianFit(TH1F* htemp, int N_iter, float N_sigma_range, bool ShowFit);
  ~SRSDoubleGaussianFit() = default;

  //  void DoubleGaussianFitLoop(TH1F* htemp, int N_iter, float N_sigma_range, bool ShowFit) ;

  float GetRChi2() { return fRChi2; }
  float GetMean() { return fMean; }
  float GetSigma() { return fMeanError; }
  float GetMeanError() { return fMeanError; }
  float GetSigmaError() { return fSigmaError; }

private:
  float fRChi2{-100.}, fMean{-100.}, fMeanError{-100.}, fSigma{-100.}, fSigmaError{-100.};

  ClassDef(SRSDoubleGaussianFit, 1)
};

#endif
