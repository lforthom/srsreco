/*******************************************************************************
 *  AMORE FOR SRS - SRS                                                         *
 *  SRSPedestal                                                                 *
 *  SRS Module Class                                                            *
 *  Author: Kondo GNANVO 18/08/2010                                             *
 *******************************************************************************/

#ifndef srsreco_SRSPedestal_h
#define srsreco_SRSPedestal_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TH1F.h>
#include <TH2F.h>
#include <TObject.h>
#include <TString.h>

#include "srsreco/SRSFECPedestalDecoder.h"
#include "srsreco/SRSRawPedestal.h"

#endif

class SRSPedestal : public TObject {
public:
  explicit SRSPedestal(int nbOfAPVs, int chMaskCut);
  explicit SRSPedestal() = default;
  ~SRSPedestal();

  void Init();
  void FillPedestalHistos(SRSFECPedestalDecoder* pedestalDecoder, SRSRawPedestal* rawped);

  void Clear(Option_t* = "") override;
  void Reset();
  void GetStyle();
  void ClearMaps();

  void ComputePedestalData();
  void ComputeMaskedChannels();

  void SavePedestalRunHistos();
  void LoadPedestalData(const char* filename);

  float GetNoise(int apvID, int channelId);
  float GetOffset(int apvID, int channelId);
  float GetMaskedChannelStatus(int apvID, int chNo);

  float GetOnlinePedestalRMS(int apvID, int channelId);
  float GetOnlinePedestalMean(int apvID, int channelId);

  std::vector<float> GetAPVNoises(int apvID);
  std::vector<float> GetAPVOffsets(int apvID);
  std::vector<float> GetAPVMaskedChannels(int apvID);

  TH1F* GetPedHisto(int apvID, int channelId);
  TH1F* BookHistos(int apvKey, TString dataType, TString dataNb);
  TH2F* Book2DHistos(int apvKey);

  TString GetHistoName(int apvKey, TString dataType, TString dataNb);

  bool IsPedestalComputed() { return fIsPedestalComputed; }
  bool IsMaskedChannelComputed() { return fIsMaskedChComputed; }

  void SetRunName(TString runname) { fRunName = runname; }
  void SetRunType(TString runtype) { fRunType = runtype; }

  static SRSPedestal* GetPedestalRootFile(const char* filename);

private:
  int fNbOfAPVs;
  int fEventNb{-1}, fChMaskCut;
  TString fRunName, fRunType;
  bool fIsPedestalComputed{false}, fIsMaskedChComputed{false}, fIsAPVHeaderComputed, fIsCommonModeComputed,
      fIsFirstEvent{true};

  std::vector<TH1F*> fRMSDist;
  std::vector<TH1F*> fNoises;
  std::vector<TH1F*> fOffsets;
  std::vector<TH1F*> fMaskedChannels;
  std::vector<TH1F*> fPedHistos;
  std::vector<TH2F*> fPed2DHistos;

  std::vector<float> fPedestalData;

  ClassDef(SRSPedestal, 6)
};

#endif
