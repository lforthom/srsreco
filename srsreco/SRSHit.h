/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSHit                                                                      *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSHit_h
#define srsreco_SRSHit_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>
#include <TString.h>

#include <vector>

#include "srsreco/SRSAPVSignalFit.h"

#endif

class SRSHit : public TObject {
public:
  explicit SRSHit() = default;
  ~SRSHit() = default;

  bool IsSortable() const override { return true; }

  void IsHitFlag(bool hitOrNoise) { fIsHit = hitOrNoise; }
  bool IsHit() { return fIsHit; }

  void Timing();

  //=== Sort hit according to the strip number
  int Compare(const TObject* obj) const override { return (fStripNo > ((SRSHit*)obj)->GetStripNo()) ? 1 : -1; }

  void ComputePosition();
  void AddTimeBinADCs(float charges) { fTimeBinADCs.push_back(charges); }

  void SetTimeBinADCs(std::vector<float> timebinCharges) {
    fTimeBinADCs.clear();
    fTimeBinADCs = timebinCharges;
  }

  void SetPadDetectorMap(std::vector<float> padDetectorMap) {
    fPadDetectorMap.clear();
    fPadDetectorMap = padDetectorMap;
    fPadDetectorMap.resize(5);
  }

  void ClearTimeBinADCs() { fTimeBinADCs.clear(); }

  std::vector<float> GetTimeBinADCs() { return fTimeBinADCs; }

  int GetAPVID() const { return fapvID; }
  void SetAPVID(int apvID) { fapvID = apvID; }
  void SetHitADCs(int sigmaLevel, float charges, TString isHitMaxOrTotalADCs);
  float GetHitADCs() const { return fHitADCs; }

  int GetSignalPeakBinNumber() {
    Timing();
    return fSignalPeakBinNumber;
  }

  const TString& GetHitMaxOrTotalADCs() const { return fIsHitMaxOrTotalADCs; }

  int GetAPVIndexOnPlane() const { return fapvIndexOnPlane; }
  void SetAPVIndexOnPlane(int index) { fapvIndexOnPlane = index; }

  int GetNbAPVsFromPlane() const { return fNbAPVsOnPlane; }
  void SetNbAPVsFromPlane(int nb) { fNbAPVsOnPlane = nb; }

  int GetAPVOrientation() const { return fAPVOrientation; }
  void SetAPVOrientation(int nb) { fAPVOrientation = nb; }

  float GetPlaneSize() const { return fPlaneSize; }
  void SetPlaneSize(float planesize) { fPlaneSize = planesize; }

  float GetTrapezoidDetInnerRadius() const { return fTrapezoidDetInnerRadius; }
  float GetTrapezoidDetOuterRadius() const { return fTrapezoidDetOuterRadius; }

  void SetTrapezoidDetRadius(float innerRadius, float outerRadius) {
    fTrapezoidDetInnerRadius = innerRadius;
    fTrapezoidDetOuterRadius = outerRadius;
  }

  const TString& GetPlane() const { return fPlane; }
  void SetPlane(TString plane) { fPlane = plane; }

  const TString& GetDetector() const { return fDetector; }
  void SetDetector(TString detector) { fDetector = detector; }

  const TString& GetReadoutBoard() const { return fReadoutBoard; }
  void SetReadoutBoard(TString readoutBoard) { fReadoutBoard = readoutBoard; }

  const TString& GetDetectorType() const { return fDetectorType; }
  void SetDetectorType(TString detectorType) { fDetectorType = detectorType; }

  void SetStripNo(int stripNo);
  int GetStripNo() const { return fStripNo; }
  int GetAbsoluteStripNo() const { return fAbsoluteStripNo; }

  void SetPadNo(int padno) { fPadNo = padno; }
  int GetPadNo() const { return fPadNo; }

  float GetStripPosition() {
    ComputePosition();
    return fStripPosition;
  }

  std::vector<float> GetPadPosition() {
    ComputePosition();
    return fPadPosition;
  }

private:
  bool fIsHit{true};
  int fapvID{0}, fStripNo{0}, fPadNo{0}, fAbsoluteStripNo{0}, fapvIndexOnPlane{0}, fNbAPVsOnPlane{6},
      fAPVOrientation{0}, fSignalPeakBinNumber{0};
  float fHitADCs{0.}, fStripPosition{0.}, fPlaneSize{512.}, fTrapezoidDetInnerRadius{220.},
      fTrapezoidDetOuterRadius{430.};
  TString fPlane{"GEM1X"}, fReadoutBoard{"CARTESIAN"}, fDetectorType{"CARTESIAN"}, fDetector{"GEM1"},
      fIsHitMaxOrTotalADCs{"signalPeak"};

  std::vector<float> fTimeBinADCs, fPadDetectorMap, fPadPosition;

  ClassDef(SRSHit, 1)
};

#endif
