/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSAlignment                                                                *
*  SRS Module Class                                                            *
*  Author: Mike Staib 12/05/2011                                               *
*******************************************************************************/

#ifndef srsreco_SRSAlignment_h
#define srsreco_SRSAlignment_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>

#include <map>
#include <vector>

#include "srsreco/SRSTrack.h"
#include "srsreco/SRSTrackFit.h"

#endif

class SRSAlignment : public TObject {
public:
  explicit SRSAlignment() = default;
  ~SRSAlignment();

  static constexpr float kSmallNumber = 1.e-8;

  float fromRadianToDegree(float angle_rad) { return (float)((180. * angle_rad) * M_1_PI); };
  float fromDegreeToRadian(float angle_deg) { return (float)((M_PI * angle_deg) / 180.); };

  void TrackCoordinates();
  void ClearTrackCoordinates();

  std::map<float, float> GetTrackFitData(const std::map<float, float>& rawData);
  float GetAngleFromTracks(std::vector<float> inX,
                           std::vector<float> inY,
                           std::vector<float> inZ,
                           std::vector<float> outX,
                           std::vector<float> outY,
                           std::vector<float> outZ);

private:
  //float fTrackOffset, fTrackDirection;
  SRSTrack* fSRSTrack{nullptr};
  std::vector<float> fTrackX, fTrackY, fTrackZ;

  ClassDef(SRSAlignment, 1)
};

#endif
