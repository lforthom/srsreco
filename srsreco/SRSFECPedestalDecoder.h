/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSFECPedestalDecoder                                                          *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSFECPedestalDecoder_h
#define srsreco_SRSFECPedestalDecoder_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TList.h>
#include <TObject.h>

#include <map>
#include <vector>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSMapping.h"

#endif

class SRSFECPedestalDecoder : public TObject {
public:
  explicit SRSFECPedestalDecoder(int nwords, unsigned int* buf);
  SRSFECPedestalDecoder() = default;
  ~SRSFECPedestalDecoder();

  void BuildAPVEvent(std::vector<unsigned int> data32bits, int fec_no, int fec_channel);

  int GetNWords() { return fNWords; }
  int GetEventNumber() { return fEventNb; }
  int GetNbOfFECEvents() { return ((int)fFECEvents->GetSize()); }
  int GetPacketSize() { return fPacketSize; }

  TList* GetFECEvents() { return fFECEvents.get(); }
  SRSAPVEvent* GetAPVEvent(int apvID);

  std::map<int, std::vector<int> > GetActiveFecChannelsMap() { return fActiveFecChannelsMap; }

private:
  unsigned int* fBuf{nullptr};
  int fNWords, fEventNb{-1}, fPacketSize{4000};
  bool fIsNewPacket{false};

  std::vector<int> fActiveFecChannels;
  std::map<int, std::vector<int> > fActiveFecChannelsMap;

  std::unique_ptr<TList> fFECEvents;

  ClassDef(SRSFECPedestalDecoder, 1)
};

#endif
