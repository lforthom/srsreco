/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSMapping                                                                  *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

// Mapping class is implemented as a singleton

#ifndef srsreco_SRSMapping_h
#define srsreco_SRSMapping_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>
#include <TString.h>

#include <list>
#include <map>
#include <sstream>
#include <vector>

#endif

class SRSMapping : public TObject {
public:
  ~SRSMapping() {
    Clear();
    ClearMapOfList(fDetectorListFromDetectorTypeMap);
    ClearMapOfList(fDetectorListFromReadoutBoardMap);
    ClearMapOfList(fDetectorPlaneListFromDetectorMap);
    ClearMapOfList(fFECIDListFromDetectorPlaneMap);
    ClearMapOfList(fAPVIDListFromFECIDMap);
    ClearMapOfList(fAPVIDListFromDetectorPlaneMap);
    ClearMapOfList(fAPVIDListFromDetectorMap);
    ClearMapOfList(fAPVToPadChannelMap);
    ClearMapOfList(fPadDetectorMap);
    ClearMapOfList(f1DStripsPlaneMap);
    ClearMapOfList(fCartesianPlaneMap);
    ClearMapOfList(fCMSGEMDetectorMap);
    ClearMapOfList(fUVangleReadoutMap);
  }

  template <typename M>
  void ClearMapOfList(M& amap);

  static std::shared_ptr<SRSMapping> GetInstance() {
    if (!instance)
      instance.reset(new SRSMapping);
    return instance;
  }

  void PrintMapping();
  void SaveMapping(const char* mappingCfgFilename);

  void LoadDefaultMapping(const char* mappingCfgFilename);
  void LoadAPVtoPadMapping(const char* mappingCfgFilename);

  void SetAPVMap(TString detPlane, int fecId, int adcCh, int apvNo, int apvOrient, int apvIndex, int apvHdrLevel);
  void SetAPVtoPadMapping(int fecId, int adcCh, int padId, int apvCh);

  void Set1DStripsReadoutMap(TString readoutBoard,
                             TString detType,
                             TString detName,
                             int detID,
                             TString plane,
                             float size,
                             int connectors,
                             int orient);
  void SetCartesianStripsReadoutMap(TString readoutBoard,
                                    TString detectorType,
                                    TString detector,
                                    int detID,
                                    TString planeX,
                                    float sizeX,
                                    int connectorsX,
                                    int orientX,
                                    TString planeY,
                                    float sizeY,
                                    int connectorsY,
                                    int orientY);
  void SetUVStripsReadoutMap(TString readoutBoard,
                             TString detType,
                             TString detector,
                             int detID,
                             float length,
                             float outerR,
                             float innerR,
                             TString planeTop,
                             int conectTop,
                             int orientTop,
                             TString planeBot,
                             int connectBot,
                             int orientBot);
  void SetPadsReadoutMap(TString readoutBoard,
                         TString detType,
                         TString detName,
                         int detID,
                         TString padPlane,
                         float sizeX,
                         float sizeY,
                         float nbOfPadX,
                         float nbOfPadY,
                         float nbOfConnectors);
  void SetCMSGEMReadoutMap(TString readoutBoard,
                           TString detectorType,
                           TString detector,
                           int detID,
                           TString EtaSector,
                           float etaSectorPos,
                           float etaSectorSize,
                           float nbOfSectorConnectors,
                           int apvOrientOnEtaSector);

  void Clear(Option_t* = "") override;

  std::map<int, int> GetAPVNoFromIDMap() { return fAPVNoFromIDMap; }
  std::map<int, int> GetAPVIDFromAPVNoMap() { return fAPVIDFromAPVNoMap; }
  std::map<int, int> GetAPVGainFromIDMap() { return fAPVGainFromIDMap; }
  std::map<int, int> GetAPVOrientationFromIDMap() { return fAPVOrientationFromIDMap; }
  std::map<int, int> GetAPVHeaderLevelFromIDMap() { return fAPVHeaderLevelFromIDMap; }
  std::map<int, int> GetAPVIndexOnPlaneFromIDMap() { return fAPVIndexOnPlaneFromIDMap; }

  std::map<int, TString> GetAPVFromIDMap() { return fAPVFromIDMap; }
  std::map<int, TString> GetDetectorFromIDMap() { return fDetectorFromIDMap; }

  std::map<TString, float> GetPlaneIDFromPlaneMap() { return fPlaneIDFromPlaneMap; }

  std::map<TString, std::vector<float> > GetPadDetectorMap() { return fPadDetectorMap; }
  std::map<TString, std::vector<float> > GetCartesianPlaneMap() { return fCartesianPlaneMap; }

  std::map<TString, std::list<int> > GetAPVIDListFromDetectorMap() { return fAPVIDListFromDetectorMap; }
  std::map<TString, std::list<int> > GetAPVIDListFromDetectorPlaneMap() { return fAPVIDListFromDetectorPlaneMap; }

  std::list<int> GetAPVIDListFromDetector(TString detName) {
    fAPVIDListFromDetectorMap[detName].unique();
    return fAPVIDListFromDetectorMap[detName];
  }

  std::list<int> GetAPVIDListFromDetectorPlane(TString planeName) {
    fAPVIDListFromDetectorPlaneMap[planeName].unique();
    return fAPVIDListFromDetectorPlaneMap[planeName];
  }

  std::list<int> GetFECIDListFromDetectorPlane(TString planeName) {
    fFECIDListFromDetectorPlaneMap[planeName].unique();
    return fFECIDListFromDetectorPlaneMap[planeName];
  }

  std::list<int> GetAPVIDListFromFECID(int fecId) {
    fAPVIDListFromFECIDMap[fecId].unique();
    return fAPVIDListFromFECIDMap[fecId];
  }

  std::list<TString> GetDetectorPlaneListFromDetector(TString detName) {
    fDetectorPlaneListFromDetectorMap[detName].unique();
    return fDetectorPlaneListFromDetectorMap[detName];
  }

  int GetFECIDFromAPVID(int apvID) { return (apvID >> 4) & 0xF; }
  int GetADCChannelFromAPVID(int apvID) { return apvID & 0xF; }

  int GetAPVNoFromID(int apvID) { return fAPVNoFromIDMap[apvID]; }
  int GetAPVIDFromAPVNo(int apvID) { return fAPVIDFromAPVNoMap[apvID]; }
  int GetAPVIndexOnPlane(int apvID) { return fAPVIndexOnPlaneFromIDMap[apvID]; }
  int GetAPVOrientation(int apvID) { return fAPVOrientationFromIDMap[apvID]; }
  int GetAPVHeaderLevelFromID(int apvID) { return fAPVHeaderLevelFromIDMap[apvID]; }
  int GetAPVIDFromName(TString apvName) { return fAPVIDFromNameMap[apvName]; }

  int GetDetectorIDFromDetector(TString detName) { return fDetectorIDFromDetectorMap[detName]; }
  TString GetDetectorFromID(int detID) { return fDetectorFromIDMap[detID]; }

  TString GetDetectorTypeFromDetector(TString detectorName) { return fDetectorTypeFromDetectorMap[detectorName]; }
  TString GetReadoutBoardFromDetector(TString detectorName) { return fReadoutBoardFromDetectorMap[detectorName]; }
  TString GetDetectorFromPlane(TString planeName) { return fDetectorFromPlaneMap[planeName]; }

  std::vector<float> GetPadDetectorMap(TString detector) { return fPadDetectorMap[detector]; }
  std::vector<float> Get1DStripsReadoutMap(TString plane) { return f1DStripsPlaneMap[plane]; }
  std::vector<float> GetCMSGEMReadoutMap(TString etaSector) { return fCMSGEMDetectorMap[etaSector]; }
  std::vector<float> GetCartesianReadoutMap(TString plane) { return fCartesianPlaneMap[plane]; }
  std::vector<float> GetUVangleReadoutMap(TString plane) { return fUVangleReadoutMap[plane]; }

  float GetPlaneIDorEtaSector(TString planeName) {
    TString readoutType = GetReadoutBoardFromDetector(GetDetectorFromPlane(planeName));
    float planeIDorEtaSector = (fCartesianPlaneMap[planeName])[0];
    if (readoutType == "1DSTRIPS")
      planeIDorEtaSector = (f1DStripsPlaneMap[planeName])[0];
    if (readoutType == "CMSGEM")
      planeIDorEtaSector = (fCMSGEMDetectorMap[planeName])[0];
    if (readoutType == "UV_ANGLE")
      planeIDorEtaSector = (fUVangleReadoutMap[planeName])[0];
    return planeIDorEtaSector;
  }

  float GetSizeOfPlane(TString planeName) {
    TString readoutType = GetReadoutBoardFromDetector(GetDetectorFromPlane(planeName));
    float planeSize = (fCartesianPlaneMap[planeName])[1];
    if (readoutType == "1DSTRIPS")
      planeSize = (f1DStripsPlaneMap[planeName])[1];
    if (readoutType == "CMSGEM")
      planeSize = (fCMSGEMDetectorMap[planeName])[1];
    return planeSize;
  }

  int GetNbOfAPVs(TString planeName) {
    TString readoutType = GetReadoutBoardFromDetector(GetDetectorFromPlane(planeName));
    TString detectorType = GetDetectorTypeFromDetector(GetDetectorFromPlane(planeName));

    int nbOfAPVs = (int)(fCartesianPlaneMap[planeName])[2];
    if (readoutType == "1DSTRIPS")
      nbOfAPVs = (int)(f1DStripsPlaneMap[planeName])[2];
    if (readoutType == "CMSGEM")
      nbOfAPVs = (int)(fCMSGEMDetectorMap[planeName])[2];
    if ((readoutType == "UV_ANGLE") && (detectorType == "EICPROTO1"))
      nbOfAPVs = (int)(((fUVangleReadoutMap[planeName])[2]) / 2);
    ;
    return nbOfAPVs;
  }

  int GetPlaneOrientation(TString planeName) {
    TString readoutType = GetReadoutBoardFromDetector(GetDetectorFromPlane(planeName));
    int orient = (int)(fCartesianPlaneMap[planeName])[3];
    if (readoutType == "1DSTRIPS")
      orient = (int)(f1DStripsPlaneMap[planeName])[3];
    if (readoutType == "CMSGEM")
      orient = (int)(fCMSGEMDetectorMap[planeName])[3];
    if (readoutType == "UV_ANGLE")
      orient = (int)(fUVangleReadoutMap[planeName])[2];
    return orient;
  }

  TString GetDetectorPlaneFromAPVID(int apvID) { return fDetectorPlaneFromAPVIDMap[apvID]; }

  TString GetAPVFromID(int apvID) { return fAPVFromIDMap[apvID]; }
  TString GetAPV(TString detPlane, int fecId, int adcChannel, int apvNo, int apvIndex, int apvID);

  std::vector<int> GetPadChannelsMapping(int apvID) { return fAPVToPadChannelMap[apvID]; }
  std::map<int, std::vector<int> > GetPadChannelsMapping() { return fAPVToPadChannelMap; }

  int GetNbOfAPVs() { return fAPVNoFromIDMap.size(); }
  int GetNbOfFECs() { return fAPVIDListFromFECIDMap.size(); }
  int GetNbOfDetectorPlane() { return fDetectorFromPlaneMap.size(); }
  int GetNbOfDetectors() { return fReadoutBoardFromDetectorMap.size(); }

  Bool_t IsAPVIDMapped(int apvID) {
    std::map<int, TString>::iterator itr;
    itr = fAPVFromIDMap.find(apvID);
    if (itr != fAPVFromIDMap.end())
      return kTRUE;
    else
      return kFALSE;
  }

private:
  explicit SRSMapping() = default;

  static std::shared_ptr<SRSMapping> instance;
  int fNbOfAPVs{0};

  std::map<int, int> fAPVHeaderLevelFromIDMap;
  std::map<int, int> fAPVNoFromIDMap, fAPVIDFromAPVNoMap, fAPVIndexOnPlaneFromIDMap, fAPVOrientationFromIDMap;
  std::map<TString, int> fNbOfAPVsFromDetectorMap;

  std::map<int, int> fAPVGainFromIDMap;
  std::map<TString, int> fAPVIDFromNameMap;
  std::map<int, TString> fAPVFromIDMap;

  std::map<TString, float> fPlaneIDFromPlaneMap;
  std::map<int, TString> fDetectorPlaneFromAPVIDMap;

  std::map<int, TString> fReadoutBoardFromIDMap;

  std::map<TString, TString> fDetectorTypeFromDetectorMap;
  std::map<TString, TString> fReadoutBoardFromDetectorMap;
  std::map<TString, int> fDetectorIDFromDetectorMap;
  std::map<int, TString> fDetectorFromIDMap;
  std::map<int, TString> fDetectorFromAPVIDMap;
  std::map<TString, TString> fDetectorFromPlaneMap;

  std::map<int, std::list<int> > fAPVIDListFromFECIDMap;

  std::map<TString, std::list<int> > fFECIDListFromDetectorPlaneMap;
  std::map<TString, std::list<int> > fAPVIDListFromDetectorMap;
  std::map<TString, std::list<int> > fAPVIDListFromDetectorPlaneMap;

  std::map<TString, std::list<TString> > fDetectorListFromDetectorTypeMap;
  std::map<TString, std::list<TString> > fDetectorListFromReadoutBoardMap;
  std::map<TString, std::list<TString> > fDetectorPlaneListFromDetectorMap;

  std::map<int, std::vector<int> > fAPVToPadChannelMap;

  std::map<TString, std::vector<float> > fPadDetectorMap;
  std::map<TString, std::vector<float> > fUVangleReadoutMap;
  std::map<TString, std::vector<float> > f1DStripsPlaneMap;
  std::map<TString, std::vector<float> > fCartesianPlaneMap;
  std::map<TString, std::vector<float> > fCMSGEMDetectorMap;

  ClassDef(SRSMapping, 1)
};

#endif
