/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSEventBuilder                                                             *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSEventBuilder_h
#define srsreco_SRSEventBuilder_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TList.h>
#include <TObject.h>

#include <list>
#include <map>
#include <vector>

#include "srsreco/SRSCluster.h"
#include "srsreco/SRSMapping.h"
#include "srsreco/SRSPedestal.h"

#endif

class SRSEventBuilder : public TObject {
public:
  explicit SRSEventBuilder(int triggerNb,
                           TString maxClusterSize,
                           TString minClusterSize,
                           TString nbofsigma,
                           TString runType,
                           bool isCluserPosCorrection);

  ~SRSEventBuilder();

  template <typename M>
  void ClearVectorMap(M& amap);

  void AddHitInDetectorPlane(SRSHit* hit) { fHitsInDetectorPlaneMap[hit->GetPlane()].push_back(hit); }

  void AddHit(SRSHit* hit) { fListOfHits->Add(hit); }

  void AddMeanDetectorPlaneNoise(TString detPlaneName, float meanAPVnoise) {
    fDetectorPlaneNoise[detPlaneName].push_back(meanAPVnoise);
  }

  void ComputeClustersInDetectorPlane();
  void AddAPVEvent(SRSAPVEvent* apvEvent) { fListOfAPVEvents->AddAt(apvEvent, apvEvent->GetAPVKey()); }
  TString GetRunType() { return fRunType; }

  TList* GetListOfHits() { return fListOfHits.get(); }

  std::map<TString, std::list<SRSHit*> > GetHitsInDetectorPlane() { return fHitsInDetectorPlaneMap; }
  std::map<TString, std::list<SRSCluster*> > GetClustersInDetectorPlane() { return fClustersInDetectorPlaneMap; }

  void SetTriggerList(std::map<TString, TString> triggerList) { fTriggerList = triggerList; }
  int GetTriggerCount() { return fTriggerCount; }

  bool IsAGoodEvent();
  bool IsAGoodClusterEvent(TString detPlaneName);
  bool IsAGoodEventInDetector(TString detName);

  bool IsCluserPosCorrection() { return fIsClusterPosCorrection; }

  //  list <SRSCluster * > CrossTalkCorrection( std::list <SRSCluster * >  listOfClusters) ;

  SRSAPVEvent* GetAPVEventFromAPVKey(int apvKey) { return (SRSAPVEvent*)(fListOfAPVEvents->At(apvKey)); }

  void DeleteListOfAPVEvents();
  void DeleteListOfHits();
  void DeleteHitsInDetectorPlaneMap();
  void DeleteClustersInDetectorPlaneMap();
  void DeleteListOfClusters(TList* listOfClusters);
  float GetDetectorPlaneNoise(TString planeName);

  std::map<int, std::vector<float> > GetDetectorCluster(TString detector);

  void SetClusterPositionCorrection(bool isClusterPosCorrection) { fIsClusterPosCorrection = isClusterPosCorrection; }
  void SetClusterPositionCorrectionRootFile(const char* filename) { fClusterPositionCorrectionRootFile = filename; }

  void SetHitMaxOrTotalADCs(TString isHitMaxOrTotalADCs) { fIsHitMaxOrTotalADCs = isHitMaxOrTotalADCs; }
  TString GetHitMaxOrTotalADCs() { return fIsHitMaxOrTotalADCs; }

  void SetMaxClusterMultiplicity(TString maxClusterMult) { fMaxClusterMultiplicity = maxClusterMult.Atoi(); }
  TString GetMaxClusterMultiplicity() { return fMaxClusterMultiplicity; }

  void SetClusterMaxOrTotalADCs(TString isClusterMaxOrTotalADCs) { fIsClusterMaxOrTotalADCs = isClusterMaxOrTotalADCs; }
  TString GetClusterMaxOrTotalADCs() { return fIsClusterMaxOrTotalADCs; }

private:
  int fMinClusterSize{1}, fMaxClusterSize{100'000};
  TString fIsClusterMaxOrTotalADCs, fIsHitMaxOrTotalADCs{"signalPeak"};
  bool fIsGoodEvent{false}, fIsGoodClusterEvent{false};
  //float fTrapezoidDetLength{1000}, fTrapezoidDetInnerRadius{220}, fTrapezoidDetOuterRadius{430};
  /*float fEICstereoAngleDegree{6.067 * M_PI / 180}, fUVangleCosineDirection{std::tan(fTrapezoidDetUVangleDegree)},
      fUVangleCosineDirection{(430 - 220) / (2 * fTrapezoidDetLength)},
      fUVangleCosineDirection{(fTrapezoidDetOuterRadius - fTrapezoidDetInnerRadius) / (2 * fTrapezoidDetLength)},
      fUVangle{std::atan(fUVangleCosineDirection)};*/

  int fTriggerCount, fMaxClusterMultiplicity, fZeroSupCut;
  bool fIsClusterPosCorrection;
  TString fRunType;
  std::unique_ptr<TList> fListOfAPVEvents, fListOfHits;

  std::map<TString, std::vector<float> > fDetectorPlaneNoise;
  std::map<TString, TString> fTriggerList;
  std::map<TString, std::list<SRSHit*> > fHitsInDetectorPlaneMap;
  std::map<TString, std::list<SRSCluster*> > fClustersInDetectorPlaneMap;

  const char* fClusterPositionCorrectionRootFile{""};

  ClassDef(SRSEventBuilder, 1)
};

#endif
