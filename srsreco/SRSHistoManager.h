/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSHistoManager                                                             *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSHistoManager_h
#define srsreco_SRSHistoManager_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TH1.h>
#include <TH2.h>
#include <TNtuple.h>
#include <TObject.h>
#include <TString.h>

#include <map>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSAPVSignalFit.h"
#include "srsreco/SRSAlignment.h"
#include "srsreco/SRSDoubleGaussianFit.h"
#include "srsreco/SRSEventBuilder.h"
#include "srsreco/SRSFECEventDecoder.h"
#include "srsreco/SRSHit.h"
#include "srsreco/SRSMapping.h"
#include "srsreco/SRSPedestal.h"
#include "srsreco/SRSRawPedestal.h"
#include "srsreco/SRSTrack.h"
#include "srsreco/SRSTrackFit.h"

#endif

class eventHeaderStruct;

class SRSHistoManager : public TObject {
public:
  explicit SRSHistoManager(
      const char* histoCfgname, int zeroSupCut, int minClustSize, int maxClustSize, int maxClustMult);
  //  SRSHistoManager(const char * histoCfgname, TString runtype, TString runname, int zeroSupCut, int minClustSize, int maxClustSize, int maxClustMult);
  SRSHistoManager() = default;
  ~SRSHistoManager();

  void BookRawDataHisto();
  void BookAPVGainHisto();
  void BookHitTimingHisto();
  void BookAPVLatencyHisto();

  void BookResidualsHisto(SRSTrack* track);
  void BookEvent3DDisplayNtuple(SRSTrack* track);
  void BookClusterPositionCorrectionHistos();

  void BookCMSHisto(TString histoName, TString title, TString type);
  void Book1DHisto(TString name, TString title, TString type, TString sigName);
  void Book1DHisto(TString name, TString title, TString type, TString sigName1, TString sigName2);
  void Book2DHisto(TString name, TString title, TString type, TString sigName);
  void Book2DHisto(TString name, TString title, TString type, TString sigName1, TString sigName2);

  void FillRawDataHistos(SRSEventBuilder* eventbuilder);
  void FillAPV25GainHistos(SRSEventBuilder* eventbuilder);
  void FillClusterPositionCorrectionHistos(SRSEventBuilder* eventbuilder);

  void TrackFit(SRSTrack* track);
  //  void ComputeOffset(SRSTrack * track) ;
  void Event3DDisplay(SRSTrack* track);
  void FillResidualHistos(SRSTrack* track);
  std::vector<float> CartesianToCylindricalCoordinates(float offsetx,
                                                       float offsety,
                                                       std::vector<float> cartesianCoordinates);
  float PlaneRotationAngle(std::vector<float> referencePoint, std::vector<float> rotationPoint);
  float XYOffsets(std::vector<float> referencePoint, std::vector<float> prodedDetector, TString xORy);

  void PhysicsRun(SRSTrack* track, SRSEventBuilder* eventbuilder, const eventHeaderStruct* eventHeader);

  void PedestalRun(SRSPedestal* ped);
  void RawPedestalRun(SRSRawPedestal* ped);

  void GaussFit(TH1F* h);
  void LandauFit(TH1F* h);
  void DoubleGaussFit(TH1F* h, int N_iter, float N_sigma_range, Bool_t ShowFit);

  void DumpList();
  void ResetHistos();
  void RefreshHistos();
  void DeleteHistos();
  void DeleteROOTObjs();

  void ClearMaps();
  void ClearMapOfHistos(std::map<TString, TH1*> mapOfHistos);
  void ClearMapOfVectors(std::map<TString, std::vector<TString> > mapOfVectors);

  void DeleteListOfClusters(TList* listOfClusters);

  void GetStyle(Bool_t setStats);
  void ReadHistoCfgFile(const char* histoCfgname);
  void SetHistoTitle(TString histoName, int goodEvents, int trigger);
  void SetHistoTitle(TString histoName);

  void SetRunParameters(TString amoreAgentId, TString runtype, TString runname, TString filePrefix, TString fileValue);
  void SetBinning(TString type, TString detPlane1, TString detPlane2);

  void Reset1DHistBinning(TString type, int nbin, float min, float max);
  void Reset2DHistBinning(TString type, int nbin, float min, float max, int nbin2, float min2, float max2);

  int GetPedestalOffset(SRSAPVEvent* apvEv, SRSPedestal* ped);

  void SaveFTBF_Residuals(SRSTrack* track) {
    SaveFTBF_XYOffsetsROOT(track);
    SaveFTBF_PlaneRotationROOT(track);
    SaveFTBF_ResidualsROOT(track);
    SaveFTBF_ResidualsTXT(track);
  }

  void SaveFTBF_AnalysisTXT(SRSTrack* track);
  void SaveFTBF_ResidualsTXT(SRSTrack* track);
  void SaveFTBF_ResidualsROOT(SRSTrack* track);
  void SaveFTBF_PlaneRotationROOT(SRSTrack* track);
  void SaveFTBF_XYOffsetsROOT(SRSTrack* track);

  void SaveAllHistos();
  void SaveAPV25GainHistos();
  void SavePosCorrectionHistos();

  void SaveRunNTuple();
  void SaveRunHistos(TObject* obj, Bool_t setStats);
  void SaveNTuplePlots(TObject* obj, Bool_t setStats);

  TString GetVarType(TString name) { return fVarType[name]; }

  std::map<TString, TObject*> GetAllObjectsToBePublished() { return fObjToDraw; }
  std::map<TString, TH1*> GetResidualHistoToBePublished() { return fResidualHistos; }
  std::map<TString, TH1*> GetPosCorrectionHistosToBePublished() { return fPosCorrectionHistos; }

  TNtuple* GetNtupleToBePublished() { return fNtuple; }

  void DeleteHitsInDetectorPlaneMap(std::map<TString, list<SRSHit*> >& stringListMap);
  void DeleteClustersInDetectorPlaneMap(std::map<TString, list<SRSCluster*> >& stringListMap);
  TString RunIdStream();

private:
  std::shared_ptr<SRSMapping>& fMapping;

  TString fRunType{"RAWDATA"}, fRunName{"toto"}, fRunFilePrefix{"std_"}, fRunFileValue{"0"}, fAmoreAgentID{"0"};
  int fZeroSupCut;

  std::vector<TH1*> fHist1dToReset;
  std::vector<TH2*> fHist2dToReset;

  TNtuple* fNtuple;
  std::map<TString, TH1*> fResidualHistos, fPosCorrectionHistos;
  std::map<TString, TObject*> fObjToDraw;

  std::map<TString, int> fNbOfEvents;
  std::map<TString, TString> fObjType, fVarType, fObjTitle;

  std::map<TString, TString> fDetector, fDetectorPlane1, fDetectorPlane2, fFitFunction, fFitParam1Str, fFitParam2Str,
      fDividedHistos;

  std::map<TString, std::vector<TString> > fFittedHistos;
  std::map<TString, std::vector<TString> > fHistoTypeToDetectorMap;

  std::map<TString, int> fAPVKeyStr;
  std::map<TString, int> fAPVIDStr;
  std::map<int, TString> fNameAPVIDMap;

  int fNBin{256};
  float fRangeMin{-0.5}, fRangeMax{255.5};

  int fNBin2{14};
  float fRangeMin2{-0.5}, fRangeMax2{13.5};

  int fNBinPlane{256};
  float fRangeMinPlane{-51.2}, fRangeMaxPlane{51.2};

  int fNBinTime{30};
  float fRangeMinTime{-0.5}, fRangeMaxTime{29.5};

  int fNBinSpectrum{100};
  float fRangeMinSpectrum{-0.5}, fRangeMaxSpectrum{3999.5};

  int fNBinMap{256};
  float fRangeMinMap{-0.5}, fRangeMaxMap{255.5};

  int fNBinMap2{256};
  float fRangeMinMap2{-0.5}, fRangeMaxMap2{255.5};

  int f1DHistNBin{21};
  float f1DHistMinRange{0.}, f1DHistMaxRange{20.};

  float fNtupleSizeX{100.}, fNtupleSizeY{100.}, fNtupleSizeZ{400.};
  int fHistosRatioCut{1}, fClustSizeRange, fMinClustSize, fMaxClustSize, fMaxClustMult;
  bool fIsFirstEvent{true};

  unsigned long long fTriggerPattern;
  unsigned int fEventType;

  int fEventNumber{0};
  float fRChi2{-100.}, fMean{-100.}, fMeanError{-100.}, fSigma{-100.}, fSigmaError{-100.};

  std::vector<TString> fDetPlaneNameList;

  ClassDef(SRSHistoManager, 1)
};

#endif
