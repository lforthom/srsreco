/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSPositionCorrection                                                              *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 06/06/2014                                             *
*******************************************************************************/

#ifndef srsreco_SRSPositionCorrection_h
#define srsreco_SRSPositionCorrection_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>
#include <TString.h>

#include <list>
#include <map>
#include <vector>

#include "SRSCluster.h"
#include "SRSMapping.h"

#endif

class SRSPositionCorrection : public TObject {
public:
  explicit SRSPositionCorrection(TString runname, TString runtype);
  explicit SRSPositionCorrection() = default;
  ~SRSPositionCorrection();

  TH1F* BookHisto(TString histoName, TString histoTitle);
  void BookClusterPositionCorrection();

  void Clear(Option_t* = "") override;
  void GetStyle();

  void AddHitInDetectorPlane(SRSHit* hit) { fHitsInDetectorPlaneMap[hit->GetPlane()].push_back(hit); }
  void ComputeClustersInDetectorPlane();

  void FillClusterPositionCorrection();
  void SaveClusterPositionCorrectionHistos();

  void LoadClusterPositionCorrection(const char* filename);

  void DeleteHitsInDetectorPlaneMap(std::map<TString, std::list<SRSHit*> >& stringListMap);
  void DeleteClustersInDetectorPlaneMap(std::map<TString, std::list<SRSCluster*> >& stringListMap);

  bool IsEtaFunctionComputed() { return fIsEtaFunctionComputed; }
  static SRSPositionCorrection* GetClusterPositionCorrectionRootFile(const char* filename);

  void SetRunName(TString runname) { fRunName = runname; }
  void SetRunType(TString runtype) { fRunType = runtype; }

  //  void PolynomialFit( TH1F* h, float start, float end) ;
  void PolynomialFit(TH1F* h);

private:
  TString fRunName, fRunType;

  bool fIsEtaFunctionComputed{false};

  TH1F** fEtaFunctionHistos;

  TH1F** fEta2PosHistos;
  TH1F** fEta3PosHistos;
  TH1F** fEta4PosHistos;
  TH1F** fEta5PosHistos;
  TH1F** fEta6PlusPosHistos;

  TH1F** fEta2NegHistos;
  TH1F** fEta3NegHistos;
  TH1F** fEta4NegHistos;
  TH1F** fEta5NegHistos;
  TH1F** fEta6PlusNegHistos;

  int fTriggerCount, fMinClusterSize, fMaxClusterSize, fMaxClusterMultiplicity, fZeroSupCut;

  std::map<TString, std::list<SRSHit*> > fHitsInDetectorPlaneMap;
  std::map<TString, std::list<SRSCluster*> > fClustersInDetectorPlaneMap;

  ClassDef(SRSPositionCorrection, 7)
};

#endif
