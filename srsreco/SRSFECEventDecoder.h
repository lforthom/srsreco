/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSFECEventDecoder                                                          *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSFECEventDecoder_h
#define srsreco_SRSFECEventDecoder_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>

#include <map>
#include <vector>

#include "srsreco/SRSAPVEvent.h"
#include "srsreco/SRSEventBuilder.h"
#include "srsreco/SRSMapping.h"
#include "srsreco/SRSPedestal.h"
#include "srsreco/SRSPositionCorrection.h"

#endif

class SRSFECEventDecoder : public TObject {
public:
  explicit SRSFECEventDecoder(int nwords, unsigned int* buf, SRSEventBuilder* eventBuilder);
  explicit SRSFECEventDecoder(
      int nwords, unsigned int* buf, SRSPedestal* ped, SRSEventBuilder* eventBuilder, int zeroSupCut);
  explicit SRSFECEventDecoder(
      int nwords, unsigned int* buf, SRSPedestal* ped, SRSPositionCorrection* clusterPosCorr, int zeroSupCut);
  SRSFECEventDecoder() = default;
  ~SRSFECEventDecoder();

  void BuildHits(std::vector<unsigned int> data32bits,
                 int fec_no,
                 int fec_channel,
                 SRSPedestal* ped,
                 SRSEventBuilder* eventBuilder,
                 int zeroSupCut);
  void BuildRawAPVEvents(std::vector<unsigned int> data32bits,
                         int fec_no,
                         int fec_channel,
                         SRSEventBuilder* eventBuilder);
  void BuildHitForPositionCorrection(std::vector<unsigned int> data32bits,
                                     int fec_id,
                                     int adc_channel,
                                     SRSPedestal* ped,
                                     SRSPositionCorrection* clusterPosCorr,
                                     int zeroSupCut);

  int GetNWords() { return fNWords; }
  int GetEventNumber() { return fEventNb; }
  int GetPacketSize() { return fPacketSize; }

  std::map<int, std::vector<int> > GetActiveFecChannelsMap() { return fActiveFecChannelsMap; }

private:
  unsigned int* fBuf{nullptr};
  size_t fNWords{0};
  int fEventNb{-1};
  size_t fPacketSize{4'000};
  bool fIsNewPacket{false};

  std::vector<int> fActiveFecChannels;
  std::map<int, std::vector<int> > fActiveFecChannelsMap;

  ClassDef(SRSFECEventDecoder, 1)
};

#endif
