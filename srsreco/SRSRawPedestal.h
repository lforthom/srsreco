/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSRawPedestal                                                              *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSRawPedestal_h
#define srsreco_SRSRawPedestal_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TH1F.h>
#include <TH2F.h>
#include <TObject.h>
#include <TString.h>

#include "srsreco/SRSFECPedestalDecoder.h"
#include "srsreco/SRSMapping.h"
//#include "srsreco/SRSEventBuilder.h"

#endif

/// \author Kondo Gnanvo
class SRSRawPedestal : public TObject {
public:
  explicit SRSRawPedestal(int nbOfAPVs);
  explicit SRSRawPedestal() = default;
  ~SRSRawPedestal();

  void Init(int nbOfAPVs);
  void FillRawPedestalHistos(SRSFECPedestalDecoder* pedestalDecoder);

  void Clear(Option_t* = "");
  void Reset();
  void GetStyle();
  void ClearMaps();

  void ComputeRawPedestalData();
  void SaveRawPedestalRunHistos();
  void LoadRawPedestalData(const char* filename);

  float GetNoise(int apvID, int channelId);
  float GetOffset(int apvID, int channelId);

  float GetOnlinePedestalRMS(int apvID, int channelId);
  float GetOnlinePedestalMean(int apvID, int channelId);

  std::vector<float> GetAPVNoises(int apvID);
  std::vector<float> GetAPVOffsets(int apvID);

  std::vector<float> GetRawPedestalData() { return fRawPedestalData; }

  TH1F* GetPedHisto(int apvID, int channelId);
  TH1F* BookHistos(int apvKey, TString dataType, TString dataNb);
  TH2F* Book2DHistos(int apvKey);

  bool IsRawPedestalComputed() { return fIsRawPedestalComputed; }
  static SRSRawPedestal* GetRawPedestalRootFile(const char* filename);

  void SetRunName(TString runname) { fRunName = runname; }
  void SetRunType(TString runtype) { fRunType = runtype; }

private:
  int fNbOfAPVs;
  int fEventNb{-1};
  TString fRunName, fRunType;
  bool fIsRawPedestalComputed{false}, fIsFirstEvent{true};

  std::vector<TH1F*> fRMSDist;

  std::vector<TH1F*> fNoises;
  std::vector<TH1F*> fOffsets;
  std::vector<TH2F*> fRawPed2DHistos;
  std::vector<TH1F*> fPedHistos;

  std::vector<float> fRawPedestalData;

  ClassDef(SRSRawPedestal, 4)
};

#endif
