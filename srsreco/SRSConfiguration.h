/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSConfiguration                                                            *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSConfiguration_h
#define srsreco_SRSConfiguration_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>

#endif

/// Class containing all configuration parameters for configuration from file(s)
class SRSConfiguration : public TObject {
public:
  explicit SRSConfiguration();
  explicit SRSConfiguration(const char* file);
  ~SRSConfiguration() = default;

  void Init(const char* file = 0);
  bool FileExists(const char* name) const;

  const char* GetCycleWait() const;
  void SetCycleWait(const char* name) { fCycleWait = name; }
  const char* GetRunType() const;
  void SetRunType(const char* name) { fRunType = name; }
  const char* GetRunName() const;
  void SetRunName(const char* name) { fRunName = name; }
  const char* GetZeroSupCut() const;
  void SetZeroSupCut(const char* name) { fZeroSupCut = name; }
  const char* GetMaskedChannelCut() const;
  void SetMaskedChannelCut(const char* name) { fMaskedChannelCut = name; }
  const char* GetStartEventNumber() const;
  void SetStartEventNumber(const char* name) { fStartEventNumber = name; }
  const char* GetEventFrequencyNumber() const;
  void SetEventFrequencyNumber(const char* name) { fEventFrequencyNumber = name; }
  const char* GetMinClusterSize() const;
  void SetMinClusterSize(const char* name) { fMinClusterSize = name; }
  const char* GetMaxClusterSize() const;
  void SetMaxClusterSize(const char* name) { fMaxClusterSize = name; }
  const char* GetMaxClusterMultiplicity() const;
  void SetMaxClusterMultiplicity(const char* name) { fMaxClusterMultiplicity = name; }
  const char* GetMappingFile() const;
  void SetMappingFile(const char* name) { fMappingFile = name; }
  const char* GetPadMappingFile() const;
  void SetPadMappingFile(const char* name) { fPadMappingFile = name; }
  const char* GetSavedMappingFile() const;
  void SetSavedMappingFile(const char* name) { fSavedMappingFile = name; }
  const char* GetRunNbFile() const;
  void SetRunNbFile(const char* name) { fRunNbFile = name; }
  const char* GetHistoCfgName() const;
  void SetHistoCfgName(const char* name) { fHistosFile = name; }
  const char* GetDisplayCfgName() const;
  void SetDisplayCfgName(const char* name) { fDisplayFile = name; }
  const char* GetROOTDataType() const;
  void SetROOTDataType(const char* name) { fROOTDataType = name; }
  const char* GetPedestalFile() const;
  void SetPedestalFile(const char* name) { fPedestalFile = name; }
  const char* GetTrackingOffsetDir() const;
  void SetTrackingOffsetDir(const char* name) { fTrackingOffsetDir = name; }
  const char* GetRawPedestalFile() const;
  void SetRawPedestalFile(const char* name) { fRawPedestalFile = name; }
  const char* GetClusterPositionCorrectionFile() const;
  void setClusterPositionCorrectionFile(const char* name) { fPositionCorrectionFile = name; }
  const char* GetClusterPositionCorrectionFlag() const;
  void setClusterPositionCorrectionFlag(const char* name) { fPositionCorrectionFlag = name; }
  const char* GetAPVGainCalibrationFile() const;
  void SetAPVGainCalibrationFile(const char* name) { fAPVGainCalibrationFile = name; }
  const char* GetClusterMaxOrTotalADCs() const;
  void SetClusterMaxOrTotalADCs(const char* name) { fIsClusterMaxOrTotalADCs = name; }
  const char* GetHitMaxOrTotalADCs() const;
  void SetHitMaxOrTotalADCs(const char* name) { fIsHitMaxOrTotalADCs = name; }

  Bool_t Load(const char* filename);
  void Save(const char* filename) const;
  void Dump() const override;

  SRSConfiguration& operator=(const SRSConfiguration& rhs);

private:
  void BookArrays();
  void SetDefaults();

  TString fMappingFile, fPadMappingFile, fSavedMappingFile, fRunNbFile, fRunName, fRunType, fROOTDataType, fCycleWait,
      fZeroSupCut, fMaskedChannelCut, fHistosFile, fTrackingOffsetDir;
  TString fDisplayFile, fPositionCorrectionFile, fPositionCorrectionFlag, fPedestalFile, fRawPedestalFile,
      fAPVGainCalibrationFile;
  TString fMaxClusterSize, fMinClusterSize, fStartEventNumber, fMaxClusterMultiplicity, fIsHitMaxOrTotalADCs,
      fIsClusterMaxOrTotalADCs, fEventFrequencyNumber;

  ClassDef(SRSConfiguration, 1)
};

#endif
