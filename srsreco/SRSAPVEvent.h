/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSAPVEvent                                                                 *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSAPVEvent_h
#define srsreco_SRSAPVEvent_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>

#include <list>
#include <map>
#include <vector>

#include "srsreco/SRSHit.h"

#endif

class SRSAPVEvent : public TObject {
public:
  explicit SRSAPVEvent(int fec_no, int fec_channel, int apv_id, int sigmaLevel, int EventNumber, int packet_size);
  ~SRSAPVEvent();
  static constexpr size_t kNumChannels = 128;

  void Add32BitsRawData(unsigned int rawdata32bits);
  void ComputeTimeBinCommonMode();

  void Set32BitsRawData(std::vector<unsigned int> rawdata32bits);
  std::vector<unsigned int> GetRawData32bits() { return fRawData32bits; }

  void ComputeRawData16bits();
  std::vector<unsigned int> GetRawData16bits() { return fRawData16bits; }

  std::multimap<int, float> GetTimeBinMap() { return fapvTimeBinDataMap; }

  void ComputeMeanTimeBinPedestalData();
  std::vector<float> GetPedestalData() { return fPedestalData; }

  void ComputeMeanTimeBinRawPedestalData();
  std::vector<float> GetRawPedestalData() { return fRawPedestalData; }
  std::list<SRSHit*> ComputeListOfAPVHits();

  int APVchannelMapping(int chNo);
  int StandardMapping(int);
  int EICStripMapping(int);
  int CMSStripMapping(int);
  int HMSStripMapping(int);
  int NS2StripMapping(int);
  int ZigZagStripMapping(int);
  int MMStripMappingAPV1(int);
  int MMStripMappingAPV2(int);
  int MMStripMappingAPV3(int);
  int PRadStripsMapping(int);

  int StripMapping(int chNo);
  int APVchannelCorrection(int chNo);

  void SetZeroSupCut(int sigmalevel) { fZeroSupCut = sigmalevel; }
  void SetAPVHeaderLevel(int level) { fAPVHeaderLevel = level; }
  void SetAPVIndexOnPlane(int index) { fAPVIndexOnPlane = index; }

  void Print(Option_t* = "") const override;
  void Clear(Option_t* = "") override;

  void SetFECNo(int fecNo) { fFECNo = fecNo; }
  int GetFECNo() { return fFECNo; }

  void SetAPVID(int apvid) { fAPVID = apvid; }
  int GetAPVID() { return fAPVID; }

  void SetAPVKey(int apvkey) { fAPVKey = apvkey; }
  int GetAPVKey() { return fAPVKey; }

  void SetAPV(TString apv) { fAPV = apv; }
  TString GetAPV() { return fAPV; }

  void SetAPVGain(float apvgain) { fAPVGain = apvgain; }
  float GetAPVGain() { return fAPVGain; }

  void SetADCChannel(int fecChannel) { fADCChannel = fecChannel; }
  int GetADCChannel() { return fADCChannel; }

  void SetEventNumber(int eventNb) { fEventNb = eventNb; }
  int GetEventNumber() { return fEventNb; }

  int GetAPVHeaderLevel() { return fAPVHeaderLevel; }
  int GetAPVIndexOnPlane() { return fAPVIndexOnPlane; }

  void SetAPVOrientation(int orient) { fAPVOrientation = orient; }
  int GetAPVOrientation() { return fAPVOrientation; }

  void SetPacketSize(int size) { fPacketSize = size; }
  int GetPacketSize() { return fPacketSize; }

  void SetNbOfAPVsFromPlane(int nb) { fNbOfAPVsFromPlane = nb; }
  int GetNbOfAPVsFromPlane() { return fNbOfAPVsFromPlane; }

  void SetPlaneSize(float planesize) { fPlaneSize = planesize; }
  float GetPlaneSize() { return fPlaneSize; }

  void SetPlane(TString plane) { fPlane = plane; }
  TString GetPlane() { return fPlane; }

  void SetPedSubFlag(bool pedSub) { fPedSubFlag = pedSub; }
  void SetCommonModeFlag(bool commonmode) { fCommonModeFlag = commonmode; }

  void SetAllFlags(bool pedSub, bool commonmode) {
    SetPedSubFlag(pedSub);
    SetCommonModeFlag(commonmode);
  }

  void SetPedestals(std::vector<float> noises, std::vector<float> offsets, std::vector<float> maskChs) {
    fMaskedChannels = maskChs;
    fPedestalNoises = noises;
    fPedestalOffsets = offsets;
  }

  void SetRawPedestals(std::vector<float> noises, std::vector<float> offsets) {
    fRawPedestalNoises = noises;
    fRawPedestalOffsets = offsets;
  }

  float GetMeanAPVnoise() { return fMeanAPVnoise; }

  void SetHitMaxOrTotalADCs(TString isHitMaxOrTotalADCs) { fIsHitMaxOrTotalADCs = isHitMaxOrTotalADCs; }
  TString GetHitMaxOrTotalADCs() { return fIsHitMaxOrTotalADCs; }

private:
  int fEventNb, fFECNo, fADCChannel, fAPVID, fAPVKey, fZeroSupCut, fNbCluster;
  int fAPVIndexOnPlane{2}, fAPVOrientation{0}, fNbOfAPVsFromPlane, fAPVHeaderLevel{1300}, fPacketSize;
  float fPlaneSize{102.4}, fEtaSectorPos{0}, fAPVGain{1.}, fMeanAPVnoise{0.};
  bool fCommonModeFlag{false}, fPedSubFlag{false}, fIsCosmicRunFlag{false}, fIsPedestalRunFlag{false},
      fIsRawPedestalRunFlag{false}, fAPVGainFlag;
  TString fAPV, fPlane{"GEM1X"}, fDetector{"GEM1"}, fDetectorType{"STANDARD"}, fReadoutBoard{"CARTESIAN"},
      fIsHitMaxOrTotalADCs;

  float fTrapezoidDetLength, fTrapezoidDetOuterRadius{430.}, fTrapezoidDetInnerRadius{220.};

  std::vector<unsigned int> fRawData16bits, fRawData32bits;
  std::multimap<int, float> fapvTimeBinDataMap;
  std::vector<float> fPedestalData, fRawPedestalData, fPedestalOffsets, fRawPedestalOffsets, fPedestalNoises,
      fRawPedestalNoises, fMaskedChannels;

  std::vector<float> fPadDetectorMap, fCommonModeOffsets, fCommonModeOffsets_odd, fCommonModeOffsets_even;

  std::map<int, int> fapvChToPadChMap;

  ClassDef(SRSAPVEvent, 1)
};

#endif
