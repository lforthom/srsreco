#ifndef srsreco_SRSCluster_h
#define srsreco_SRSCluster_h

/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSCluster                                                                  *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObjArray.h>
#include <TObject.h>

#include "srsreco/SRSHit.h"

#endif

class SRSCluster : public TObject {
public:
  explicit SRSCluster(int minClusterSize, int maxClusterSize, TString isMaximumOrTotalCharges);
  ~SRSCluster();

  bool IsSortable() const override { return true; }
  TObjArray *GetArrayOfHits() { return fArrayOfHits.get(); }

  SRSHit *GetHit(int i) {
    TObjArray &temp = *fArrayOfHits.get();
    return (SRSHit *)temp[i];
  }

  void SetMinClusterSize(int min) { fMinClusterSize = min; }
  void SetMaxClusterSize(int max) { fMaxClusterSize = max; }

  void AddHit(SRSHit *h);
  void Timing();

  int Compare(const TObject *obj) const override;

  int &GetNbOfHits() { return fNbOfHits; }

  TString GetPlane() { return fPlane; }
  void SetPlane(TString planename) { fPlane = planename; }

  int GetNbAPVsFromPlane() { return fNbAPVsOnPlane; }
  void SetNbAPVsFromPlane(int nb) { fNbAPVsOnPlane = nb; }

  float GetPlaneSize() { return fPlaneSize; }
  void SetPlaneSize(float planesize) { fPlaneSize = planesize; }

  float &GetClusterPosition() { return fposition; }
  float &GetClusterCentralStrip() { return fclusterCentralStrip; }

  int GetClusterTimeBin();
  int GetClusterPeakTimeBin() { return fClusterPeakTimeBin; }

  float GetClusterADCs();
  void SetClusterADCs(float adc) { fClusterSumADCs = adc; }

  void Dump() const override;
  void ClearArrayOfHits();
  bool IsGoodCluster();

  void ClusterCentralStrip();
  void ClusterPositionHistoMean();
  void ClusterPositionGausFitMean();
  void ClusterPositionPulseHeghtWeight();
  void GetClusterPositionCorrectionHisto();
  void GetClusterPositionAfterCorrection();
  std::vector<float> GetClusterTimeBinADCs() { return fClusterTimeBinADCs; };

  void ComputeClusterPositionWithCorrection();
  void ComputeClusterPositionWithoutCorrection();

  void ComputeClusterPositionWithCorrection(const char *filename);
  void SetClusterPositionCorrection(bool isCluserPosCorrection) { fIsCluserPosCorrection = isCluserPosCorrection; }

private:
  int fNbOfHits{0};                         // numbers of strips with hit
  std::unique_ptr<TObjArray> fArrayOfHits;  // APV hits table
  int fClusterPeakTimeBin{0}, fClusterTimeBin{0};
  float fClusterPeakADCs{0.}, fClusterTimeBinADC{0.}, fClusterSumADCs{0.}, fposition{0.}, fclusterCentralStrip{0.},
      fstrip{0.}, fPlaneSize{512.};
  int fapvID, fStripNo, fAbsoluteStripNo, fapvIndexOnPlane, fNbAPVsOnPlane{10}, fMinClusterSize, fMaxClusterSize;
  TString fIsClusterMaxOrSumADCs, fPlane{"GEM1X"};
  bool fIsGoodCluster{true}, fIsCluserPosCorrection{false};

  std::vector<float> fClusterTimeBinADCs;

  ClassDef(SRSCluster, 1)
};

#endif
