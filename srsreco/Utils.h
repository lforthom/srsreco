#ifndef srsreco_Utils_h
#define srsreco_Utils_h

#include <TString.h>

#include <vector>

namespace srsreco {
  namespace utils {
    std::vector<float> getDirection(const std::vector<float>&);
    float projectedAngleXY(const std::vector<float>& u, TString xORy);
    float projectedAngleXZ(const std::vector<float>& u, TString xORz);
    float normVec(const std::vector<float>& u);
    float dotVec(const std::vector<float>& u, const std::vector<float>& v);
    std::vector<float> subVec(const std::vector<float>& u, const std::vector<float>& v);
    std::vector<float> addVec(const std::vector<float>& u, const std::vector<float>& v);
    std::vector<float> prodVec(float a, const std::vector<float>& u);
    std::vector<float> directionVectorFrom2Points(const std::vector<float>& u, const std::vector<float>& v);
    float getAngleTo(const std::vector<float>& u, const std::vector<float>& v);
    std::vector<float> getXandYKnowingZ(const std::vector<float>& w, const std::vector<float>& v, float z0);
    float convertRadianToDegree(float angle_rad);
    float convertDegreeToRadian(float angle_deg);
  }  // namespace utils
}  // namespace srsreco

#endif
