/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSTrackFit                                                                 *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 01/05/2011                                             *
*******************************************************************************/

#ifndef srsreco_SRSTrackFit_h
#define srsreco_SRSTrackFit_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>
#include <TString.h>

#include <map>
#include <vector>

#endif

class SRSTrackFit : public TObject {
public:
  explicit SRSTrackFit(std::map<TString, std::vector<float> > trackerData,
                       std::map<TString, std::vector<float> > rawData);
  ~SRSTrackFit();

  void WeightedLeastSquareLinearFitTrack();

  std::map<TString, float> GetFitParameters() { return fFitParameters; }

  std::map<TString, std::vector<float> > GetTrackerData() { return fTrackerData; }
  std::map<TString, std::vector<float> > GetTrackFittedData() { return fTrackFittedData; }

  void PlaneRotationAlongZaxis(float alpha, std::vector<float>& u);
  float GetAngleBetweenTwoTracks(const std::map<TString, std::vector<float> >& firstTrack,
                                 const std::map<TString, std::vector<float> >& secondTrack);

  void ClearTracks();

private:
  std::map<TString, std::vector<float> > fTrackerData, fTrackFittedData, fRawData;
  std::map<TString, float> fFitParameters;

  ClassDef(SRSTrackFit, 1)
};

#endif
