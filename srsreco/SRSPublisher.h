/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSPublisher                                                                *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSPublisher_h
#define srsreco_SRSPublisher_h

#include <Event.h>
#include <MonitorObject.h>
#include <PublisherModule.h>
#include <TDATEEventParser.h>
#include <TFile.h>

#include <list>
#include <map>

#include "srsreco/SRSAlignment.h"
#include "srsreco/SRSCommon.h"
#include "srsreco/SRSConfiguration.h"
#include "srsreco/SRSFECEventDecoder.h"
#include "srsreco/SRSFECPedestalDecoder.h"
#include "srsreco/SRSHistoManager.h"
#include "srsreco/SRSMapping.h"
#include "srsreco/SRSOutputROOT.h"
#include "srsreco/SRSPedestal.h"
#include "srsreco/SRSPositionCorrection.h"
#include "srsreco/SRSRawPedestal.h"
#include "srsreco/SRSTrack.h"

//class amore::core::Event;

namespace amore {
  namespace SRS {
    namespace publisher {
      class SRSPublisher : public amore::publisher::PublisherModule, public amore::SRS::common::SRSCommon {
      public:
        explicit SRSPublisher();
        ~SRSPublisher() = default;

        virtual void BookMonitorObjects();
        virtual void EndOfCycle();
        virtual void StartOfCycle();
        virtual void MonitorEvent(amore::core::Event& event);
        virtual void EndOfRun(const amore::core::Run& run);
        virtual void StartOfRun(const amore::core::Run& run);
        virtual void EndOfSession(const amore::core::Session& session) {};
        virtual void StartOfSession(const amore::core::Session& session) {};

        virtual std::string Version();  // the version of the module

        void ResetMonitors(void);
        void ObjectsToBePublished(void);

        void LoadPedestalRootFile(const char* filename, Int_t nbOfAPVs);
        void LoadRawPedestalRootFile(const char* filename, Int_t nbOfAPVs);
        void LoadAPVGainCalibrationRootFile(const char* filename, Int_t nbOfAPVs);
        //     	void LoadClusterPositionCorrectionRootFile(const char * filename) ;

      private:
        Int_t fEvent{0};
        Int_t fEventType{0};
        Int_t fEventRunNb{-1};
        Int_t fEquipmentSize{0};

        TFile* fPedRootFile;
        TFile* fRawPedRootFile;
        TFile* fClusterPositionCorrectionRootFile;

        //	TString fRunType, fCFGFile, fCycleWait, fPocaDataTextFile, fIsClusterMaxOrSumADCs, fStartEventNumber, fEventFrequencyNumber ;
        TString fRunType{"RAWDATA"}, fCFGFile, fCycleWait, fStartEventNumber{"0"}, fEventFrequencyNumber{"1"},
            fAmoreAgentID;

        SRSTrack* fTrack;
        SRSHistoManager* fHMan;
        SRSConfiguration* fSRSConf;
        SRSAlignment* fAlignment;
        SRSOutputROOT* fROOT;

        SRSPedestal* fSavePedFromThisRun;
        SRSPedestal* fLoadPedToUse;

        SRSRawPedestal* fSaveRawPedFromThisRun;
        SRSRawPedestal* fLoadRawPedToUse;

        SRSPositionCorrection* fSavePosCorrFromThisRun;
        SRSPositionCorrection* fLoadPositionCorrectionToUse;

        SRSMapping* fMapping;

        Int_t fMinNbOfEquipments{1};
        Int_t fMaxNbOfEquipments{8};

        Bool_t fIsClusterPosCorrection{false}, fPlotEtaFunctionHistos{false};
        map<Int_t, Int_t> fApvNoFromApvIDMap;
        map<Int_t, Float_t> fApvGainFromApvNoMap;
        map<Int_t, TString> fApvNameFromIDMap;
        TH1F** fEtaFunctionHistos;

        ClassDef(SRSPublisher, 1);  // SRS Module Base Class
      };
    };  // namespace publisher
  };  // namespace SRS
};  // namespace amore

#endif
