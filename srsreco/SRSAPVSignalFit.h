/*******************************************************************************
*  AMORE FOR SRS - SRS                                                         *
*  SRSAPVSignalFit                                                             *
*  SRS Module Class                                                            *
*  Author: Kondo GNANVO 18/08/2010                                             *
*******************************************************************************/

#ifndef srsreco_SRSAPVSignalFit_h
#define srsreco_SRSAPVSignalFit_h

#if !defined(__CINT__) || defined(__MAKECINT__)

#include <TObject.h>

#include <vector>

#endif

class SRSAPVSignalFit : public TObject {
public:
  explicit SRSAPVSignalFit(int apvID, int stripNo);
  ~SRSAPVSignalFit() = default;

  void SetAPVID(int apvID) { fapvID = apvID; }
  void SetStripNo(int stripNo) { fStripNo = stripNo; }

  double GetAPVStripPulseHeight(std::vector<double> apvTimingSignal);

private:
  int fapvID;    // APV channel ID
  int fStripNo;  // strip number

  ClassDef(SRSAPVSignalFit, 1)
};

#endif
