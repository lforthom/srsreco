/**************************************************************************
 * Copyright(c) 2007-2009, ALICE Experiment at CERN, All rights reserved. *
 * See cxx source for full Copyright notice                               *
 **************************************************************************/

#ifndef srsreco_SRSCommon_h
#define srsreco_SRSCommon_h

namespace amore {
  namespace SRS {
    namespace common {
      /// \author Kondo Gnanvo
      class SRSCommon {
      public:
        SRSCommon();
        ~SRSCommon();

      protected:
        /*amore::core::MonitorObjectString *moDisplayCfgFilename;
        amore::core::MonitorObjectString *moString, *moDetPlanesDisplay, *moDetectorsDisplay;
        amore::core::MonitorObjectH1I *moHit1D, *moHitDist, *moClusterSizeDist, *moPulseHeight1D;
        amore::core::MonitorObjectH2I *moHitMap;
        amore::core::MonitorObjectGraph *moChargesShGraph, *moClusterCorrGraph;*/
      };
    }  // namespace common
  }  // namespace SRS
}  // namespace amore

#endif
